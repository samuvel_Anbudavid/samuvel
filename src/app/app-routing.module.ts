// Angular
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Components
import {BaseComponent} from './views/theme/base/base.component';
import {ErrorPageComponent} from './views/theme/content/error-page/error-page.component';
// Auth
import {AuthGuard} from './core/auth';

import {AdminDashboardComponent} from './views/pages/admin-dashboard/admin-dashboard.component';
import {BrandingElementMasterComponent} from './views/pages/branding-element-master/branding-element-master.component';
import {CheckListMasterComponent} from './views/pages/check-list-master/check-list-master.component';
import {ProductGroupingComponent} from './views/pages/product-grouping/product-grouping.component';
import {PromotionalMaterialMasterComponent} from './views/pages/promotional-material-master/promotional-material-master.component';
import {RetailMasterComponent} from './views/pages/retail-master/retail-master.component';
import {StoreCategoryMasterComponent} from './views/pages/store-category-master/store-category-master.component';
import {TerritoryMasterComponent} from './views/pages/territory-master/territory-master.component';
import {UserMasterComponent} from './views/pages/user-master/user-master.component';
import {WorkCategoryMasterComponent} from './views/pages/work-category-master/work-category-master.component';
import{TestingComponent}from'./views/pages/testing/testing.component';

import{StoreInspectorMasterComponent}from'./views/pages/store-inspector-master/store-inspector-master.component';
//import { RegContractorsVendorComponent } from './views/pages/reg-contractors-vendor/reg-contractors-vendor.component';
import{MerchandMenuComponent}from'./views/pages/merchand-menu/merchand-menu.component';
import {AuditListComponent} from './views/pages/audit-list/audit-list.component';
import {ContractorDashboardComponent} from './views/pages/contractor-dashboard/contractor-dashboard.component';
import {ManagerDashboardComponent} from './views/pages/manager-dashboard/manager-dashboard.component';
import {UserProfileComponent} from './views/pages/user-profile/user-profile.component';
import{ViewReportsComponent} from './views/pages/view-reports/view-reports.component';
import{DefectReportComponent}from'./views/pages/defect-report/defect-report.component';
import { from } from 'rxjs';
const routes: Routes = [
	{path: 'auth', loadChildren: () => import('app/views/pages/auth/auth.module').then(m => m.AuthModule)},

	{
		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: 'admin_dashboard',
				component: AdminDashboardComponent
			},
			{
				path: 'branding_element_master',
				component: BrandingElementMasterComponent
			},
			{
				path: 'check_list_master',
				component: CheckListMasterComponent
			},
			{
				path: 'product_grouping',
				component: ProductGroupingComponent
			},
			{
				path: 'promotional_material_master',
				component: PromotionalMaterialMasterComponent
			},
			{
				path: 'retail_master',
				component: RetailMasterComponent
			},
			{
				path: 'audit_list',
				component: AuditListComponent
			},
			{
				path: 'store_category_master',
				component: StoreCategoryMasterComponent
			},
			{
				path: 'territory_master',
				component: TerritoryMasterComponent
			},
			{
				path: 'user_master',
				component: MerchandMenuComponent
			},
			{
				path: 'work_category_master',
				component: WorkCategoryMasterComponent
			},			
			{
				path: 'store_inspector_master',
				component: StoreInspectorMasterComponent
				
			},
			{
				path: 'm_dashboard',
				component: ManagerDashboardComponent
				
			},
			{
				path: 'c_dashboard',
				component: ContractorDashboardComponent
				
			},	

			{
				path: 'profile',
				component: UserProfileComponent
				
			},
			{
				path:'view_reports',
				component:ViewReportsComponent
			},
			{
				path:'defect_reports',
				component:DefectReportComponent
			},
			// {
			// 	path: 'dashboard',
			// 	loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
			// },
			// {
			// 	path: 'mail',
			// 	loadChildren: () => import('app/views/pages/apps/mail/mail.module').then(m => m.MailModule),
			// },
			// {
			// 	path: 'ecommerce',
			// 	loadChildren: () => import('app/views/pages/apps/e-commerce/e-commerce.module').then(m => m.ECommerceModule),
			// },
			// {
			// 	path: 'ngbootstrap',
			// 	loadChildren: () => import('app/views/pages/ngbootstrap/ngbootstrap.module').then(m => m.NgbootstrapModule),
			// },
			// {
			// 	path: 'material',
			// 	loadChildren: () => import('app/views/pages/material/material.module').then(m => m.MaterialModule),
			// },
			// {
			// 	path: 'user-management',
			// 	loadChildren: () => import('app/views/pages/user-management/user-management.module').then(m => m.UserManagementModule),
			// },
			// {
			// 	path: 'wizard',
			// 	loadChildren: () => import('app/views/pages/wizard/wizard.module').then(m => m.WizardModule),
			// },
			// {
			// 	path: 'builder',
			// 	loadChildren: () => import('app/views/theme/content/builder/builder.module').then(m => m.BuilderModule),
			// },
			{
				path: 'error/403',
				component: ErrorPageComponent,
				data: {
					type: 'error-v6',
					code: 403,
					title: '403... Access forbidden',
					desc: 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator',
				},
			},
			{path: 'error/:type', component: ErrorPageComponent},
		//	{path: '', redirectTo: 'dashboard', pathMatch: 'full'},
		//	{path: '**', redirectTo: 'dashboard', pathMatch: 'full'},
		{path: '**', redirectTo: '/auth/login', pathMatch: 'full'}

		],
	},

	{path: '**', redirectTo: 'error/403', pathMatch: 'full'},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {
}
