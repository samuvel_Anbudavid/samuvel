// Angular
import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash'; 

/*
@Pipe({
    name: 'unique',
    pure: false
})
export class UniqueValuePipe implements PipeTransform {

	/**
	 * Transform
	 *
	 * @param value: any
	 * @param args: any
	 */
  /*  transform(value: any): any{
        if(value!== undefined && value!== null){
           return _.uniqBy(value, 'material');
        }
        return value;
    }
}*/


@Pipe({
    name: 'unique',
    pure: false
})
export class UniqueValuePipe implements PipeTransform {

        transform(items, args): any {

        // lodash uniqBy function
        return _.uniqBy(items, args);
    }
}
