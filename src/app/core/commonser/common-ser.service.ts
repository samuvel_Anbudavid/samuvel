import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { PageEvent } from '@angular/material';
@Injectable({
  providedIn: 'root'
})
export class CommonSerService {

  constructor(private modalService:NgbModal) { }
  timeDiffCalc(dateFuture, dateNow) {
    var finalAry=[];
    let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
    
    // calculate days
    const days = Math.floor(diffInMilliSeconds / 86400);
    diffInMilliSeconds -= days * 86400;
    console.log('calculated days', days);
    
    // calculate hours
    const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
    diffInMilliSeconds -= hours * 3600;
    console.log('calculated hours', hours);
    
    // calculate minutes
    const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
    diffInMilliSeconds -= minutes * 60;
    console.log('minutes', minutes);
    
    let difference = '';
    if (days > 0) {
    difference += (days === 1) ? `${days} day, ` : `${days} days, `;
    }
    
    difference += (hours === 0 || hours === 1) ? `${hours} hour, ` : `${hours} hours, `;
    
    difference += (minutes === 0 || hours === 1) ? `${minutes} minutes` : `${minutes} minutes`;
    finalAry=[difference,days,hours,minutes];
   
    return finalAry;
    }
    timeStampConversion(days:number,hours:number,minutes:number){ //unix timestamp conversion
      const daysTsp = days * 86400;
      const hoursTsp = hours * 3600;
      const minutesTsp = minutes * 60;
      return daysTsp+hoursTsp+minutesTsp; 

    }
    minutesToSecConversion(mins:number){ //seconds conve
      return mins*60;
      }

    hoursToSecConversion(hrs:number){
    return hrs*3600;
    }

    daysToSecConversion(days:number){
      return days*86400;
      }

      durationDropdownSelection(dur,strtTsp,number){
        var diff;
        switch (dur) {
          case 'Minute': {
           
            diff=(strtTsp-this.minutesToSecConversion(number));
            break;
          }
          case 'Hour': {
           
            
            diff=(strtTsp-(this.hoursToSecConversion(number)));
            break;
          }
          case 'Day': {
          
            
            diff=(strtTsp-this.daysToSecConversion(number));
            break;
          }
        }
        return diff;
      }
      durationDropdownSelectionActual(dur,strtTsp,number){
        var result;
        switch (dur) {
          case 'Minute': {
           
            result=this.minutesToSecConversion(number);
            break;
          }
          case 'Hour': {
           
            
            result=this.hoursToSecConversion(number);
            break;
          }
          case 'Day': {
          
            
            result=this.daysToSecConversion(number);
            break;
          }
        }
        return result;
      }

  
  
    isControlHasError(controlName: string, validationType: string,fromgroup:FormGroup): boolean {
      const control = fromgroup.controls[controlName];
      if (!control) {
      return false;
      }
      
      const result = control.hasError(validationType) && (control.dirty || control.touched);
      return result;
      }
      // Time-Choose
     TimeChoose=[
       { type:'Minute'},
       { type:'Hour'},
       { type:'Day'},
      ]
      
       // Model box -serivce
       openCentred(content,size) {
       return this.modalService.open(content, { centered: true, size:size} );
    }
  Modal_close()
  {
    this.modalService.dismissAll();
  }
  //Apply Filter
  applyFilter(filterValue: string,dataSource:any) {
   
  
    if (dataSource.paginator) {
     dataSource.paginator.firstPage();
    }
    return dataSource.filter = filterValue.trim().toLowerCase();
  }
  // Toggle botton
  formValidCheck(form:FormGroup)
  {
    
  }
  // pagenagi
   //iterator -Pagenation
   iterator(assignedProduct,currentPage,pageSize )
   {
 
     const end = (currentPage + 1) * pageSize;
   const start =  currentPage * pageSize;
   const part = assignedProduct.slice(start, end);
   return assignedProduct = part;
   
   }
   handlePage(e: PageEvent ,data) {
     console.log(e);
     var currentPage = e.pageIndex;
     var pageSize = e.pageSize;
     this.iterator(data,currentPage,pageSize);
   }
 
  //  Checkbox 
  

}
//Interface
export interface User {
 
    type:string;
  }
  
const TimeChoose:User[]=[];

