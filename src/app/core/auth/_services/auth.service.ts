import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../_models/user.model';
import { Permission } from '../_models/permission.model';
import { Role } from '../_models/role.model';
import { catchError, map } from 'rxjs/operators';
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';

//import{RequestOptions,Headers,Http} from '@angular/http';

//const API_USERS_URL = 'api/users';
const API_PERMISSION_URL = 'api/permissions';
const API_ROLES_URL = 'api/roles';
const API_USERS_URL = 'https://dev.kaspontech.com/VM_BackEnd/public/';
// const API_USERS_URL = 'http://localhost:8080/VM/VM_BackEnd/public/';
const httpHeaders = new HttpHeaders();
httpHeaders.set('Content-Type', 'application/json');
httpHeaders.set('Access-Control-Allow-Origin', '*');

@Injectable()
export class AuthService {
    constructor(private http: HttpClient) {}
    // Authentication/Authorization 
    // login1(email_id: string, password: string): Observable<any> {  //done
    //     return this.http.post<any>(API_USERS_URL+'login_user'  ,JSON.stringify({ email_id, password }), { headers: httpHeaders });
    // }
    createProductTable(ary:any){
        return this.http.post<any>(API_USERS_URL+'create_product_table' ,JSON.stringify({ ary }), { headers: httpHeaders });
 
    }
    storeProductsValues(products:any){
        return this.http.post<any>(API_USERS_URL+'store_products', JSON.stringify({ products }), { headers: httpHeaders });
    
    }
    editProductsValues(products:any,s_no:number){
        return this.http.post<any>(API_USERS_URL+'edit_products', JSON.stringify({ products,s_no }), { headers: httpHeaders });
    
    }
   
    getAllProducts(){
        return this.http.get<any>(API_USERS_URL+'get_all_products', { headers: httpHeaders });

     }
     deleteProducts(product_id)
{
    return this.http.post<any>(API_USERS_URL+'delete_products', JSON.stringify({ product_id }), { headers: httpHeaders });
    

}
uploadProducts(be: any): Observable<any>{
    return this.http.post<any>(API_USERS_URL+'upload_products', JSON.stringify({ be }), { headers: httpHeaders });

 }
 uploadBrandingElements(be: any): Observable<any>{
    return this.http.post<any>(API_USERS_URL+'upload_branding_elements', JSON.stringify({ be }), { headers: httpHeaders });

 }

 storeBrandingElements(user: any): Observable<any> {
    return this.http.post<any>(API_USERS_URL+'create_branding_elements', user, { headers: httpHeaders });
    
 }
 editBrandingElements(id: number){
    return this.http.post<any>(API_USERS_URL+'edit_branding_elements', JSON.stringify({ id }), { headers: httpHeaders });

 }
 deleteBrandingElements(id: number){
    return this.http.post<any>(API_USERS_URL+'delete_branding_elements', JSON.stringify({ id }), { headers: httpHeaders });

 }
 saveNewAuditPeriodic(details:any){
    return this.http.post<any>(API_USERS_URL+'save_new_audit_periodic', details, { headers: httpHeaders });

 }
 getAllAuditList(userid:any,email:any){
    return this.http.post<any>(API_USERS_URL+'get_all_audit_list', JSON.stringify({ userid,email }), { headers: httpHeaders });

}
//  getBrandingElements(){
//     return this.http.get<any>(API_USERS_URL+'get_all_branding_elements', { headers: httpHeaders });

//  }
 bulkDeleteBrandingElements(be: any): Observable<any>{
    return this.http.post<any>(API_USERS_URL+'bulk_delete_branding_elements', JSON.stringify({ be }), { headers: httpHeaders });

 }

//  getRetailCategories(){
//     return this.http.get<any>(API_USERS_URL+'get_all_store_categories', { headers: httpHeaders });

//  }
//  getBrandingElementsRetails(){
//     return this.http.get<any>(API_USERS_URL+'get_all_branding_elements_retails', { headers: httpHeaders });

//  }
//  getPromotionalRetails(){
//     return this.http.get<any>(API_USERS_URL+'get_all_promotional_retails', { headers: httpHeaders });

//  }
//  getProductRetails(){
//     return this.http.get<any>(API_USERS_URL+'get_all_product_retails', { headers: httpHeaders });

//  }
//  getCheckListRetails(){
//     return this.http.get<any>(API_USERS_URL+'get_all_checklist_retails', { headers: httpHeaders });

//  }
 saveRetailDetails(details: any): Observable<any> {
    return this.http.post<any>(API_USERS_URL+'store_retail_details', details, { headers: httpHeaders });
    
 }
 savePersonalDetails(details: any): Observable<any> {
    return this.http.post<any>(API_USERS_URL+'save_personal_details', details, { headers: httpHeaders });
    
 }
//  getRetailList(){
//     return this.http.get<any>(API_USERS_URL+'get_all_retails', { headers: httpHeaders });

//  }
 getUserDetails(s_no:any,role_id:any){
      return this.http.post<any>(API_USERS_URL+'get_user_detail', JSON.stringify({ s_no,role_id }), { headers: httpHeaders });

 }
 getRetailListNewAudit(email:any,user_id:any){
    return this.http.post<any>(API_USERS_URL+'get_retails_new_audit', JSON.stringify({ email,user_id }), { headers: httpHeaders });
 }
 getInspectorListNewAudit(){
    return this.http.get<any>(API_USERS_URL+'get_all_inspectors_new_audit', { headers: httpHeaders }); 
 }
 updateUserPassword(s_no:any,role_id:any,current_pwd:any,new_pwd:any){
    return this.http.post<any>(API_USERS_URL+'update_user_pwd', JSON.stringify({ s_no,role_id,current_pwd,new_pwd }), { headers: httpHeaders });

 }
 deleteRetail(id:number){
    return this.http.post<any>(API_USERS_URL+'delete_retail', JSON.stringify({ id }), { headers: httpHeaders });  
 }
 bulkDeleteRetails(be: any): Observable<any>{
    return this.http.post<any>(API_USERS_URL+'bulk_delete_retails', JSON.stringify({ be }), { headers: httpHeaders });

 }
 uploadRetaildetails(be: any): Observable<any>{
    return this.http.post<any>(API_USERS_URL+'upload_all_retail_details', be , { headers: httpHeaders });

 }
 retailcheck(retail: any): Observable<any> {
    return this.http.post<any>(API_USERS_URL+'retailer_check',retail, { headers: httpHeaders });
    
 }
 /*getRetailBEList(){
    return this.http.get<any>(API_USERS_URL+'get_all_retails_be', { headers: httpHeaders });

 }*/
    getUserByToken(): Observable<User> {
        const userToken = localStorage.getItem(environment.authTokenKey);
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Authorization', 'Bearer ' + userToken);
        return this.http.get<User>(API_USERS_URL, { headers: httpHeaders });
    }

    register(user: User): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<User>(API_USERS_URL, user, { headers: httpHeaders })
            .pipe(
                map((res: User) => {
                    return res;
                }),
                catchError(err => {
                    return null;
                })
            );
    }

    /*
     * Submit forgot password request
     *
     * @param {string} email
     * @returns {Observable<any>}
     */
    public requestPassword(email: string): Observable<any> {
    	return this.http.get(API_USERS_URL + '/forgot?=' + email)
    		.pipe(catchError(this.handleError('forgot-password', []))
	    );
    }


    getAllUsers(): Observable<User[]> {
		return this.http.get<User[]>(API_USERS_URL);
    }

    getUserById(userId: number): Observable<User> {
		return this.http.get<User>(API_USERS_URL + `/${userId}`);
	}


    // DELETE => delete the user from the server
	deleteUser(userId: number) {
		const url = `${API_USERS_URL}/${userId}`;
		return this.http.delete(url);
    }

    // UPDATE => PUT: update the user on the server
	updateUser(_user: User): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		      return this.http.put(API_USERS_URL, _user, { headers: httpHeaders });
	}

    // CREATE =>  POST: add a new user to the server
	createUser(user: User): Observable<User> {
    	const httpHeaders = new HttpHeaders();
     httpHeaders.set('Content-Type', 'application/json');
		   return this.http.post<User>(API_USERS_URL, user, { headers: httpHeaders});
	}

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
	// items => filtered/sorted result
	findUsers(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		      return this.http.post<QueryResultsModel>(API_USERS_URL + '/findUsers', queryParams, { headers: httpHeaders});
    }

    // Permission
    getAllPermissions(): Observable<Permission[]> {
		return this.http.get<Permission[]>(API_PERMISSION_URL);
    }

    getRolePermissions(roleId: number): Observable<Permission[]> {
        return this.http.get<Permission[]>(API_PERMISSION_URL + '/getRolePermission?=' + roleId);
    }

    // Roles
    getAllRoles(): Observable<Role[]> {
        return this.http.get<Role[]>(API_ROLES_URL);
    }

    getRoleById(roleId: number): Observable<Role> {
		return this.http.get<Role>(API_ROLES_URL + `/${roleId}`);
    }

    // CREATE =>  POST: add a new role to the server
	createRole(role: Role): Observable<Role> {
		// Note: Add headers if needed (tokens/bearer)
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		      return this.http.post<Role>(API_ROLES_URL, role, { headers: httpHeaders});
	}

    // UPDATE => PUT: update the role on the server
	updateRole(role: Role): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		      return this.http.put(API_ROLES_URL, role, { headers: httpHeaders });
	}

	// DELETE => delete the role from the server
	deleteRole(roleId: number): Observable<Role> {
		const url = `${API_ROLES_URL}/${roleId}`;
		return this.http.delete<Role>(url);
	}

    // Check Role Before deletion
    isRoleAssignedToUsers(roleId: number): Observable<boolean> {
        return this.http.get<boolean>(API_ROLES_URL + '/checkIsRollAssignedToUser?roleId=' + roleId);
    }

    findRoles(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        // This code imitates server calls
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		      return this.http.post<QueryResultsModel>(API_ROLES_URL + '/findRoles', queryParams, { headers: httpHeaders});
	}

 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result);
        };
    }

    // AJAX service of store & work category 

    public servicepost(data, methods, url,head) {
        var obj = {      
          submitUrl: url,
          data:data,
        }
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        
        return this.http.post<any>(API_USERS_URL +obj.submitUrl, JSON.stringify(obj.data),{ headers: httpHeaders })
       .pipe(
          // eg. "map" without a dot before
          map(response =>response),
          
       )
    
      };
      public serviceget(url) {
        //   var obj = {
        //     methods: methods,
        //     submitUrl: url,
        //     data: data,
        //     dataType: "JSON"
        //   }
        //   return this.http.get(API_USERS_URL + obj.submitUrl)
        //     .pipe(
        //       // eg. "map" without a dot before
        //       map(data => {
        //         return data;
        //       }),
        //       // "catchError" instead "catch"
        //       catchError(error => {
        //         return Observable.throw('Something went wrong ;)');
        //       })
        //    )
        // };
        // private _errorhandler(error: Response){
        //   console.error(error);
        //   return Observable.throw(error || "Error occured");
        // }
        return this.http.get<any>(API_USERS_URL+url, { headers: httpHeaders });
        }
}
