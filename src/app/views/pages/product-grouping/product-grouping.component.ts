import {SelectionModel} from '@angular/cdk/collections';
import {FlatTreeControl} from '@angular/cdk/tree';
import { Component, OnInit, Input, ViewChild, ElementRef, Injectable, ChangeDetectorRef} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {BehaviorSubject} from 'rxjs';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { AuthService } from '../../../core/auth';
import { finalize, takeUntil, tap, isEmpty } from 'rxjs/operators';
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {DataSource} from '@angular/cdk/collections';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray  } from '@angular/forms';
import * as XLSX from 'xlsx';
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import {LoaderComponent} from '../loader/loader.component';
import {CommonSerService}from'../../../core/commonser';
type AOA = any[][];


interface FoodNode {
  name: string;
  children?: FoodNode[];
}
/**
 * Node for to-do item
 */
export class TodoItemNode {
  children: TodoItemNode[];
  item: string;
}

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
}

/**
 * The Json object for to-do list data.
 */
/*var TREE_DATA = {
  'Click here to add nodes' : {
    'Almond Meal flour': null,
    'Organic eggs': null,
    'Protein Powder': null,
    Fruits: {
      Apple: null,
      Berries: ['Blueberry', 'Raspberry'],
      Orange: null
    }
  }
  
};*/
const TREE_DATA = {
  'Click Here To Add Sub-Groups' : ['Product ID']
}

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<TodoItemNode[]>([]);

  get data(): TodoItemNode[] { return this.dataChange.value; }

  constructor(private common:CommonSerService) {
    this.initialize();
  }

  initialize() {
    // Build the tree nodes from Json object. The result is a list of `TodoItemNode` with nested
    //     file node as children.
    const data = this.buildFileTree(TREE_DATA, 0);

    // Notify the change.
    this.dataChange.next(data);
  }

  /**
   * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `TodoItemNode`.
   */
  buildFileTree(obj: {[key: string]: any}, level: number): TodoItemNode[] {
    return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new TodoItemNode();
      node.item = key;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.item = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }

  /** Add an item to to-do list */
  insertItem(parent: TodoItemNode, name: string) {
  //  console.log(parent);
    if (parent.children) {
      parent.children.push({item: name} as TodoItemNode);
      this.dataChange.next(this.data);
    }
  }

  updateItem(node: TodoItemNode, name: string) {
    node.item = name;
    //console.log(this.data);
    this.dataChange.next(this.data);
  }

  removeItem(node: TodoItemNode, name: string) {
    this.data[0].children.forEach((element,index) => {
     if(element['item']===name){
        this.data[0].children.splice(index,1);
      }
    });
    this.dataChange.next(this.data);
  }
   
 
}

/**
 * @title Tree with checkboxes
 */

@Component({
  selector: 'kt-product-grouping',
  templateUrl: './product-grouping.component.html',
  styleUrls: ['./product-grouping.component.scss'],
  providers: [ChecklistDatabase]
})
export class ProductGroupingComponent{
  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

  /** A selected parent node to be inserted */
  selectedParent: TodoItemFlatNode | null = null;

  /** The new item's name */
  newItemName = '';

  treeControl: FlatTreeControl<TodoItemFlatNode>;

  treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;

  dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

  /** The selection for checklist */
  checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

  /**Start of Nested Tree*/
  treeControl1 = new NestedTreeControl<FoodNode>(node => node.children);
  dataSource2 = new MatTreeNestedDataSource<FoodNode>();
  nestedArray: FoodNode[] =[];
  NESTED_TREE_DATA: FoodNode[] = [];
  /**End of Nested Tree */

	@ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
	@ViewChild('sort1', {static: false}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  dataSource1=new MatTableDataSource();
  displayedColumns;
  columns;
  private showTree:boolean;
  private showTable:boolean;
  private products:FormGroup;
  private rows=[];
  private createNewAry=[];
  private snoValues;
  private currentSerialNum;
  private alertMessage;
  private allDataExcel= [];
  private fileName: string = 'Product Grouping.xlsx';
  private be: AOA = [[]];


  ngAfterViewInit()
{
  this.dataSource1.paginator = this.paginator;
    this.dataSource1.sort = this.sort; 

}

  constructor(private _database: ChecklistDatabase,private modalService: NgbModal,
    private auth: AuthService,private layoutUtilsService: LayoutUtilsService,
    private fb: FormBuilder,private cdr: ChangeDetectorRef,private overlay: Overlay,private common:CommonSerService)
     {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    _database.dataChange.subscribe(data => {
      this.dataSource.data = data;
    });
    this.columns = [
      
    ];
  
    this.displayedColumns = this.columns.map(c => c.columnDef);
    
    this.dataSource1 = new MatTableDataSource(ELEMENT_DATA1);
   
   //   this.dataSource1.paginator = this.paginator;
    //  this.dataSource1.sort = this.sort; 

      this.dataSource2.data = this.NESTED_TREE_DATA;
     }

  getLevel = (node: TodoItemFlatNode) => node.level;

  isExpandable = (node: TodoItemFlatNode) => node.expandable;

  getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

  hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

  hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

  hasChildNested = (_: number, node: FoodNode) => !!node.children && node.children.length > 0;


  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: TodoItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.item === node.item
        ? existingNode
        : new TodoItemFlatNode();
    flatNode.item = node.item;
   
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }

  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: TodoItemFlatNode): void {
    let parent: TodoItemFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: TodoItemFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

  /* Get the parent node of a node */
  getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  /** Select the category so we can insert the new item. */
  addNewItem(node: TodoItemFlatNode) {
   
    const parentNode = this.flatNodeMap.get(node);
    this._database.insertItem(parentNode!, '');
    this.treeControl.expand(node);
  }

  /** Save the node to database */
  saveNode(node: TodoItemFlatNode, itemValue: string) {
    const nestedNode = this.flatNodeMap.get(node);
    this._database.updateItem(nestedNode!, itemValue);
  }


  removeNode(node: TodoItemFlatNode, itemValue: string){
    
    const nestedNode = this.flatNodeMap.get(node);
    this._database.removeItem(nestedNode!, itemValue);

  }
  ngOnInit() {
     
this.overlayRef = this.overlay.create({	
  positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),	
  hasBackdrop: true	
});	
  this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
      this.overlayRef.attach(this.LoaderComponentPortal);
      this.getpro();
      this.initRegistrationForm();
  }


  createTable(content7){
    //this.openSmall(content7);
this.common.openCentred(content7,'md');
  }


initRegistrationForm() {


this.products = this.fb.group({
  persons: this.fb.array([this.fb.group({'Product ID':['',Validators.compose([Validators.required])]})])
});
}


openLarge(content) {
  this.modalService.open(content, {
    size: 'lg',
  });
}

openSmall(content) {
  this.modalService.open(content, {
      size: 'md'
  });
}


isControlHasError(controlName: string, validationType: string, index: number): boolean {
 
  const control = this.products.controls.persons['controls'][index].controls[controlName];
  if (!control) {
    return false;
  }

  const result = control.hasError(validationType) && (control.dirty || control.touched);
  return result;
}

headersInclusion(convertedAry){
  convertedAry.splice(0, 0, "s_no");
 var convertedAryLength=convertedAry.length;
 convertedAry.splice(convertedAryLength+1, 0, "action");
 return convertedAry;
}
clickedOk() {
//this.modalService.dismissAll();
this.common.Modal_close();
this.overlayRef.attach(this.LoaderComponentPortal);


  this.dataSource.data[0].children.forEach((element,index) => {
    if(element.item!=""){
    this.createNewAry.push(element.item);
    }
  });
 

  this.nestedTreeStructureCreation(this.createNewAry);
console.log(this.createNewAry);
  this.auth.createProductTable(this.createNewAry).pipe(
    tap(result => {
      console.log(result);
      
   //this.modalService.dismissAll();
 this.common.Modal_close();
 this.formContolObjetcsCreation(this.createNewAry);
 var convertedAry=this.headersInclusion(result['data']);
this.columns=[];
ELEMENT_DATA1=[];
var newObj={};
var newArry=[]
convertedAry.forEach((val,index) => { 
var lc=val.toLowerCase();

this.columns.push({columnDef:lc.replace(/\s/g, ""),header:val, cell: (element: any) =>  `${element[val]}` } );
  
});

  this.displayedColumns = this.columns.map(c => c.columnDef);
 
  this.dataSource1 = new MatTableDataSource(ELEMENT_DATA1);
  this.cdr.detectChanges();

   this.dataSource1.paginator = this.paginator;
      this.dataSource1.sort = this.sort;

  this.showTable=true;
  this.showTree=false;
  this.overlayRef.detach();
  
    }),
    
    ).subscribe();
}

formContolObjetcsCreation(headers): void{
  const persons =  this.products.get('persons') as FormArray;
  persons.clear();
  this.createNewAry=headers;
  for(var t=0;t<headers.length;t++){
    var keysValuePair={};
    keysValuePair[headers[t]]=['',Validators.compose([Validators.required])]; 
    persons.push(this.fb.group(keysValuePair));  // update FormArray
   }
}

nestedTreeStructureCreation(headers): void{
  var NESTED_TREE_DATA: FoodNode[] = [{name:'Product ID',children:[]}];
  for(var j=1;j<headers.length;j++){
    var testing={name:headers[j],children:[]};
    this.nestedArray=this.searchTree(NESTED_TREE_DATA[0], headers[j],testing);
}
//this.showTree=true;
var newArry=[];
newArry.push(this.nestedArray);
this.dataSource2=new MatTreeNestedDataSource<FoodNode>();
this.dataSource2.data=newArry;
this.treeControl1.dataNodes=newArry;
this.treeControl1.expandAll();
//this.overlayRef.detach();
}

structureCreation(headercolms,rows,myClonedArray): void{
this.columns=[];
ELEMENT_DATA1=[];
let newcol=this.headersInclusion(headercolms);

newcol.forEach((c_val,c_index) => {
    this.columns.push({columnDef:c_val,header:c_val, cell: (element: any) =>  `${element[c_val]}` } );
  });
  ELEMENT_DATA1.push(rows);


this.displayedColumns = this.columns.map(c => c.columnDef);

this.dataSource1 = new MatTableDataSource(ELEMENT_DATA1[0]);
this.showTree=false;
this.showTable=true;
this.cdr.detectChanges();
this.dataSource1.paginator = this.paginator;
this.dataSource1.sort = this.sort;
this.nestedTreeStructureCreation(myClonedArray);
this.formContolObjetcsCreation(myClonedArray);


}

onSubmitProducts(contentSmallAlert,action){
 

  var productsToStore={};
    if(this.checkFieldsEmpty(this.createNewAry)===true){
   
      this.createNewAry.forEach((element,index) => {
      const controls = this.products.controls.persons['controls'][index].controls;
      productsToStore[element]=controls[element].value;
     //  formData.append(element, controls[element].value);
});
//this.modalService.dismissAll();
this.common.Modal_close();
this.overlayRef.attach(this.LoaderComponentPortal);
switch(action) { 
  case 'new': { 
    this.auth.storeProductsValues(productsToStore).pipe(
      tap(result => {
        this.overlayRef.detach();

      this.alertMessage='Product ID Already Exists!';
      if(result['code']===200){
        this.products.reset();
        this.getpro();
        // this.modalService.dismissAll();
        this.common.Modal_close();
    //   this.layoutUtilsService.showActionNotification(result['message']);
    swal.fire(
 
      'Save!',
      'Your record has been saved.',
      'success'
    )
    
      }
      else{
       // this.openSmall(contentSmallAlert);
       this.common.openCentred(contentSmallAlert,'md');
      }
      }),
    
    ).subscribe();
    break;    
  }
  case 'edit':{
    console.log(this.currentSerialNum)
    this.auth.editProductsValues(productsToStore,this.currentSerialNum).pipe(
      tap(result => {
        this.overlayRef.detach();
      this.alertMessage='Product ID Already Exists!';
      if(result['code']===200){
        this.products.reset();
        this.getpro();
        //this.modalService.dismissAll();
        this.common.Modal_close();
      //  this.layoutUtilsService.showActionNotification(result['message']);
      swal.fire(
        'Edit!',
        'Your record has been Updated.',
        'success'
      )
      }
      else{
       // this.openSmall(contentSmallAlert);
       this.common.openCentred(contentSmallAlert,'md')
      }
    }),
    
  ).subscribe();
    break;    
  }
  default: { 
   
    break;              
 } 
}


  }
  

}


getpro(){
  const url='get_all_products';
  this.auth.serviceget(url).pipe(
    tap(result => {
      
      this.overlayRef.detach();
       if(((result['status']=='success')&&(result['data_c'].length>0))||(result['data_r'].length>0)){
        this.snoValues=result['data_s_no'];
        this.allDataExcel=result['data_r'];
        console.log(this.allDataExcel);
        const myClonedArray  = Object.assign([], result['data_c']);
        this.structureCreation(result['data_c'],result['data_r'],myClonedArray);
      }
      else{
        this.showTable=false;
        this.showTree=true;
      }
      

    }),

  ).subscribe();
}

 checkFieldsEmpty(formFields){
var returnVal:boolean;
  formFields.forEach((element,index) => {
    const controls = this.products.controls.persons['controls'][index].controls;
    if(!controls[element].value){
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
        );
        returnVal=false;
    }
    else{
      returnVal=true;
    }
  });
   return returnVal;

   
}

deleteProducts(product_id){

  swal.fire({
    title: 'Are you sure?',
    text: 'You will not be able to recover this Record!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, keep it'
  }).then((result) => {
    if (result.value) {
    this.overlayRef.attach(this.LoaderComponentPortal);

    this.auth.deleteProducts(product_id).pipe(
      tap(result => {
        this.overlayRef.detach();

        if(result['status']=="success"){
          const message='Your record has been deleted.';
          this.getpro();
          swal.fire(message);
                }
        else{ 

          const message='Failed to delete.';
          swal.fire(message);
                }
      }),
      
    ).subscribe(err => {
      console.log(err);
    });
    }
  })  





}


formContolObjetcsCreationForEdit(headers,values):void{
  const persons =  this.products.get('persons') as FormArray;
  persons.clear();
  //this.createNewAry=headers;
  //console.log(this.createNewAry);
  for(var t=0;t<headers.length;t++){
    var keysValuePair={};
    keysValuePair[headers[t]]=[values[headers[t]],Validators.compose([Validators.required])]; 
    persons.push(this.fb.group(keysValuePair));  // update FormArray
   }
 //  console.log(this.products.controls);
}

editProduct(productDetails,sNo){
console.log(productDetails);
console.log(sNo);

this.currentSerialNum=sNo;
this.formContolObjetcsCreationForEdit(this.createNewAry,productDetails)

}


 searchTree(element, matchingTitle,testing){
 if (element.children.length != 0){

       var i;
       for(i=0; i < element.children.length; i++){

          this.searchTree(element.children[i], matchingTitle,testing);
       }
  }
  else if(element.children.length==0)
  {

element.children.push(testing);
  }
  

  return element;
}

applyFilter(filterValue: string) {
  this.dataSource1.filter = filterValue.trim().toLowerCase();

  if (this.dataSource1.paginator) {
    this.dataSource1.paginator.firstPage();
  }
}

export(): void {

	var downloadArray= [];
	var testArray= [];
	var headerArray= [];

  const myClonedArray  = Object.assign([], this.createNewAry);
	headerArray=myClonedArray;
	headerArray.splice(0, 0, "S No");
	testArray.push(headerArray);
  
	for(var k=0;k<this.allDataExcel.length;k++) {
      downloadArray=Object.values(this.allDataExcel[k]);
      downloadArray.splice(0, 0, k+1);
      testArray.push(downloadArray);
	}

swal.fire({
  title: "Are you sure?",
  text: " You want to Download this page!",
  showConfirmButton: true,
  showCancelButton: true     
  })
  .then((willDelete) => {

  if(willDelete.value){
        const message ="Downloaded successfully!"
        this.generateSheet(testArray);
      swal.fire(message);

  }else{
    const message ="Download Rejected!"
     swal.fire(message);
  }

  
});


}

generateSheet(sheetData){
	/* generate worksheet */
	const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(sheetData);

	/* generate workbook and add the worksheet */
	const wb: XLSX.WorkBook = XLSX.utils.book_new();
	XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  
	/* save to file */
	XLSX.writeFile(wb, this.fileName);
}

onFileChange(evt: any,contentSmallAlert) {
  this.overlayRef.attach(this.LoaderComponentPortal);

  const target: DataTransfer = <DataTransfer>(evt.target);
  var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
  if (target.files.length !== 1) {
 // this.layoutUtilsService.showActionNotification(`Multiple files not allowed.`, MessageType.Update, 5000, true, true);
 this.overlayRef.detach();
 swal.fire('Multiple files not allowed.');
 return false;
  }
  if (validExts.indexOf(target.files[0]['type']) < 0)
  {
 //this.layoutUtilsService.showActionNotification(`Only file with extensions .xlsx, .xls, .csv is allowed.`, MessageType.Update, 5000, true, true);
 this.overlayRef.detach();
 swal.fire('Only file with extensions .xlsx, .xls, .csv is allowed.');
 return false;
 }
 else{
  var headerArray= [];

  const myClonedArray  = Object.assign([], this.createNewAry);
	headerArray=myClonedArray;
  headerArray.splice(0, 0, "S No");
  const reader: FileReader = new FileReader();
  reader.onload = (e: any) => {
  /* read workbook */
  const bstr: string = e.target.result;
  const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

  /* grab first sheet */
  const wsname: string = wb.SheetNames[0];
  const ws: XLSX.WorkSheet = wb.Sheets[wsname];

  /* save data */
  this.be = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
//  this.be.splice(0, 1);
 var result = this.arraysEqual(headerArray,this.be[0]);
  if(result===true){
  this.auth.uploadProducts(this.be).pipe(
    tap(result => {
      this.overlayRef.detach();
      this.ngOnInit();
      this.getpro();
      if(result['status']=="success"){
        if(result['failed_products'].length==0){
     // this.layoutUtilsService.showActionNotification(`Products successfully saved.`, MessageType.Update, 5000, true, true);
     swal.fire(
 
      'Save!',
      'Your records has been saved.',
      'success'
    )
    }
      else{
        let ids='';
        result['failed_products'].forEach((element,index) => {
          ids += String((index+1))+". "+element+"\n";
        });
        this.alertMessage='Following Product with Product IDs already exists.'+"\n"+ids;
       
        //this.openSmall(contentSmallAlert);
        this.common.openCentred(contentSmallAlert,'md');
      }
    }
    else{
    //  this.layoutUtilsService.showActionNotification(`Failed to upload. Please try again.`, MessageType.Update, 5000, true, true);
    this.overlayRef.detach();

      swal.fire('Failed to upload. Please try again.');
  }
    }),
    
    ).subscribe();
  }
  else{
  //  this.layoutUtilsService.showActionNotification(`Incorrect sheet uploaded. Please try again.`, MessageType.Update, 5000, true, true);
  this.overlayRef.detach();
  swal.fire('Incorrect Sheet Uploaded.Refer Sample Sheet!.');
  }
  
};
//return false;
reader.readAsBinaryString(target.files[0]);
 }
 evt.target.value = ''
}
 arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;
    for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

} //end of class


var ELEMENT_DATA1: any[] = [


];
