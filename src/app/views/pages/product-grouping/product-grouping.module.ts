import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTreeModule} from '@angular/material/tree';
//import { CdkTableModule } from '@angular/cdk';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTreeModule
 //   CdkTableModule
  ]
})
export class ProductGroupingModule { }
