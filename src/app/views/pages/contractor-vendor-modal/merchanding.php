<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MerchandingModel;
use App\storemodel;
use App\workmodel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class merchanding extends Controller
{
    // save the merchand
    public function store(Request $request)
    {
        $data=$request->json()->all();
      $geo_data=array("country"=>$data['country'],"state"=>$data['state'],"district"=>$data['district'],"taluk"=>$data['taluk'],"city"=>$data['city'],"pincode"=>$data['pincode'],"address1"=>$data['address_line1'],"address2"=>$data['address_line2']);
      $geo1_data=array("country"=>$data['c_country'],"state"=>$data['c_state'],"district"=>$data['c_district'],"taluk"=>$data['c_taluk'],"city"=>$data['c_town'],"pincode"=>$data['c_pincode'],"address1"=>$data['c_address'],"address2"=>$data['c_address1']);
       $save_mer=new MerchandingModel;
       $save_mer->user_id=$data['user_id'];
       $save_mer->f_name=$data['f_name'];
       $save_mer->l_name=$data['l_name'];
       $save_mer->mobile_no=$data['mobile_no'];
       $save_mer->alt_mobile_no=$data['alt_mobile_no'];
       $save_mer->email=$data['email'];
       $save_mer->p_geo_data=json_encode($geo_data);
       $save_mer->geo1_data=json_encode($geo1_data);
       $save_mer->category=json_encode($data['cate']);
       $save_mer->role_id=2;
       $save_mer->save();
       if($save_mer)
       {
           $result=1;
       }
       else
       {
        $result=0;   
       }
       
       return json_encode(array("result" => $result));
   
    }
    
    // get  the merchand

    public function get_all_merchand()
    {
        $get_data=MerchandingModel::get();
        return json_encode(array("result" => $get_data));
    }

    // delete functions
    public function delete(Request $request)
    {
        //
        $data=$request->json()->all();
        $del_data=MerchandingModel::where('user_id',$data['user_id'])->delete();
        if($del_data)
{
$result=1;
}
else
{
$result=0;
}

return json_encode(array("result" => $result));
    }

//bulk delte

    public function bulk_delete(Request $request)
    {
        //
        $data=$request->json()->all();
        foreach ($data as $dat){
            $del_data=MerchandingModel::where('id',$dat['id'])->delete();
        }
        if($del_data)
{
$result=1;
}
else
{
$result=0;
}
return json_encode(array("result" => $result));
    }


    //editing merchand manger
    public function edit_merchand(Request $request)
    {
        //
        $data=$request->json()->all();
         $geo_data=array("country"=>$data['country'],"state"=>$data['state'],"district"=>$data['district'],"taluk"=>$data['taluk'],"city"=>$data['city'],"pincode"=>$data['pincode'],"address1"=>$data['address_line1'],"address2"=>$data['address_line2']);
      $geo1_data=array("country"=>$data['c_country'],"state"=>$data['c_state'],"district"=>$data['c_district'],"taluk"=>$data['c_taluk'],"city"=>$data['c_town'],"pincode"=>$data['c_pincode'],"address1"=>$data['c_address'],"address2"=>$data['c_address1']);
$edit_data=MerchandingModel::where('user_id',$data['user_id'])->update([
    'f_name'=>$data['f_name'],
    'l_name'=>$data['l_name'],
    'mobile_no'=>$data['mobile_no'],
    'email'=>$data['email'],
    'p_geo_data'=>json_encode($geo_data),
    'geo1_data'=>json_encode($geo1_data),
    'category'=>json_encode($data['cate']),
   
    'alt_mobile_no'=>$data['alt_mobile_no']
    ]);

 if($edit_data)
{
$result=1;
}
else
{
$result=0;
}
return json_encode(array("result" => $result));
}
public function save_store_inspector (Request $request)
{
    $data=$request->json()->all();
    $geo_data=array("country"=>$data['country'],"state"=>$data['state'],"district"=>$data['district'],"taluk"=>$data['taluk'],"city"=>$data['city'],"pincode"=>$data['pincode'],"address1"=>$data['address_line1'],"address2"=>$data['address_line2']);
     $save_mer=new MerchandingModel;
     $save_mer->user_id=$data['user_id'];
     $save_mer->f_name=$data['f_name'];
     $save_mer->l_name=$data['l_name'];
     $save_mer->mobile_no=$data['mobile_no'];
     $save_mer->alt_mobile_no=$data['alt_mobile_no'];
     $save_mer->email=$data['email'];
     $save_mer->p_geo_data=json_encode($geo_data);
     $save_mer->category=json_encode($data['manger']);
     $save_mer->role_id=3;
     $save_mer->save();
     if($save_mer)
     {
         $result=1;
     }
     else
     {
      $result=0;   
     }
     
     return json_encode(array("result" => $result));
}
public function edit_store_inspector(Request $request)
{
    //
    $data=$request->json()->all();
     $geo_data=array("country"=>$data['country'],"state"=>$data['state'],"district"=>$data['district'],"taluk"=>$data['taluk'],"city"=>$data['city'],"pincode"=>$data['pincode'],"address1"=>$data['address_line1'],"address2"=>$data['address_line2']);
 
$edit_data=MerchandingModel::where('user_id',$data['user_id'])->update([
'f_name'=>$data['f_name'],
'l_name'=>$data['l_name'],
'mobile_no'=>$data['mobile_no'],
'email'=>$data['email'],
'p_geo_data'=>json_encode($geo_data),

'category'=>json_encode($data['manger']),

'alt_mobile_no'=>$data['alt_mobile_no']
]);

if($edit_data)
{
$result=1;
}
else
{
$result=0;
}
return json_encode(array("result" => $result));
}
public function bulk_upload_merchand(Request $request)
{
    $data=$request->json()->all();
    //print_r(sizeof($data['store_cat']));
    $i=0;
    $duplicate=array();
    if(sizeof($data['b_data'])>0) 
    {
    
     foreach($data['b_data'] as $dat)
     { 
        $categoy1=array();
         $check=MerchandingModel::where('user_id',$dat['user_id'])->count();
         if($check ==0)
        {
            
            array_push($duplicate,$dat['user_id']);
           
          foreach($data['store_cat'][$i] as $sat)
          {

           
              $categoy=storemodel::select('category','store_name')->where('store_name',$sat)->get();
              if(count($categoy)==0)
              {
                $result=['status'=>'not_store','message'=>$duplicate,'code'=>500];
                $check=1;
              }
              else
              {
              array_push($categoy1,$categoy);
              }
          }
  
       $collection = collect($categoy1);
       $collapsed = $collection->collapse();
       $collapsed->all();
        $save_mer=new MerchandingModel;
        $save_mer->user_id=$dat['user_id'];
        $save_mer->f_name=$dat['f_name'];
        $save_mer->l_name=$dat['l_name'];
        $save_mer->mobile_no=$dat['mobile_no'];
        $save_mer->alt_mobile_no=$dat['alt_mobile_no'];
        $save_mer->email=$dat['E-mail'];
        $save_mer->geo1_data=json_encode($data['b_geo_data'][$i]);
        $save_mer->p_geo_data=json_encode($data['p_geo_data'][$i]);
        $save_mer->category=$collapsed;
        $save_mer->role_id=2;
        $save_mer->save();
        $i++;  
        if($save_mer)
        {
            $result=['status'=>'sucess','message'=>$dat['user_id'],'code'=>200];
        }
    }
else
{
    array_push($duplicate,$dat['user_id']);
   
    $result=['status'=>'duplicate','message'=>$duplicate,'code'=>500];
}
 }
}
else
{
    $result=['status'=>'Empty','message'=>$duplicate,'code'=>500];
}
 return json_encode($result);
}

// save contractor
public function store_contractor(Request $request)
    {
        $data=$request->json()->all();
      $geo_data=array("country"=>$data['country'],"state"=>$data['state'],"district"=>$data['district'],"taluk"=>$data['taluk'],"city"=>$data['city'],"pincode"=>$data['pincode'],"address1"=>$data['address_line1'],"address2"=>$data['address_line2']);
      $geo1_data=array("country"=>$data['c_country'],"state"=>$data['c_state'],"district"=>$data['c_district'],"taluk"=>$data['c_taluk'],"city"=>$data['c_town'],"pincode"=>$data['c_pincode'],"address1"=>$data['c_address'],"address2"=>$data['c_address1']);
       $save_mer=new MerchandingModel;
       $save_mer->user_id=$data['user_id'];
       $save_mer->f_name=$data['f_name'];
       $save_mer->l_name=$data['l_name'];
       $save_mer->mobile_no=$data['mobile_no'];
       $save_mer->alt_mobile_no=$data['alt_mobile_no'];
       $save_mer->email=$data['email'];
       $save_mer->p_geo_data=json_encode($geo_data);
       $save_mer->geo1_data=json_encode($geo1_data);
       $save_mer->category=json_encode($data['cate']);
       $save_mer->role_id=4;
       $save_mer->save();
       if($save_mer)
       {
           $result=1;
       }
       else
       {
        $result=0;   
       }
       
       return json_encode(array("result" => $result));
   
    }
    public function edit_contractor(Request $request)
    {
        //
        $data=$request->json()->all();
         $geo_data=array("country"=>$data['country'],"state"=>$data['state'],"district"=>$data['district'],"taluk"=>$data['taluk'],"city"=>$data['city'],"pincode"=>$data['pincode'],"address1"=>$data['address_line1'],"address2"=>$data['address_line2']);
      $geo1_data=array("country"=>$data['c_country'],"state"=>$data['c_state'],"district"=>$data['c_district'],"taluk"=>$data['c_taluk'],"city"=>$data['c_town'],"pincode"=>$data['c_pincode'],"address1"=>$data['c_address'],"address2"=>$data['c_address1']);
$edit_data=MerchandingModel::where('user_id',$data['user_id'])->update([
    'f_name'=>$data['f_name'],
    'l_name'=>$data['l_name'],
    'mobile_no'=>$data['mobile_no'],
    'email'=>$data['email'],
    'p_geo_data'=>json_encode($geo_data),
    'geo1_data'=>json_encode($geo1_data),
    'category'=>json_encode($data['cate']),
   
    'alt_mobile_no'=>$data['alt_mobile_no']
    ]);

 if($edit_data)
{
$result=1;
}
else
{
$result=0;
}
return json_encode(array("result" => $result));
}
public function bulk_upload_contractor(Request $request)
{
    $data=$request->json()->all();
    $i=0;
    $duplicate=array();
    if(sizeof($data['b_data'])>0 && sizeof($data['store_cat'])>0 && sizeof($data['p_geo_data']>0)) 
    {
     foreach($data['b_data'] as $dat)
     { 
        $categoy1=array();
         $check=MerchandingModel::where('user_id',$dat['user_id'])->count();
         if($check ==0)
        {
            
            array_push($duplicate,$dat['user_id']);
           
          foreach($data['store_cat'][$i] as $sat)
          {

           
              $categoy=workmodel::select('category','work_name')->where('work_name',$sat)->get();
              if(count($categoy)==0)
              {
                $result=['status'=>'not_store','message'=>$duplicate,'code'=>500];
                $check=1;
              }
              else
              {
              array_push($categoy1,$categoy);
              }
          }
  
       $collection = collect($categoy1);
       $collapsed = $collection->collapse();
       $collapsed->all();
        $save_mer=new MerchandingModel;
        $save_mer->user_id=$dat['user_id'];
        $save_mer->f_name=$dat['f_name'];
        $save_mer->l_name=$dat['l_name'];
        $save_mer->mobile_no=$dat['mobile_no'];
        $save_mer->alt_mobile_no=$dat['alt_mobile_no'];
        $save_mer->email=$dat['E-mail'];
        $save_mer->geo1_data=json_encode($data['b_geo_data'][$i]);
        $save_mer->p_geo_data=json_encode($data['p_geo_data'][$i]);
        $save_mer->category=$collapsed;
        $save_mer->role_id=4;
        $save_mer->save();
        $i++;  
        if($save_mer)
        {
            $result=['status'=>'sucess','message'=>$dat['user_id'],'code'=>200];
        }
    }
else
{
    array_push($duplicate,$dat['user_id']);
   
    $result=['status'=>'duplicate','message'=>$duplicate,'code'=>500];
}
 }
}
else
{
    $result=['status'=>'Empty','message'=>$duplicate,'code'=>500];
}

 return json_encode($result);
}
}