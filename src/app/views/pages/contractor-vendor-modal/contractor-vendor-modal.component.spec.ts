import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorVendorModalComponent } from './contractor-vendor-modal.component';

describe('ContractorVendorModalComponent', () => {
  let component: ContractorVendorModalComponent;
  let fixture: ComponentFixture<ContractorVendorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorVendorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorVendorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
