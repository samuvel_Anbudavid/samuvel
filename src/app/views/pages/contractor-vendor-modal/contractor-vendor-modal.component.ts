import { AfterViewInit, Component, ElementRef, OnInit, ViewChild,Input ,Output,EventEmitter} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../core/auth';
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import csc from 'country-state-city';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
//import{UserMasterComponent}from'../user-master/user-master.component';
//import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import Swal from 'sweetalert2';
@Component({
  selector: 'kt-contractor-vendor-modal',
  templateUrl: './contractor-vendor-modal.component.html',
  styleUrls: ['./contractor-vendor-modal.component.scss']
})
export class ContractorVendorModalComponent implements OnInit {
 
 @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
@ViewChild('sort1', {static: true}) sort: MatSort;
 @ViewChild('wizard', {static: true}) el: ElementRef;
 @Input() edit_id:User;
 saved: EventEmitter<any> = new EventEmitter();
 //@ViewChild('UserMasterComponent',{static:true})private child: UserMasterComponent;
 private merchand:FormGroup;
 private grade:number;
private emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
private phonePattern: RegExp = /^[0-9-+]{10,12}$/;
submitted = false;
country:any;
state:any;
district:any;
country_geo:any;
state_geo:any;
district_geo:any;
taluk_geo:any;
city:any;
disable:boolean;
taluk:any;
dropdownList = [];
  selectedItems = [];
  dropdownSettings :IDropdownSettings= {};
Edit_enable:boolean;
save_enable:boolean;
overlayRef: OverlayRef; 
allDataExcel:any=[];
	duplicated: boolean;
	duplicated1: boolean;
	duplicated2: boolean;
	dd_selected: boolean;

constructor(private modalService: NgbModal,private fb: FormBuilder, private auth: AuthService,private overlay: Overlay,public activeModal: NgbActiveModal) {
	}

	ngOnInit() {
	this.dd_selected=true;
	this.save_enable=false;
	this.Edit_enable=true;
	this.initRegistrationForm();
	this.disable=true;
	this.use_data();
	this.country=csc.getAllCountries();
	this.country_geo=csc.getAllCountries();
 var data = "";
 var method = "post";
 var url = "get_all_work";
 this.auth.servicepost(data, method, url, 'application/json')
    .subscribe(data => {
  this.dropdownList = data.result; 
	}
      
  );  
	//	this.dropdownList = this.category;
	this.selectedItems = [ ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'category',
      textField: 'work_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
	//Edit data come  form parent of UserMasterComponent
 if(this.edit_id)
{ this.dd_selected=false;
	this.edit_merchand();
	this.save_enable=true;
	this.Edit_enable=false;
}

  

}
  getstate_geo(country_id)
  {
	this.merchand.get('c_state').reset(); 
	this.merchand.get('c_state').enable();

	this.merchand.get('c_district').reset();
	this.merchand.get('c_district').disable();

	this.merchand.get('c_taluk').reset();
	this.merchand.get('c_taluk').disable();

	this.merchand.get('c_town').reset();
	this.merchand.get('c_town').disable();

	this.merchand.get('c_pincode').reset();
	this.merchand.get('c_pincode').disable();
	  setTimeout(() => {
     this.state_geo=csc.getStatesOfCountry(country_id);
     });
  }
  
   getdistrict_geo(state_id)
  {
	this.merchand.get('c_district').enable();

	this.merchand.get('c_taluk').reset();
	this.merchand.get('c_taluk').disable();

	this.merchand.get('c_town').reset();
	this.merchand.get('c_town').disable();
	  setTimeout(() => { 
	
     if(state_id<=41)
     {
		
    this.district_geo=csc.getDistrictById(state_id);
    console.log(this.district);
    console.log(state_id);
     }
     else
     {
		 
		
      this.city=csc.getCitiesOfState(state_id);
    
     }
	  });
  }
  
 gettaluk_geo(district)
  {
	this.merchand.get('c_taluk').enable();

	this.merchand.get('c_town').reset();
	this.merchand.get('c_town').enable();

	this.merchand.get('c_pincode').reset();
	this.merchand.get('c_pincode').enable();
	
	   setTimeout(() => {
	// this.taluk=csc.getTalukOfState(district2);
  this.taluk_geo=csc.getTalukOfDistrict (district);
  console.log(this.taluk);
	   });
  }
	
//personalbar
	 getstate(country_id)
  {
	this.merchand.get('state').reset(); 
	this.merchand.get('state').enable();

	this.merchand.get('district').reset();
	this.merchand.get('district').disable();

	this.merchand.get('taluk').reset();
	this.merchand.get('taluk').disable();

	this.merchand.get('city').reset();
	this.merchand.get('city').disable();

	this.merchand.get('pincode').reset();
	this.merchand.get('pincode').disable();
	   setTimeout(() => {
     this.state=csc.getStatesOfCountry(country_id);
	   });
  }

   getdistrict(state_id)
  {
	this.merchand.get('district').enable();

	this.merchand.get('taluk').reset();
	this.merchand.get('taluk').disable();

	this.merchand.get('city').reset();
	this.merchand.get('city').disable();

	this.merchand.get('pincode').reset();
	this.merchand.get('pincode').disable();

	   setTimeout(() => {
	  
	
     if(state_id<=41)
     {
		
    this.district=csc.getDistrictById(state_id);
    console.log(this.district);
    console.log(state_id);
     }
     else
     {
		 
		
      this.city=csc.getCitiesOfState(state_id);
    
     }
	   });
  }

 gettaluk(district)
  {
	   //	console.log(district['id']);
	   this.merchand.get('taluk').enable();

	   this.merchand.get('city').reset();
	   this.merchand.get('city').enable();

	   this.merchand.get('pincode').reset();
	   this.merchand.get('pincode').enable();
	   setTimeout(() => {
	// this.taluk=csc.getTalukOfState(district2);
  this.taluk=csc.getTalukOfDistrict (district);
	   console.log(this.taluk)});
  }
  
	initRegistrationForm() {

		this.merchand = this.fb.group({
		  user_id: ['', Validators.compose([
		 Validators.required
	   ])
	  ],
	  f_name: ['', Validators.compose([
		Validators.required
			  ])
	 ],
	 l_name: ['', Validators.compose([
		Validators.required
	])
	],
	mobile_no: ['', Validators.compose([
	  Validators.required,
	  Validators.pattern(this.phonePattern)
	  
	])
	],
	email: ['', Validators.compose([
	  Validators.required,
	  //Validators.pattern(this.emailPattern)
	  Validators.email
	])
	],
	address_line1: ['', Validators.compose([
	  Validators.required,
	
	])
	],
	address_line2: ['', Validators.compose([
	  Validators.required,
	 
	])
	],
	city: ['', Validators.compose([
	  Validators.required,
	 
	])
	],
	state: ['', Validators.compose([
		Validators.required,
		
	  ])
	  ],
	country: ['', Validators.compose([
	  Validators.required,
	  
	])
	],
	pincode: ['', Validators.compose([
		Validators.required,
		
	  ])
	  ],
	 c_country:['',Validators.compose([
	 Validators.required
		])
		],
	 c_state:['',Validators.compose([
	 Validators.required
		])
		],
		 c_district:['',Validators.compose([
	 Validators.required
		])
		],
		 c_taluk:['',Validators.compose([
	 Validators.required
		])
		],
		 c_town:['',Validators.compose([
	 Validators.required
		])
		],
		c_address:['',Validators.compose([
	 Validators.required
		])
		],
		c_address1:['',Validators.compose([
	 Validators.required
		])
		],
		alt_mobile_no: ['', Validators.compose([
	 
	  Validators.pattern(this.phonePattern)
	  
	])
	],
	 taluk:['',Validators.compose([
	 Validators.required
		])
		],
		 c_pincode:['',Validators.compose([
	 Validators.required
		])
		],
		 district:['',Validators.compose([
	 Validators.required
		])
		],
		
			cate:['',Validators.compose([
	 Validators.required
		])
		],
	 
	});
	 
	}
	
	isControlHasError(controlName: string, validationType: string): boolean {
	  const control = this.merchand.controls[controlName];
	  if (!control) {
		return false;
	  }
	
	  const result = control.hasError(validationType) && (control.dirty || control.touched);
	  return result;
	}


	ngAfterViewInit(): void {
		// const wizard = new KTWizard(this.el.nativeElement, {
		// 	startStep: 1
		// });
		
	//const refresh= new UserMasterComponent();

	}

	onSubmit_next() {
		this.submitted = true;
		const wizard = new KTWizard(this.el.nativeElement, {
			startStep: 1
			  });
		const controls = this.merchand.controls;
		const firstPageControls={
			"user_id":FormControl,
			"f_name":FormControl,
			"l_name":FormControl,
			"email":FormControl,
			"mobile_no":FormControl,
			"address_line1":FormControl,
			"address_line2":FormControl,
			"city":FormControl,
			"state":FormControl,
			"country":FormControl,
			"pincode":FormControl,
			"district":FormControl,
			"taluk":FormControl
		
		  }
const secondPageControls={
			"c_address":FormControl,
			"c_address1":FormControl,
			"c_country":FormControl,
			"c_district":FormControl,
			"c_state":FormControl,
			"c_taluk":FormControl,
			"c_town":FormControl,
			"c_pincode":FormControl
		
		  }
		   
		    this.grade=wizard.currentStep; 
		  console.log(this.grade);
		  
		  switch (this.grade) {
			
            case 1: {
				
					 if(controls['user_id'].value==null || controls['user_id'].value=="" ||
				controls['f_name'].value==null || controls['f_name'].value=="" ||
				 controls['l_name'].value==null || controls['l_name'].value=="" ||
				 controls['pincode'].value==null || controls['pincode'].value=="" ||
				 controls['mobile_no'].value==null || controls['mobile_no'].value=="" ||
				 controls['email'].value==null || controls['email'].value=="" ||
				 controls['address_line1'].value==null || controls['address_line1'].value=="" ||
				 controls['address_line2'].value==null || controls['address_line2'].value=="" ||
				 controls['country'].value==null || controls['country'].value=="" ||
				 controls['state'].value==null || controls['state'].value=="" ||
				 controls['city'].value==null || controls['city'].value=="" || this.duplicated==true || this.duplicated1==true || this.duplicated2 ==true ){ 
					  Object.keys(firstPageControls).forEach(controlName =>
                        controls[controlName].markAsTouched()
                    );
                    // wizard.on('beforeNext', function(wizardObj) {
					// 	wizardObj.goTo(1);
                    //  //wizardObj.start();
					wizard.stop();
                    // });
                  
				 }
				 else
				 			{   
								// wizard.on('beforeNext', function(wizardObj) {
								// 	wizardObj.start();
								// 	wizardObj.goTo(2);
								//   });
								  
								//   console.log('hi1');
								wizard.goTo(2);
								wizard.start();
								
							 }
							
		 }

break;
case 2: {

		
	if(controls['c_country'].value==null || controls['c_country'].value=="" ||
			controls['c_address'].value==null || controls['c_address'].value=="" ||
			 controls['c_district'].value==null || controls['c_district'].value=="" ||
			 controls['c_taluk'].value==null || controls['c_taluk'].value=="" ||
			 controls['c_town'].value==null || controls['c_town'].value=="" ||
			 controls['c_state'].value==null || controls['c_state'].value=="" ||
			 controls['c_address1'].value==null || controls['c_address1'].value==""
				){ 
			
					Object.keys(secondPageControls).forEach(controlName =>
								   controls[controlName].markAsTouched()
								 );
								 wizard.stop();
		
			
				}
				else
				{
					// wizard.on('beforeNext', 	function(wizardObj) {
					// 	wizardObj.start();
					// 	wizardObj.goTo(2);
					//   });
					wizard.start();
				}
			}
			
		 }
	}

	onSubmit() {
		
if(this.merchand.invalid)
{
	const wizard = new KTWizard(this.el.nativeElement, {
			startStep: 1  });
	Swal.fire(
       'Error!',
       'Please Check ',
       'error'
     )
}
else
{
console.log(this.merchand.value);
 
	this.activeModal.dismiss(this.merchand.value);
	this.modalService.dismissAll();
}
	}
	reset()
	{
		this.modalService.dismissAll();
	}
	
	edit_merchand()
	{
		this.selectedItems = JSON.parse(this.edit_id.category);
		 var geo_data=JSON.parse(this.edit_id.p_geo_data);
		 var geo1_data=JSON.parse(this.edit_id.geo1_data);
		this.merchand = this.fb.group({
			id:[this.edit_id.id],
		  user_id: [  this.edit_id.user_id, Validators.compose([
		 Validators.required
	   ])
	  ],
	  f_name: [this.edit_id.f_name, Validators.compose([
		Validators.required
			  ])
	 ],
	 l_name: [this.edit_id.l_name, Validators.compose([
		Validators.required
	])
	],
	mobile_no: [this.edit_id.mobile_no, Validators.compose([
	  Validators.required,
	  Validators.pattern(this.phonePattern)
	  
	])
	],
	email: [this.edit_id.email, Validators.compose([
	  Validators.required,
	  //Validators.pattern(this.emailPattern)
	  Validators.email
	])
	],
	address_line1: [geo_data.address1, Validators.compose([
	  Validators.required,
	
	])
	],
	address_line2: [geo_data.address2, Validators.compose([
	  Validators.required,
	 
	])
	],
	city: [geo_data.city, Validators.compose([
	  Validators.required,
	 
	])
	],
	state: [geo_data.state, Validators.compose([
		Validators.required,
		
	  ])
	  ],
	country: [geo_data.country, Validators.compose([
	  Validators.required,
	  
	])
	],
	pincode: [geo_data.pincode, Validators.compose([
		Validators.required,
		
	  ])
	  ],
	 c_country:[geo1_data.country,Validators.compose([
	 Validators.required
		])
		],
	 c_state:[geo1_data.state,Validators.compose([
	 Validators.required
		])
		],
		 c_district:[geo1_data.district,Validators.compose([
	 Validators.required
		])
		],
		 c_taluk:[geo1_data.taluk,Validators.compose([
	 Validators.required
		])
		],
		 c_town:[geo1_data.city,Validators.compose([
	 Validators.required
		])
		],
		c_address:[geo1_data.address1,Validators.compose([
	 Validators.required
		])
		],
		c_address1:[geo1_data.address2,Validators.compose([
	 Validators.required
		])
		],
		c_pincode:[geo1_data.pincode,Validators.compose([
	 Validators.required
		])
		],
		district:[geo_data.district,Validators.compose([
	 Validators.required
		])
		],
		taluk:[geo_data.taluk,Validators.compose([
	 Validators.required
		])
		],
		alt_mobile_no:[this.edit_id.alt_mobile_no,Validators.compose([
	  Validators.pattern(this.phonePattern)
		])
		],
			cate:[this.selectedItems[0],Validators.compose([
	 Validators.required
		])
		]
		});
	this.district=csc.getDistrictById(geo_data.state);
	this.taluk=csc.getTalukOfDistrict(geo_data.district);
	this.state=csc.getStatesOfCountry(geo_data.country);
	this.district_geo=csc.getDistrictById(geo1_data.state);
	this.taluk_geo=csc.getTalukOfDistrict(geo1_data.district);
	this.state_geo=csc.getStatesOfCountry(geo1_data.country);
	}
	
	onEdit()
	{
	
if(this.merchand.invalid)
{
	Swal.fire(
       'Error!',
       'Please Check ',
       'error'
     )
}
else
{
	
 
	 this.activeModal.dismiss(this.merchand.value);
	this.modalService.dismissAll();
	
}
	}
	use_data()
	{
		var data = "";
 var method = "post";
 var url = "get_merchand";
 this.auth.servicepost(data, method, url, 'application/json')
   .subscribe(data => {
	   for(let c of data.result){
		   if(c.role_id == 4)
		   {
	  ELEMENT_DATA.push(c);
   this.allDataExcel=ELEMENT_DATA;
	
 
		   }
		
	}
   });
	}
   // duplicate  
applyFilter(filterValue: string) {
   filterValue = filterValue.trim(); // Remove whitespace
   filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
   var sample= this.allDataExcel.filter(question => question.user_id.toLowerCase()== filterValue ) ;
    
   if(this.edit_id!=null  && sample.length>0)
   {
   if(sample.length>0 && sample[0]['user_id'].toLowerCase() ==this.edit_id.user_id.toLowerCase())
   {
	   console.log(sample);
	this.duplicated =false;
   }
   else{this.duplicated =true;}
}
else
{
   if(sample.length >0)
   {
	   console.log(sample);
	this.duplicated =true;
   }
   else{this.duplicated =false;}
}
 }
 applyFilter1(filterValue: string) {
   filterValue = filterValue.trim(); // Remove whitespace
   filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
   var sample= this.allDataExcel.filter(question => question.email.toLowerCase() == filterValue ) ;
   
   if(this.edit_id!=null  && sample.length>0)
   {
   if(sample.length>0 && sample[0]['email'].toLowerCase() ==this.edit_id.email.toLowerCase())
   {
	   console.log(sample);
	this.duplicated1 =false;
   }
   else{this.duplicated1 =true;}
}
else
{
   if(sample.length >0)
   {
	   console.log(sample);
	this.duplicated1 =true;
   }
   else{this.duplicated1 =false;}
}
 }
 applyFilter2(filterValue: string) {
   filterValue = filterValue.trim(); // Remove whitespace
   filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
   var sample= this.allDataExcel.filter(question => question.mobile_no== filterValue ) ;
   if(this.edit_id!=null  && sample.length>0)
   {
   if(sample.length>0 && sample[0]['mobile_no'] ==this.edit_id.mobile_no)
   {
	   console.log(sample);
	this.duplicated2 =false;
   }
   else{this.duplicated2 =true;}
}
else
{
   if(sample.length >0)
   {
	   console.log(sample);
	this.duplicated2 =true;
   }
   else{this.duplicated2 =false;}
}
 }
}
//interface  data structure
export interface User {
	id:number;
  user_id: string;
    f_name: string;
	l_name:string;
    p_address:string;
    c_address:string;
    email:string;
	mobile_no:string;
	address_1:string;
	address_2:string;
	city:string;
	state:string;
	country:string;
	c_country:string;
	c_state:string;
	c_district:string;
	c_taluk:string;
	c_town:string;
	pincode:string;
category:string;
alt_mobile_no:string;
district:string;
taluk:string;
c_address1:string;
c_pincode:string;
geo1_data:string;
p_geo_data:string;
  }
var ELEMENT_DATA= [];