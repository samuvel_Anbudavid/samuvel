import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchandMenuComponent } from './merchand-menu.component';

describe('MerchandMenuComponent', () => {
  let component: MerchandMenuComponent;
  let fixture: ComponentFixture<MerchandMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchandMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchandMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
