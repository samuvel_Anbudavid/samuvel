import { Component, OnInit, ViewChild } from '@angular/core';
import {StoreInspectorMasterComponent}from'../store-inspector-master/store-inspector-master.component';
@Component({
  selector: 'kt-merchand-menu',
  templateUrl: './merchand-menu.component.html',
  styleUrls: ['./merchand-menu.component.scss'],
  
})
export class MerchandMenuComponent implements OnInit {
@ViewChild(StoreInspectorMasterComponent,{static:true}) private _child: 
      StoreInspectorMasterComponent;
  constructor() { }
private store_inspector:StoreInspectorMasterComponent
  ngOnInit() {
  }
 store_inspector_call()
 {
	 this._child.get_merchand();
	
 }
}