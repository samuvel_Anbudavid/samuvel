import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../core/auth';
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import { TestingComponent } from '../testing/testing.component'
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import csc from 'country-state-city';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';
import { CommonSerService } from '../../../core/commonser';
import { DatePipe } from '@angular/common';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
import swal from 'sweetalert2';
import _remove from 'lodash/remove';
import { environment } from '../../../../environments/environment';
import { from } from 'rxjs';
//import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

type AOA = any[][];
// Data Source Merge
const mergeById = (array1, array2) =>
  array1.map(itm => ({
    ...array2.find((item) => (item.id === itm.id) && item),
    ...itm
  }));
const mergeById1 = (array1, array2) =>
  array1.map(itm => ({
    ...array2.find((item) => (item.s_no === itm.s_no) && item),
    ...itm
  }));

@Component({
  selector: 'kt-view-reports',
  templateUrl: './view-reports.component.html',
  styleUrls: ['./view-reports.component.scss']
})
export class ViewReportsComponent implements OnInit {
  //array merege

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild('sort1', { static: true }) sort: MatSort;
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  @ViewChild('carousel', { static: false }) carousel: NgbCarousel;
  private displayedColumns = ['s_no', 'audit_id', 'retail_id', 'start_date', 'end_date', 'branding_elements', 'promo', 'planogram', 'check_list'];
  private displayedColumnsBrandingElements = ['s_no', 'name', 'material', 'dimensions', 'measurement', 'uom', 'actual_quantity', 'quantity', 'comments', 'image'];
  private displayedColumnsPromo = ['s_no', 'product_category', 'name', 'actual_quantity', 'quantity', 'comments', 'image'];
  private displayedColumnsProduct = [];
  private displayedColumnsCheckList = ['s_no', 'question', 'ans_category', 'complaince/desc', 'image'];
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  private typesList = [];
  // Datasource
  private dataSource;
  private dataSourceBE;
  private dataSourcePromo;
  private dataSourceProductsDisplay;
  private dataSourceCheckList;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;
  private RawData: any;
  private RawData_checks: any;
  private RawData_pro: any;
  private RawData_diplay: any[];
  private selection;
  private selectionBE;
  private modalTableTitle: String;
  private allData: any;
  private loader: String = "./assets/media/loader/loader.gif";
  images: any;
  imgName: string;
  imgNumber: number = 1
  imgLength: number;
  belowImageData;

  private data: any = [];
  private selectionPromo;
  private selectionCheck;
  private selectionPalno;
  isLoading: boolean;
  filteredItems: any[];
  modalTableTitle1: string;
  filteredItems1: any[];
  constructor(private modalService: NgbModal, private fb: FormBuilder, private cdr: ChangeDetectorRef, private auth: AuthService, private overlay: Overlay, private layoutUtilsService: LayoutUtilsService, private commonSer: CommonSerService, private datePipe: DatePipe) {
    this.dataSource = new MatTableDataSource();
    this.dataSourceBE = new MatTableDataSource();
    this.dataSourcePromo = new MatTableDataSource();
    this.dataSourceProductsDisplay = new MatTableDataSource();
    this.dataSourceCheckList = new MatTableDataSource();
    this.isLoading = true;

    this.selection = new SelectionModel();
    this.selectionBE = new SelectionModel();
    this.selectionPromo = new SelectionModel();
    this.selectionPalno = new SelectionModel();
    this.selectionCheck = new SelectionModel();

    this.belowImageData = JSON.parse(JSON.stringify(this.data));
    this.belowImageData.splice(0, 1);
  }

  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.getReportList();
    this.getAuditTypesList();
    // this.overlayRef.detach();

  }

  getReportList() {
    //this.overlayRef.attach(this.LoaderComponentPortal);
    console.log(this.dataSource)
    const url = 'view_reports';
    this.auth.serviceget(url).pipe(
      //	this.auth.getRetailList().pipe(
      tap(result => {
        //consolelog(result);
        this.dataSource.data = result['result'];
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.allData = result['result'];
        //this.cdr.detectChanges();		
        // Selection Model
        this.selection = new SelectionModel(true, this.dataSource.data.filter(t => t.IsAssigned));
        this.selectionBE = new SelectionModel(true, this.dataSourceBE.data.filter(t => t.IsAssigned));
        this.selectionPromo = new SelectionModel(true, this.dataSourcePromo.data.filter(t => t.IsAssigned));
        this.selectionPalno = new SelectionModel(true, this.dataSourceProductsDisplay.data.filter(t => t.IsAssigned));
        this.selectionCheck = new SelectionModel(true, this.dataSourceCheckList.data.filter(t => t.IsAssigned));
      }),


    ).subscribe();
    //this.overlayRef.detach();
    setTimeout(() => {
      this.overlayRef.detach();
    }, 3000);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  // Modal Box 
  openLarge(content, size) {
    //consolelog(size);
    this.modalService.open(content, { windowClass: "myCustomModalClass", centered: true });
  }
  openmodal(content) {

    this.modalService.open(content, { size: "md", centered: true })
  }
  // Selected Value
  fetchRetails() {
    const messages = [];
    const sNos = [];
    this.selection.selected.forEach(element => {
      sNos.push(element['s_no']);
    });


  }

  masterToggle() {
    if (this.selection.selected.length === this.dataSource.data.length) {
      this.selection.clear();
    } else {
      this.dataSource.data.forEach(row => this.selection.select(row));
    }
  }
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  // Select in Branding Elements
  masterToggleBE() {
    console.log(this.selectionBE.selected.length);
    if (this.selectionBE.selected.length === this.dataSourceBE.data.length) {
      this.selectionBE.clear();
    } else {
      this.dataSourceBE.data.forEach(row => this.selectionBE.select(row));
    }
  }
  isAllSelectedBE(): boolean {
    const numSelected = this.selectionBE.selected.length;
    const numRows = this.dataSourceBE.data.length;
    return numSelected === numRows;
  }

  // Select in Promo
  masterTogglePromo() {
    console.log(this.selectionPromo.selected.length);
    console.log(this.dataSourcePromo.data.length);
    if (this.selectionPromo.selected.length === this.dataSourcePromo.data.length) {
      this.selectionPromo.clear();
    } else {
      this.dataSourcePromo.data.forEach(row => this.selectionPromo.select(row));
    }
  }
  isAllSelectedPromo(): boolean {
    const numSelected = this.selectionPromo.selected.length;
    const numRows = this.dataSourcePromo.data.length;
    return numSelected === numRows;
  }
  // Select in Product
  masterTogglePalno() {
    console.log(this.selectionPalno.selected.length);
    if (this.selectionPalno.selected.length === this.dataSourceProductsDisplay.data.length) {
      this.selectionPalno.clear();
    } else {
      this.dataSourceProductsDisplay.data.forEach(row => this.selectionPalno.select(row));
    }
  }
  isAllSelectedPalno(): boolean {
    const numSelected = this.selectionPalno.selected.length;
    const numRows = this.dataSourceProductsDisplay.data.length;
    return numSelected === numRows;
  }

  //// Select in check
  masterToggleCheck() {
    console.log(this.selectionCheck.selected.length);
    if (this.selectionCheck.selected.length === this.dataSourceCheckList.data.length) {
      this.selectionCheck.clear();
    } else {
      this.dataSourceCheckList.data.forEach(row => this.selectionCheck.select(row));
    }
  }
  isAllSelectedCheck(): boolean {
    const numSelected = this.selectionCheck.selected.length;
    const numRows = this.dataSourceCheckList.data.length;
    return numSelected === numRows;
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  // Audit Filter

  getAuditTypesList() {
    const url = 'get_all_audit_types';
    this.auth.serviceget(url).pipe(

      tap(result => {
        console.log(result);
        this.typesList = result['result'];

      }),

    ).subscribe();
    //this.overlayRef.detach();
  }


  typeChanged(e) {

    console.log(e.target.selectedIndex);
    var filteredArray = [];
    if (e.target.selectedIndex !== 0) {
      filteredArray = this.allData.filter(function (itm) {

        return itm.audit_list[0]['audit_type'] === e.target.selectedIndex;
      });


    }
    else {
      filteredArray = this.allData;
    }
    this.dataSource.data = filteredArray;
    this.cdr.detectChanges();

  }
  //  Modal-view

  viewBEInfo(index,modalcategory) {
    this.selectionBE.clear();
    this.modalTableTitle = 'Branding Elements';
    this.modalTableTitle1 =modalcategory ;
    console.log(this.dataSource.data[0]);
    var test = this.dataSource.data[index]['branding_elements'];
      this.RawData = JSON.parse(test);
    // .filter(({action_required}) => action_required === 1);
    this.dataSourceBE.data = this.commonSer.iterator(this.RawData, this.currentPage, this.pageSize)
    this.totalSize = this.dataSourceBE.data.length;

  }

  viewCheckListInfo(index,modalcategory) {
    this.selectionCheck.clear();
    this.modalTableTitle = 'Check List';
    this.modalTableTitle1 = modalcategory;
    this.RawData_checks = mergeById(JSON.parse(this.dataSource.data[index]['check_list']), JSON.parse(this.dataSource.data[index]['check_list1']));
    // this.RawData_checks=JSON.parse(this.dataSource.data[index]['check_list']);
    this.dataSourceCheckList.data = this.commonSer.iterator(this.RawData_checks, this.currentPage, this.pageSize);
    console.log(this.dataSourceCheckList)
    this.totalSize = this.dataSourceCheckList.data.length;
    this.cdr.detectChanges();
    console.log(this.dataSourceCheckList)

  }

  viewPromoInfo(index,modalcategory) {
    this.selectionPromo.clear();
    this.modalTableTitle = 'Promo Materials';
    this.modalTableTitle1 = modalcategory;
    this.RawData_pro = mergeById(JSON.parse(this.dataSource.data[index]['promo']), JSON.parse(this.dataSource.data[index]['promoas']));
    console.log(this.RawData_pro);
    //this.RawData_pro=JSON.parse(this.dataSource.data[index]['promo']);
    this.dataSourcePromo.data = this.RawData_pro;
    this.totalSize = this.dataSourcePromo.data.length;
    this.cdr.detectChanges();

  }
  viewProInfo(index,modalcategory) {
    this.selectionPalno.clear();
    this.modalTableTitle = 'Planogram';
    this.modalTableTitle1=modalcategory;
    var test = mergeById1(JSON.parse(this.dataSource.data[index]['planogram']), JSON.parse(this.dataSource.data[index]['prod']));
    this.displayedColumnsProduct = Object.keys(test[0]);
    //this.displayedColumnsProduct.splice(1,0,'s_index');
    const items = this.displayedColumnsProduct;
    const valuesToRemove = ['id', 'name', 'imagename', 'image_count','action_required','workcategory_id'];
    this.filteredItems = items.filter(item => !valuesToRemove.includes(item))
    this.displayedColumnsProduct = this.filteredItems;
    console.log(this.displayedColumnsProduct);
    this.dataSourceProductsDisplay.data = test
    var displayProducts = [];
    test.forEach(element => {

      displayProducts.push(element);

    });
  }

  // image view
  imageviewBE(i) {
    this.isLoading = true;
    this.data = [];
    this.images = this.dataSourceBE.data[i]['image'];
    console.log(this.images)
    this.images.forEach((element, index) => {
      // var imgname=this.dataSourceBE.data[i]['imagename'].filter(s => s.indexOf(index));
      var datas = {
        image: environment.file_url + element,
        head: this.dataSourceBE.data[i]['imagename'][index],
        data: '',
        name: this.dataSourceBE.data[i]['imagename'][index]
      }
      this.data.push(datas
      )

    });

    this.imgName = this.data[0].name
    this.imgNumber = 1
    this.imgLength = this.data.length
    this.belowImageData;
    this.cdr.detectChanges();


  }

  imageviewPromo(i) {
    this.data = [];
    this.images = this.dataSourcePromo.data[i]['image'];
    console.log(this.images)
    this.images.forEach((element, index) => {
      console.log(element)
      var imgname = this.dataSourcePromo.data[i]['imagename'].filter(s => s.indexOf(index));
      var datas = {
        image: environment.file_url + element,
        head: this.dataSourcePromo.data[i]['imagename'][index],
        data: '',
        name: this.dataSourcePromo.data[i]['imagename'][index]
      }
      this.data.push(datas
      )

    });

    this.imgName = this.data[0].name
    this.imgNumber = 1
    this.imgLength = this.data.length
    this.belowImageData;
    this.cdr.detectChanges();


  }

  imageviewPlano(i) {
    this.data = [];
    this.images = this.dataSourceProductsDisplay.data[i]['image'];
    console.log(this.images)
    this.images.forEach((element, index) => {
      console.log(element)
      var imgname = this.dataSourceProductsDisplay.data[i]['imagename'].filter(s => s.indexOf(index));
      var datas = {
        image: environment.file_url + element,
        head: this.dataSourceProductsDisplay.data[i]['imagename'][index],
        data: '',
        name: this.dataSourceProductsDisplay.data[i]['imagename'][index],
      }
      this.data.push(datas
      )

    });

    this.imgName = this.data[0].name
    this.imgNumber = 1
    this.imgLength = this.data.length
    this.belowImageData;
    this.cdr.detectChanges();


  }
  imageviewCheck(i) {
    this.data = [];
    this.images = this.dataSourceCheckList.data[i]['image'];
    console.log(this.images)
    this.images.forEach((element, index) => {
      var imgname = this.dataSourceCheckList.data[i]['imagename'].filter(s => s.indexOf(index));
      console.log(element)
      var datas = {
        image: environment.file_url + element,
        head: this.dataSourceCheckList.data[i]['imagename'][index],
        data: '',
        name: this.dataSourceCheckList.data[i]['imagename'][index],
      }
      this.data.push(datas
      )

    });

    this.imgName = this.data[0].name
    this.imgNumber = 1
    this.imgLength = this.data.length
    this.belowImageData;
    this.cdr.detectChanges();


  }


  change(data) {

    this.imgNumber = data.current + 1;
    this.imgName = this.data[data.current].name
    this.belowImageData = JSON.parse(JSON.stringify(this.data));
    this.belowImageData.splice(data.current, 1);

  }
  hideLoader() {
    this.isLoading = false;
  }
  page_chage(events,data)
  {
    
    
    var range=this.commonSer.handlePage(events,data);
    this.dataSourceBE=this.commonSer.iterator(this.RawData,range[0]['start'],range[0]['end']);
    this.dataSourceCheckList=this.commonSer.iterator(this.RawData_checks,range[0]['start'],range[0]['end'])
    this.dataSourcePromo=this.commonSer.iterator(this.RawData_pro,range[0]['start'],range[0]['end'])
    this.dataSourceProductsDisplay=this.commonSer.iterator(this.RawData_diplay,range[0]['start'],range[0]['end'])
    this.cdr.detectChanges();
    
  }
}

