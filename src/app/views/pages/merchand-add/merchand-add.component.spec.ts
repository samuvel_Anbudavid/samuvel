import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchandAddComponent } from './merchand-add.component';

describe('MerchandAddComponent', () => {
  let component: MerchandAddComponent;
  let fixture: ComponentFixture<MerchandAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchandAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchandAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
