// import { Component, OnInit } from '@angular/core';
import { AfterViewInit, AfterViewChecked, ViewChild } from '@angular/core';
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';
// Angular
import { Component, OnInit, ElementRef, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef,ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatSnackBar,MatTableDataSource ,MatButtonToggleModule} from '@angular/material';
import { AuthService } from '../../../core/auth';
import { FormGroup, FormControl, Validators,FormBuilder } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { HttpserviceService } from '../../../httpservice.service';
//import { Http, ResponseContentType, Headers, RequestOptions } from '@angular/http';
// import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import * as XLSX from 'xlsx';
import { from } from 'rxjs';
//import { map } from 'rxjs/operators';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import {LoaderComponent} from '../loader/loader.component';
import { count } from 'rxjs/operators';
import {CommonSerService}from'../../../core/commonser';
import { common } from '../testing/testing.component';
// import { OverlayRef } from '@angular/cdk/overlay';

@Component({
  selector: 'kt-store-category-master',
  templateUrl: './store-category-master.component.html',
  styleUrls: ['./store-category-master.component.scss'],
 
})

export class StoreCategoryMasterComponent implements OnInit {
  displayedColumns = ['select', 's_no', 'category', 'store_name','actions'];
  dataSource=new MatTableDataSource<User>();
 // dataSource=ELEMENT_DATA;
//private dataSource;
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  // Filter fields
	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  //lastQuery: QueryParamsModel;
  //Variable Declare
  selection = new SelectionModel<User>(true, []);
  usersResult: User[] = [];
  public categorys: any[]=[{category: 1,name:''}];
  private allDataExcel= [];
  private fileName: string = 'Store Category.xlsx';
  category_label:string;
  Edit_Name_form:FormGroup;
  Name_form:FormGroup;
  private be: any;
  str_id:any;
  cat_val:any;
  public valid_dat=[];
  showSpinner=true;
  cond=false;
  overlayRef: OverlayRef; 
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  constructor(private modalService: NgbModal, private service: AuthService,private fb: FormBuilder,private layoutUtilsService: LayoutUtilsService,private overlay: Overlay,private common:CommonSerService) {
  
    
  }
//Modal box functions
   openCentred(content) {
      this.modalService.open(content, { centered: true } );
     this.cond=false;

  }
  //paginator
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  //dynamic categorys      
  addAddress() {
    this.categorys.push({category: this.categorys.length + 1, });
  }

  removeAddress(i: number) {
    this.categorys.splice(i, 1);
  }

 ngOnInit() {
this.overlayRef = this.overlay.create({	
    positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),	
    hasBackdrop: true	
  });	
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
	  this.dataSource=new MatTableDataSource<User>();
this.get_category();
this.get_max();
console.log( this.dataSource);

this.Name_form =this.fb.group({
   store_name:  new FormControl('', [Validators.required ]),
    category:  new FormControl(this.cat_val, [ ]),

});
//this.overlayRef.detach();
      
}
// form Reset modal
reset()
{
	this.overlayRef.detach();
this.Name_form =this.fb.group({
   store_name:  new FormControl('', [Validators.required ]),
    category:  new FormControl(this.cat_val, [ ]),

});
this.cond=false;
}
// get MAx  value  category increment
get_max()
{
  var data = "";
  var method = "post";
  var url = "max_store";
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { 
    //this.categorys = [{category: data.result+1,name:''}];
       this.str_id= data.result+1;
	   this.overlayRef.detach();
      
  });
  this.cat_val='Category'+this.str_id;
}	
//datatable datas
get_category()
{
  const ELEMENT_DATA:any=[];
    this.overlayRef.attach(this.LoaderComponentPortal);
  var data = "";
  var method = "post";
  var url = "get_all_store";
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => {
    this.allDataExcel= data.result;  
    for(let c of data.result){
         ELEMENT_DATA.push(c);
         this.dataSource=new MatTableDataSource(ELEMENT_DATA);
  this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
	 this.overlayRef.detach();
        }
      
  });    
}					
//Checkbox function																															
User
isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
  }
/** FILTRATION */
filterConfiguration(): any {
  const filter: any = {};
  const searchText: string = this.searchInput.nativeElement.value;

  filter.lastName = searchText;

  filter.category = searchText;
  filter.store_name = searchText;
  //filter. = searchText
  return filter;
}

 applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  // duplicated Remover
duplicate()
{
  this.cond=false;

}
//Validation
get log() { return this.Name_form.controls;  }
get editlog() {  return this.Edit_Name_form.controls;}

//Edit modal Fecth value
editUser(id,content)
{

this.openCentred(content);
 this.Edit_Name_form =this.fb.group({
   store_name:  new FormControl(id.store_name, [Validators.required ]),
    category:  new FormControl(id.category, [ ]),
s_no:new FormControl(id.s_no, [ ]),
});
 this.category_label=id.category;

  
}
//crud function

add_store_master()
{
	if (this.Name_form.invalid) {
		document.getElementById("store_name").focus();
      this.Name_form.get('store_name').markAsTouched();
    
      
    }
	else
	{ 
    
    for(let data1 of this.allDataExcel)
{  
  const valid_dat=[];
  this.cond=false;
  if(data1.store_name.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()==this.Name_form.value.store_name.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())
  {
   this.valid_dat.push(data1);
    
  }
//  return this.valid_dat;
}
    if(this.valid_dat.length==0)
   {
    console.log(this.Name_form.value.store_name);
this.overlayRef.attach(this.LoaderComponentPortal);
var data = this.Name_form.value;
 var method = "post";
 var url = "save_store";
 this.showSpinner=true;
 //this.modalService.dismissAll();
 this.common.Modal_close();
 this.service.servicepost(data, method, url, 'application/json')
  .subscribe(data => { 

this.overlayRef.detach();
Swal.fire(
 
      'Save!',
      'Your record has been saved.',
      'success'
    )
 
this.ngOnInit();
  
       this.showSpinner=false;
      
  });
}
else
{
 this.cond=true;
 this.valid_dat=[];
}
  }

}
 
edit_store_master()
{
  this.valid_dat=[];
  for(let data1 of this.allDataExcel)
  {  
    const valid_dat=[];
    this.cond=false;
    if(data1.store_name.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()==this.Edit_Name_form.value.store_name.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())
    {
     this.valid_dat.push(data1);
      
    }
  //  return this.valid_dat;
  }
  
  if((this.valid_dat.length==0)||(this.valid_dat.length==1 && this.valid_dat[0]['category']== this.category_label &&  this.valid_dat[0]['store_name']!=this.Edit_Name_form.value))
 {
  this.overlayRef.attach(this.LoaderComponentPortal);
 var data = this.Edit_Name_form.value;
  var method = "post";
  var url = "edit_store";
  this.showSpinner=true;
  //this.modalService.dismissAll();
  this.common.Modal_close();
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1){
      this.overlayRef.detach();
Swal.fire(
      'Edit!',
      'Your record has been Updated.',
      'success'
    )

this.ngOnInit();}
  this.showSpinner=false;
}); 
 }
 else
 {
  this.cond=true;
  this.valid_dat=[];

 }
}

fetchUsers() {
  const messages = [];
  
  this.selection.selected.forEach(elem => {
    messages.push({
      category: elem.category,
      store_name:elem.store_name,
      s_no:elem.s_no
    });
console.log(elem);      
  });
   
Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this  Record!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {
this.showSpinner=true;
  if (result.value) {
    this.overlayRef.attach(this.LoaderComponentPortal);
var data = messages;
  var method = "post";
  var url = "bulkdelete_store";
  
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1)
    {
   	
    
    
this.dataSource=new MatTableDataSource<User>();
 
//this.isAllSelected();  
//this.masterToggle();
this.selection.selected.length=0;
this.overlayRef.detach();
this.ngOnInit();
this.selection.clear() ;
//this.overlayRef.detach();
this.showSpinner=false;
 
}
  });
}
})
  // this.layoutUtilsService.fetchElements(messages);
//this.selection.selected.length=0;
}


deletestore(del)
{
	
Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this  Record!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {
  if (result.value) {
    this.overlayRef.attach(this.LoaderComponentPortal);
var data = del;
  var method = "post";
  var url = "delete_store";
  this.showSpinner=true;
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1){Swal.fire(
      'Deleted!',
      'Your record has been deleted.',
      'success'
    )
this.selection.clear() ;
this.dataSource=new MatTableDataSource<User>();
this.overlayRef.detach();
this.ngOnInit();

this.showSpinner=false;
}
  });
}
})
}

//Download  excel
export(): void {
//  const _title = 'Download';
  //const _description = 'Are you sure to download the user data?';
//  const _waitDesciption = 'Data is downloading...';
//  const _deleteMessage = `User has been deleted`;
  var downloadArray= [];
  var testArray= [];
  var headerArray= [];
  headerArray=['S.No','Name'];
  testArray.push(headerArray);
  
  for(var k=0;k<this.allDataExcel.length;k++) {
    //downloadArray=[k+1,this.allDataExcel[k]['category'],this.allDataExcel[k]['store_name']];
    downloadArray=[k+1,this.allDataExcel[k]['store_name']];
    
    testArray.push(downloadArray);
  }


Swal.fire({
  title: "Are you sure?",
  text: " You want to Download this page!",
  showConfirmButton: true,
  showCancelButton: true     
  })
  .then((willDelete) => {

  if(willDelete.value){
        const message ="Downloaded successfully!"
        this.generateSheet(testArray);
this.layoutUtilsService.showActionNotification(message);  
 this.overlayRef.detach();
    //  swal.fire("Success");
  }else{
    const message ="Download Rejected!"
    this.layoutUtilsService.showActionNotification(message); 
  //   swal.fire("Fail");
  }

  console.log(willDelete)
});
}
generateSheet(sheetData){
  /* generate worksheet */
this.overlayRef.attach(this.LoaderComponentPortal);
  const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(sheetData);
  /*ws['!cols'] = fitToColumn(sheetData);

  function fitToColumn(sheetData) {
      // get maximum character of each column
      return sheetData[0].map((a, i) => ({ wch: Math.max(...sheetData.map(a2 => a2[i].toString().length)) }));
  }
  /* generate workbook and add the worksheet */
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  
  /* save to file */
  XLSX.writeFile(wb, this.fileName);
  
  
}

//uploading
onFileChange(evt: any) {
  console.log(evt.target.files.length);
  console.log(evt.target.files[0]['type']);
  const target: DataTransfer = <DataTransfer>(evt.target);
  var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
  if (target.files.length !== 1) {
  this.layoutUtilsService.showActionNotification(`Multiple files not allowed.`, MessageType.Update, 5000, true, true);
  }
  if (validExts.indexOf(target.files[0]['type']) < 0)
   {
  this.layoutUtilsService.showActionNotification(`Only file with extensions .xlsx, .xls, .csv is allowed.`, MessageType.Update, 5000, true, true);

  }
  else{

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
    /* read workbook */
    const bstr: string = e.target.result;
    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
  
    /* grab first sheet */
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
  
    /* save data */
    this.be = (XLSX.utils.sheet_to_json(ws, { header: 1 }));
    this.be.splice(0, 1);
    console.log(this.be);
this.overlayRef.attach(this.LoaderComponentPortal);
  var data = this.be;
  var method = "post";
  var url = "upload_save";
  this.showSpinner=true;
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1){
      Swal.fire(
       'Save!',
       'Your record has been saved.',
       'success'
     )
     
 
  this.overlayRef.detach();
 this.ngOnInit();
 this.showSpinner=false;
 }
 else
 {
   if(data.result==2)
   {
    this.layoutUtilsService.showActionNotification("Upload Document  is Empty" ); 
    this.overlayRef.detach();
   this.ngOnInit();
   }
   else{
   const dat_valid=[];
   for(let f of data.result ){
     console.log(f);
   
  dat_valid.push(f);
  
   }
   Swal.fire(
    'Error!',
    'Already Exist Category Name:' +dat_valid,
    'error'
  )
 // this.layoutUtilsService.showActionNotification("Already Exist Category Name:" +dat_valid); 
   this.overlayRef.detach();
   this.ngOnInit();
 }
 }
   });
 
 };
 reader.readAsBinaryString(target.files[0]);
 }
 evt.target.value = ''
 }
//sample template
downloadFile() {

var downloadArray= [];
  var testArray= [];
  var headerArray= [];
  headerArray=['S.No','Name'];
  testArray.push(headerArray);
  this.generateSheet(testArray);
       this.overlayRef.detach();
  }


}

//Interface
export interface User {
  s_no: number;
    category: string;
    store_name:string;
    created_at:string;
    update_at:string;
  }
  
//const ELEMENT_DATA:User[]=[];




  
  

 

 
