import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreCategoryMasterComponent } from './store-category-master.component';

describe('StoreCategoryMasterComponent', () => {
  let component: StoreCategoryMasterComponent;
  let fixture: ComponentFixture<StoreCategoryMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreCategoryMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreCategoryMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
