// import { Component, OnInit } from '@angular/core';
import { AfterViewInit, AfterViewChecked, ViewChild } from '@angular/core';
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';
// Angular
import { Component, OnInit, ElementRef, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef,ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatSnackBar,MatTableDataSource ,MatButtonToggleModule} from '@angular/material';
import { AuthService } from '../../../core/auth';
import { FormGroup, FormControl, Validators,FormBuilder } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { HttpserviceService } from '../../../httpservice.service';
//import { Http, ResponseContentType, Headers, RequestOptions } from '@angular/http';
// import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import * as XLSX from 'xlsx';
import { from } from 'rxjs';
//import { map } from 'rxjs/operators';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import {LoaderComponent} from '../loader/loader.component';
import { count } from 'rxjs/operators';
// import { OverlayRef } from '@angular/cdk/overlay';
import  {CommonSerService} from '../../../core/commonser'
@Component({
  selector: 'kt-promotional-material-master',
  templateUrl: './promotional-material-master.component.html',
  styleUrls: ['./promotional-material-master.component.scss']
})
export class PromotionalMaterialMasterComponent implements OnInit {
  displayedColumns = ['select', 's_no', 'product_id', 'product_category','name_description','actions'];
  dataSource=new MatTableDataSource<Promotional>(ELEMENT_DATA);
 // dataSource=ELEMENT_DATA;
//private dataSource;
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  // Filter fields
	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  
  selection = new SelectionModel<Promotional>(true, []);
  usersResult: Promotional[] = [];
  private allDataExcel= [];
  private fileName: string = 'Promotional Material.xlsx';
  Edit_Name_form:FormGroup;
  Name_form:FormGroup;
  private be: any;
  cond=false;
  public valid_dat=[];                                                                          
  overlayRef: OverlayRef; 
  public categorys:any;
  addcategory=true;
  category_dis=false;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  constructor(private modalService: NgbModal, private service: AuthService,private fb: FormBuilder,private layoutUtilsService: LayoutUtilsService,private overlay: Overlay,private common :CommonSerService) {
  
    
  }
//Modal box functions
   openCentred(content) {
   //   this.modalService.open(content, { centered: true, size: 'lg' } );
   this.common.openCentred(content,'lg');  
   this.cond=false;

  }
  
  
  //paginator
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor=(data,attribute)=>data[attribute];
  }


ngOnInit() {
this.overlayRef = this.overlay.create({	
positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),hasBackdrop: true});	
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
	  this.dataSource=new MatTableDataSource<Promotional>();
this.get_promotional();
this.get_category_promotional();
this.overlayRef.detach();
this.Name_form =this.fb.group({
  product_id:  new FormControl('', [Validators.required ]),
  product_category:  new FormControl('', [Validators.required ]),
  name_description:  new FormControl('', [Validators.required ]),
  
   
   
});

      
}
// form Reset modal
reset()
{
	this.overlayRef.detach();
	this.addcategory=true;
	this.category_dis=false;
this.Name_form =this.fb.group({
    product_id:  new FormControl('', [Validators.required ]),
  product_category:  new FormControl('', [Validators.required ]),
  name_description:  new FormControl('', [Validators.required ]),

});
this.cond=false;
}
//datatable datas
get_promotional()
{
  this.overlayRef.attach(this.LoaderComponentPortal);
  const ELEMENT_DATA:Promotional[]=[];                                                                                        
  var data = "";
  var method = "post";
  var url = "get_all_promotional";
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => {
    this.allDataExcel= data.result;  
    for(let c of data.result){
         ELEMENT_DATA.push(c);
         this.dataSource=new MatTableDataSource(ELEMENT_DATA);
  this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
     this.overlayRef.detach();
	 console.log( this.categorys);
        }
      
  });    
}					
//Checkbox function																															
User
isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
  }
/** FILTRATION */
filterConfiguration(): any {
  const filter: any = {};
  const searchText: string = this.searchInput.nativeElement.value;

  filter.lastName = searchText;

  filter.category = searchText;
  filter.name_descripiton = searchText;
  filter.product_category = searchText
  return filter;
}
//add category
category(category: string) {
	console.log(category);
	category = category.trim(); // Remove whitespace
  //  category = category.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase();
	category = category.toLowerCase();
	var skinName = this.categorys.find(x=>x.product_category.toLowerCase()== category);
	//console.log(Object.keys(skinName).length);
if(skinName ||  category==null)
	{
		this.addcategory=true;
		this.category_dis=false;
	}
	else
	{
		this.addcategory=false;
		 this.category_dis=true;
	}
	
}

 applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  // duplicated Remover
duplicate()
{
  this.cond=false;

}
//Validation
get log() { return this.Name_form.controls;  }
get editlog() {  return this.Edit_Name_form.controls;}

//Edit modal Fecth value
editUser(id,content)
{

this.openCentred(content);
 this.Edit_Name_form =this.fb.group({
product_id:  new FormControl(id.product_id, [Validators.required ]),
product_category:  new FormControl(id.product_category, [Validators.required ]),
name_description:  new FormControl(id.name_description, [Validators.required ]),


});
}
//crud function
get_category_promotional()
{
	var data = "";
 var method = "post";
 var url = "get_category_promotional";
  this.service.servicepost(data, method, url, 'application/json')
  .subscribe(data => { console.log(data.result);
  this.categorys=data.result;
  });
}

add_category(category)
{

	var data ={"category":category} ;
 var method = "post";
 var url = "add_category_promotional";
  this.service.servicepost(data, method, url, 'application/json')
  .subscribe(data => { 
   
   if(data.result==1)
   {
	   this.addcategory=true;
	   this.get_category_promotional();
	    this.category_dis=false;
   }
   
  });
}
add_promotional_master()
{
	if (this.Name_form.invalid) {
this.Name_form.markAllAsTouched();
 /* this.Name_form.get('product_id').markAsTouched();
  this.Name_form.get('product_category').markAsTouched();
  this.Name_form.get('name_description').markAsTouched();
  this.Name_form.get('visitingcard').markAsTouched();    
  this.Name_form.get('complements_gifts').markAsTouched();    */
  
}
	else
	{ 
    
    for(let data1 of this.allDataExcel)
{  
  const valid_dat=[];
  this.cond=false;
  if(data1.product_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()==this.Name_form.value.product_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())
  {
   this.valid_dat.push(data1);
    
  }
//  return this.valid_dat;
}
    if(this.valid_dat.length==0)
   {
    console.log(this.Name_form.value.promotional_name);
this.overlayRef.attach(this.LoaderComponentPortal);
var data = this.Name_form.value;
 var method = "post";
 var url = "save_promotional";
 //this.modalService.dismissAll();
 this.common.Modal_close();
 this.service.servicepost(data, method, url, 'application/json')
  .subscribe(data => { 

this.overlayRef.detach();
Swal.fire(
 
      'Save!',
      'Your record has been saved.',
      'success'
    )
 
this.ngOnInit();
  
      
      
  });
}
else
{
 this.cond=true;
 this.valid_dat=[];
}
  }

}
 
edit_promotional_master()
{
  
 this.overlayRef.attach(this.LoaderComponentPortal);
 var data = this.Edit_Name_form.value;
  var method = "post";
  var url = "edit_promotional";
  //this.modalService.dismissAll();
  this.common.Modal_close();
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1){
      this.overlayRef.detach();
Swal.fire(
      'Edit!',
      'Your record has been Updated.',
      'success'
    )

this.ngOnInit();}
}); 
 
 
}

fetchUsers() {
  const messages = [];
  
  this.selection.selected.forEach(elem => {
    messages.push({
     
  product_id:elem.product_id,
  product_category:elem.product_brochure,
  name_description: elem.samples_testers,
  
    });
console.log(elem);      
  });
    
Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this  Record!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {
  if (result.value) {
    this.overlayRef.attach(this.LoaderComponentPortal);
var data = messages;
  var method = "post";
  var url = "bulkdelete_promotional";
  
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1)
    {
   	
    
    
this.dataSource=new MatTableDataSource<Promotional>();
 
//this.isAllSelected();  
//this.masterToggle();
this.selection.selected.length=0;
this.overlayRef.detach();
this.ngOnInit();
this.selection.clear() ;
//this.overlayRef.detach();
 
}
  });
}
})
  // this.layoutUtilsService.fetchElements(messages);
//this.selection.selected.length=0;
}


deletepromotional(del)
{
	
Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this  Record!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {
  if (result.value) {
    this.overlayRef.attach(this.LoaderComponentPortal);
var data = del;
  var method = "post";
  var url = "delete_promotional";
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1){Swal.fire(
      'Deleted!',
      'Your record has been deleted.',
      'success'
    )
	
this.selection.clear() ;
this.dataSource=new MatTableDataSource<Promotional>();
this.overlayRef.detach();
this.ngOnInit();
}
  });
}
})
}

//Download  excel
export(): void {
//  const _title = 'Download';
  //const _description = 'Are you sure to download the user data?';
//  const _waitDesciption = 'Data is downloading...';
//  const _deleteMessage = `User has been deleted`;
  var downloadArray= [];
  var testArray= [];
  var headerArray= [];
  headerArray=['S.No', 'Product Id', 'Product Category','Name /Description'];
  testArray.push(headerArray);
  
  for(var k=0;k<this.allDataExcel.length;k++) {
    //downloadArray=[k+1,this.allDataExcel[k]['category'],this.allDataExcel[k]['promotional_name']];
    downloadArray=[k+1,this.allDataExcel[k]['product_id'], this.allDataExcel[k]['product_category'],this.allDataExcel[k]['name_description']];
    
    testArray.push(downloadArray);
  }


Swal.fire({
  title: "Are you sure?",
  text: " You want to Download this page!",
  showConfirmButton: true,
  showCancelButton: true     
  })
  .then((willDelete) => {

  if(willDelete.value){
        const message ="Downloaded successfully!"
        this.generateSheet(testArray);
this.layoutUtilsService.showActionNotification(message);  
 this.overlayRef.detach();
    //  swal.fire("Success");
  }else{
    const message ="Download Rejected!"
    this.layoutUtilsService.showActionNotification(message); 
  //   swal.fire("Fail");
  }

  console.log(willDelete)
});
}
generateSheet(sheetData){
  /* generate worksheet */
this.overlayRef.attach(this.LoaderComponentPortal);
  const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(sheetData);
  ws['!cols'] = fitToColumn(sheetData);

function fitToColumn(sheetData) {
    // get maximum character of each column
    return sheetData[0].map((a, i) => ({ wch: Math.max(...sheetData.map(a2 => a2[i].toString().length)) }));
}

  /* generate workbook and add the worksheet */
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  
  /* save to file */
  XLSX.writeFile(wb, this.fileName);
  
  
}

//uploading
onFileChange(evt: any) {
  console.log(evt.target.files.length);
  console.log(evt.target.files[0]['type']);
  const target: DataTransfer = <DataTransfer>(evt.target);
  var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
  if (target.files.length !== 1) {
  this.layoutUtilsService.showActionNotification(`Multiple files not allowed.`, MessageType.Update, 5000, true, true);
  }
  if (validExts.indexOf(target.files[0]['type']) < 0)
   {
  this.layoutUtilsService.showActionNotification(`Only file with extensions .xlsx, .xls, .csv is allowed.`, MessageType.Update, 5000, true, true);

  }
  else{

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
    /* read workbook */
    const bstr: string = e.target.result;
    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
  
    /* grab first sheet */
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
  
    /* save data */
    this.be = (XLSX.utils.sheet_to_json(ws, { header: 1 }));
    this.be.splice(0, 1);
    console.log(this.be);
this.overlayRef.attach(this.LoaderComponentPortal);
  var data = this.be;
  var method = "post";
  var url = "upload_save_promotional";
  
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { console.log(data.result.length);
	if(data.result.length==0){
      Swal.fire(
       'Save!',
       'Your record has been saved.',
       'success'
     )
     
 
  this.overlayRef.detach();
 this.ngOnInit();
 }
 else
 {
   if(data.result=="empty")
   {
    this.layoutUtilsService.showActionNotification("Upload Document  is Empty" ); 
    this.overlayRef.detach();
   this.ngOnInit();
   }
   else{
   const dat_valid=[];
   for(let f of data.result ){
    // console.log(f[2]);
   
  dat_valid.push(f);
  
   }
   Swal.fire(
    'Error!',
    'Already Exist Product ID:' +dat_valid,
    'error'
  )
  //this.layoutUtilsService.showActionNotification("Already Exist Product Id" +dat_valid); 
   this.overlayRef.detach();
   this.ngOnInit();
 }
 }
   });
 
 };
 reader.readAsBinaryString(target.files[0]);
 }
 evt.target.value = ''
 }
//sample template
downloadFile() {

var downloadArray= [];
  var testArray= [];
  var headerArray= [];
  headerArray=['S.No', 'Product Id', 'Product Category','Name /Description'];
  testArray.push(headerArray);
  this.generateSheet(testArray);
       this.overlayRef.detach();
  }


}

//Interface
export interface Promotional {
  s_no: number;
  product_id:string;
  product_brochure:string;
  samples_testers: string;
  visitingcard: string;
  complements_gifts:string;
  created_at:string;
  updated_at:string;
  }
  
const ELEMENT_DATA:Promotional[]=[];