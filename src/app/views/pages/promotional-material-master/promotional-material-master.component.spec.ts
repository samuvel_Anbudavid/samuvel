import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionalMaterialMasterComponent } from './promotional-material-master.component';

describe('PromotionalMaterialMasterComponent', () => {
  let component: PromotionalMaterialMasterComponent;
  let fixture: ComponentFixture<PromotionalMaterialMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionalMaterialMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionalMaterialMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
