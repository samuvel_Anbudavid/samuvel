// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { FormsModule } from '@angular/forms';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';
import { NgbootstrapModule } from './ngbootstrap/ngbootstrap.module';
import { MailModule } from './apps/mail/mail.module';
import { ECommerceModule } from './apps/e-commerce/e-commerce.module';
import { UserManagementModule } from './user-management/user-management.module';
import { MyPageComponent } from './my-page/my-page.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ProductGroupingComponent } from './product-grouping/product-grouping.component';
import { BrandingElementMasterComponent } from './branding-element-master/branding-element-master.component';
import { PromotionalMaterialMasterComponent } from './promotional-material-master/promotional-material-master.component';
import { TerritoryMasterComponent } from './territory-master/territory-master.component';
import { StoreCategoryMasterComponent } from './store-category-master/store-category-master.component';
import { UserMasterComponent } from './user-master/user-master.component';
import { WorkCategoryMasterComponent } from './work-category-master/work-category-master.component';
import { CheckListMasterComponent } from './check-list-master/check-list-master.component';
import { RetailMasterComponent } from './retail-master/retail-master.component';
import { LoaderComponent } from './loader/loader.component';
//import { AuthNoticeComponent } from '../pages/auth/auth-notice/auth-notice.component'
//import { NgbdNavBasic } from './nav-basic';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
// NgBootstrap
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthModule } from './auth/auth.module'
import{StoreInspectorMasterComponent}from'./store-inspector-master/store-inspector-master.component';
import { MerchandAddComponent } from './merchand-add/merchand-add.component';
import { StoreInspectorModalComponent } from './store-inspector-modal/store-inspector-modal.component';
import { MerchandMenuComponent } from './merchand-menu/merchand-menu.component';
import { ContractorVendorMasterComponent } from './contractor-vendor-master/contractor-vendor-master.component';
import { ContractorVendorModalComponent } from './contractor-vendor-modal/contractor-vendor-modal.component';
// Highlight JS
import { HighlightModule } from 'ngx-highlightjs';
//import { ClarityModule } from '@clr/angular';
// Perfect Scrollbar
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// Material
import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatExpansionModule,
	MatAutocompleteModule,
	MatSnackBarModule,
	MatTooltipModule,
	MatTreeModule,
	MatCheckboxModule
} from '@angular/material';
import { TestingComponent } from './testing/testing.component';
import { AuditListComponent } from './audit-list/audit-list.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';
import { ContractorDashboardComponent } from './contractor-dashboard/contractor-dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from "@mat-datetimepicker/core";
//import '@clr/icons';
//import '@clr/icons/shapes/all-shapes';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ViewReportsComponent } from './view-reports/view-reports.component';
import { DefectReportComponent } from './defect-report/defect-report.component';


@NgModule({
	declarations: [MyPageComponent, AdminDashboardComponent, ProductGroupingComponent,
		 BrandingElementMasterComponent, PromotionalMaterialMasterComponent, 
		  StoreCategoryMasterComponent, UserMasterComponent, TestingComponent,
		 WorkCategoryMasterComponent, TerritoryMasterComponent,CheckListMasterComponent, RetailMasterComponent,
		  LoaderComponent, TestingComponent,MerchandAddComponent,StoreInspectorMasterComponent,StoreInspectorModalComponent, 
		  MerchandMenuComponent, ContractorVendorMasterComponent, ContractorVendorModalComponent, AuditListComponent, ManagerDashboardComponent, ContractorDashboardComponent, UserProfileComponent, ViewReportsComponent, DefectReportComponent
		  //AuthNoticeComponent
		],
	exports: [],
	imports: [
		MatDatetimepickerModule, MatNativeDatetimeModule,
		NgMultiSelectDropDownModule.forRoot(),
		CommonModule,
		HttpClientModule,
		FormsModule,
		CoreModule,
		PartialsModule,
		MailModule,
		ECommerceModule,
		UserManagementModule,
		MatInputModule,
		MatPaginatorModule,
		MatProgressSpinnerModule,MatSortModule,MatTableModule,MatSelectModule,
		MatMenuModule,MatProgressBarModule,
		MatButtonModule,MatDialogModule,
		MatTabsModule,
		MatNativeDateModule,MatCardModule,
		MatRadioModule,
		MatIconModule,MatDatepickerModule,
		MatExpansionModule,
	    MatTreeModule,
		MatAutocompleteModule,
		MatCheckboxModule,NgbootstrapModule,MatTooltipModule,ReactiveFormsModule,
		AuthModule,
		NgbTabsetModule,HighlightModule,PerfectScrollbarModule,
	//	ClarityModule,
		BrowserAnimationsModule,
		BrowserModule,
		NgbModule
	
	],
	providers: [],
	entryComponents: [ TestingComponent,MerchandAddComponent, StoreInspectorModalComponent,ContractorVendorModalComponent ]
})
export class PagesModule {
}
