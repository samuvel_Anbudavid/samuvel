import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreInspectorModalComponent } from './store-inspector-modal.component';

describe('StoreInspectorModalComponent', () => {
  let component: StoreInspectorModalComponent;
  let fixture: ComponentFixture<StoreInspectorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreInspectorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreInspectorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
