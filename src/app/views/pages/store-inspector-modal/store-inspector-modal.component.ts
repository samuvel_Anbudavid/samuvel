import { AfterViewInit, Component, ElementRef, OnInit, ViewChild,Input ,Output,EventEmitter,ChangeDetectorRef} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../core/auth';
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import csc from 'country-state-city';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
//import{UserMasterComponent}from'../user-master/user-master.component';
//import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import{CommonSerService} from'../../../core/commonser';
import { stringify } from 'querystring';
@Component({
  selector: 'kt-store-inspector-modal',
  templateUrl: './store-inspector-modal.component.html',
  styleUrls: ['./store-inspector-modal.component.scss']
})
export class StoreInspectorModalComponent implements OnInit {

 @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
@ViewChild('sort1', {static: true}) sort: MatSort;
 @ViewChild('wizard', {static: true}) el: ElementRef;
 @Input() edit_id:User;
 saved: EventEmitter<any> = new EventEmitter();
 //@ViewChild('UserMasterComponent',{static:true})private child: UserMasterComponent;
 private merchand:FormGroup;
 private grade:number;
private emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
private phonePattern: RegExp = /^[0-9-+]{10,12}$/;
submitted = false;
country:any;
state:any;
district:any;
country_geo:any;
state_geo:any;
district_geo:any;
taluk_geo:any;
city:any;
disable:boolean;
taluk:any;
dropdownList = [];
  selectedItems:any=[];
  dropdownSettings :IDropdownSettings= {};
Edit_enable:boolean;
save_enable:boolean;
overlayRef: OverlayRef; 
	duplicated2: boolean;
	duplicated1: boolean;
	duplicated: boolean;
	allDataExcel: any;
	manger_list=[];
	manager_details:any;
	dd_selected: boolean;
	dd_as: any;
constructor(private modalService: NgbModal, private commonSer: CommonSerService,private cdRef: ChangeDetectorRef ,private fb: FormBuilder, private auth: AuthService,private overlay: Overlay,public activeModal: NgbActiveModal) {
	}

	ngOnInit() {
		this.use_data();
		this.dd_selected=true;
			this.save_enable=false;
	this.Edit_enable=true;

		this.initRegistrationForm();
		  this.disable=true;
		this.country=csc.getAllCountries();
		this.country_geo=csc.getAllCountries();
this.dd_manger();
	//Edit data come  form parent of UserMasterComponent\
	// this.cdRef.detectChanges();
 if(this.edit_id )
{
    	this.dd_selected=false;
	this.edit_merchand();
	//this.state=csc.getAllStates();
	//this.district=csc.getAllDistricts();
	//this.taluk=csc.getAllTaulks();
	this.save_enable=true;
	this.Edit_enable=false;
}

  
} 
dd_manger()
{
	var data = "";
	//  var manger_list=[];
var method = "post";
var url = "get_merchand";
this.auth.servicepost(data, method, url, 'application/json')
.subscribe(data => {
	 for(let c of data.result){
	  if(c.role_id == 2)
	  {
		  var geo_value=JSON.parse(c.geo1_data);
		
		  var state=csc.getStateById(geo_value.state);
		  var district_geo=csc.getDistrictsById(geo_value.district);
		   var taluk_geo=csc.getTalukById(geo_value.taluk);
	// var 	 name_geo=c.f_name+" "+c.l_name + "("+taluk_geo.name+","+district_geo.name+"-"+state.name+")"
	var 	 name_geo=c.f_name+" "+c.l_name ;
	this.manger_list.push({"name":name_geo,"taluk":geo_value.taluk,"id":c.id});
   
 //this.dropdownList = manger_list ; 
 //this.selectedItems = [ ];
this.dropdownSettings = {
  singleSelection: false,
  idField: 'id',
  textField: 'name',
  selectAllText: 'Select All',
  unSelectAllText: 'UnSelect All',
  itemsShowLimit: 3,
  allowSearchFilter: true
};

	  }
	  }

});  


//consolelog(this.manger_list);
//	this.dropdownList = this.category;

}
ngOnChanges() {
	// this.dd_manger()
}
	 onItemSelect(item: any) {
    //consolelog(item);
  }
  onSelectAll(items: any) {
    //consolelog(items);
  }
	
  getstate_geo(country_id)
  {
	this.merchand.get('c_state').reset(); 
	this.merchand.get('c_state').enable();
  
	this.merchand.get('c_district').reset();
	this.merchand.get('c_district').disable();
  
	this.merchand.get('c_taluk').reset();
	this.merchand.get('c_taluk').disable();
  
	this.merchand.get('c_town').reset();
	this.merchand.get('c_town').disable();
  
	this.merchand.get('c_pincode').reset();
	this.merchand.get('c_pincode').disable();
	  setTimeout(() => {
	 this.state_geo=csc.getStatesOfCountry(country_id);
	 });
  }
  
   getdistrict_geo(state_id)
  {
	this.merchand.get('c_district').enable();
  
	this.merchand.get('c_taluk').reset();
	this.merchand.get('c_taluk').disable();
  
	this.merchand.get('c_town').reset();
	this.merchand.get('c_town').disable();
	  setTimeout(() => { 
	
	 if(state_id<=41)
	 {
		
	this.district_geo=csc.getDistrictById(state_id);
	//consolelog(this.district);
	//consolelog(state_id);
	 }
	 else
	 {
		 
		
	  this.city=csc.getCitiesOfState(state_id);
	
	 }
	  });
  }
  
  gettaluk_geo(district)
  {
	this.merchand.get('c_taluk').enable();
  
	this.merchand.get('c_town').reset();
	this.merchand.get('c_town').enable();
  
	this.merchand.get('c_pincode').reset();
	this.merchand.get('c_pincode').enable();
	
	   setTimeout(() => {
	// this.taluk=csc.getTalukOfState(district2);
  this.taluk_geo=csc.getTalukOfDistrict (district);
  //consolelog(this.taluk);
	   });
  }
	
  //personalbar
	 getstate(country_id)
  {
	this.merchand.get('state').reset(); 
	this.merchand.get('state').enable();
  
	this.merchand.get('district').reset();
	this.merchand.get('district').disable();
  
	this.merchand.get('taluk').reset();
	this.merchand.get('taluk').disable();
  
	this.merchand.get('city').reset();
	this.merchand.get('city').disable();
  
	this.merchand.get('pincode').reset();
	this.merchand.get('pincode').disable();
	   setTimeout(() => {
	 this.state=csc.getStatesOfCountry(country_id);
	   });
  }
  
   getdistrict(state_id)
  {
	this.merchand.get('district').enable();
  
	this.merchand.get('taluk').reset();
	this.merchand.get('taluk').disable();
  
	this.merchand.get('city').reset();
	this.merchand.get('city').disable();
  
	this.merchand.get('pincode').reset();
	this.merchand.get('pincode').disable();
  
	   setTimeout(() => {
	  
	
	 if(state_id<=41)
	 {
		
	this.district=csc.getDistrictById(state_id);
	//consolelog(this.district);
	//consolelog(state_id);
	 }
	 else
	 {
		 
		
	  this.city=csc.getCitiesOfState(state_id);
	
	 }
	   });
  }
  
  gettaluk(district)
  {
	   //	//consolelog(district['id']);
	   this.merchand.get('taluk').enable();
  
	   this.merchand.get('city').reset();
	   this.merchand.get('city').enable();
  
	   this.merchand.get('pincode').reset();
	   this.merchand.get('pincode').enable();
	   setTimeout(() => {
	// this.taluk=csc.getTalukOfState(district2);
  this.taluk=csc.getTalukOfDistrict (district);
	   //consolelog(this.taluk)
	});
  }
  
  dropdown(taluk:string)
  {

	console.log(taluk+this.manger_list);
	  manger_name=[];
	  this.dropdownList=[];
	 var manager= this.manger_list.filter(question => question.taluk == taluk.toString() ||  question.taluk == taluk ) ;
	//  for(let manger_ls of manger){
	// 	// var manger_name=[];
	//  manger_name.push(manger_ls.name);
	// 	//this.dropdownList=manger_ls.name;
	//  }
	 this.dropdownList=manager;
	// this.selectedItems=[];
	 this.dd_as=[];
	 console.log(this.dropdownList);
	
  }
	initRegistrationForm() {

		this.merchand = this.fb.group({
		  user_id: ['', Validators.compose([
		 Validators.required
	   ])
	  ],
	  f_name: ['', Validators.compose([
		Validators.required
			  ])
	 ],
	 l_name: ['', Validators.compose([
		Validators.required
	])
	],
	mobile_no: ['', Validators.compose([
	  Validators.required,
	  Validators.pattern(this.phonePattern)
	  
	])
	],
	email: ['', Validators.compose([
	  Validators.required,
	  //Validators.pattern(this.emailPattern)
	  Validators.email
	])
	],
	address_line1: ['', Validators.compose([
	  Validators.required,
	
	])
	],
	address_line2: ['', Validators.compose([
	  Validators.required,
	 
	])
	],
	city: ['', Validators.compose([
	  Validators.required,
	 
	])
	],
	state: ['', Validators.compose([
		Validators.required,
		
	  ])
	  ],
	country: ['', Validators.compose([
	  Validators.required,
	  
	])
	],
	pincode: ['', Validators.compose([
		Validators.required,
		
	  ])
	  ],
	alt_mobile_no: ['', Validators.compose([
	 
	  Validators.pattern(this.phonePattern)
	  
	])
	],
	 taluk:['',Validators.compose([
	 Validators.required
		])
		],
	district:['',Validators.compose([
	 Validators.required
		])
		],
	manger:['',Validators.compose([
	 Validators.required
		])
		],
	 
	});
	 
	}
	
	isControlHasError(controlName: string, validationType: string): boolean {
	  const control = this.merchand.controls[controlName];
	  if (!control) {
		return false;
	  }
	
	  const result = control.hasError(validationType) && (control.dirty || control.touched);
	  return result;
	}


	ngAfterViewInit(): void {
		this.cdRef.detectChanges();
		//this.drop();
		// const wizard = new KTWizard(this.el.nativeElement, {
		// 	startStep: 1,
		// 	//clickableSteps: false
		// });

	
		// wizard.on('beforeNext', (wizardObj) => {
			
		// });

		// // Change event
		// wizard.on('change', () => {
		// 	setTimeout(() => {
		// 		KTUtil.scrollTop();
		// 	}, 500);
		// });
	//const refresh= new UserMasterComponent();

	}
	
onSubmit_pre()
{
	const wizard = new KTWizard(this.el.nativeElement, {
			
		});
		wizard.goTo(1);
	//this.edit_merchand();
}
	onSubmit_next() {
		this.submitted = true;
		const wizard = new KTWizard(this.el.nativeElement, {
			startStep: 1,
		});
		const controls = this.merchand.controls;
		const firstPageControls={
			"user_id":FormControl,
			"f_name":FormControl,
			"l_name":FormControl,
			"email":FormControl,
			"mobile_no":FormControl,
			"address_line1":FormControl,
			"address_line2":FormControl,
			"city":FormControl,
			"state":FormControl,
			"country":FormControl,
			"pincode":FormControl,
			"district":FormControl,
			"taluk":FormControl
		
		  }

		    this.grade=wizard.currentStep; 
		  //consolelog(this.grade);
		  
		 
		  switch (this.grade) {
			
            case 1: {
				
					 if(controls['user_id'].value==null || controls['user_id'].value=="" ||
				controls['f_name'].value==null || controls['f_name'].value=="" ||
				 controls['l_name'].value==null || controls['l_name'].value=="" ||
				 controls['pincode'].value==null || controls['pincode'].value=="" ||
				 controls['mobile_no'].value==null || controls['mobile_no'].value=="" ||
				 controls['email'].value==null || controls['email'].value=="" ||
				 controls['address_line1'].value==null || controls['address_line1'].value=="" ||
				 controls['address_line2'].value==null || controls['address_line2'].value=="" ||
				 controls['country'].value==null || controls['country'].value=="" ||
				 controls['state'].value==null || controls['state'].value=="" ||
				 controls['city'].value==null || controls['city'].value=="" || this.duplicated==true || this.duplicated1==true || this.duplicated2 ==true ){ 
					  Object.keys(firstPageControls).forEach(controlName =>
                        controls[controlName].markAsTouched()
                    );
                    // wizard.on('beforeNext', function(wizardObj) {
					// 	wizardObj.goTo(1);
                    //  //wizardObj.start();
					wizard.stop();
					
                    // });
                  
				 }
				 else
				 			{   
								// wizard.on('beforeNext', function(wizardObj) {
								// 	wizardObj.start();
								// 	wizardObj.goTo(2);
								//   });
								  
								//   //consolelog('hi1');
								wizard.start();
								wizard.goTo(2);
								//this.merchand.value(manger)=this.selectedItems
								console.log(this.dd_as);
								 if(this.dd_as.id != null)
								 {
								 	this.dropdown(this.dd_as.id);
									//this.dd_as=[];
								 this.selectedItems=JSON.parse(this.edit_id.category);
								}
							 }
							
		 }

break;

	}
		
	}

	onSubmit() {
		
if(this.merchand.invalid)
{
	const wizard = new KTWizard(this.el.nativeElement, {
			startStep: 1  });
	Swal.fire(
       'Error!',
       'Please Check ',
       'error'
     )
}
else
{
//consolelog(this.merchand.value);
 
	this.activeModal.dismiss(this.merchand.value);
	this.commonSer.Modal_close();
}
	}
	reset()
	{
		this.commonSer.Modal_close();
	}
	
	edit_merchand()
	{
		this.selectedItems = JSON.parse(this.edit_id.category);
		 var geo_data=JSON.parse(this.edit_id.p_geo_data);
		this.merchand = this.fb.group({
			id:[this.edit_id.id],
		  user_id: [  this.edit_id.user_id , Validators.compose([
		 Validators.required
	   ])
	  ],
	  f_name: [this.edit_id.f_name, Validators.compose([
		Validators.required
			  ])
	 ],
	 l_name: [this.edit_id.l_name, Validators.compose([
		Validators.required
	])
	],
	mobile_no: [this.edit_id.mobile_no, Validators.compose([
	  Validators.required,
	  Validators.pattern(this.phonePattern)
	  
	])
	],
	email: [this.edit_id.email, Validators.compose([
	  Validators.required,
	  //Validators.pattern(this.emailPattern)
	  Validators.email
	])
	],
	address_line1: [geo_data.address1, Validators.compose([
	  Validators.required,
	
	])
	],
	address_line2: [geo_data.address2, Validators.compose([
	  Validators.required,
	 
	])
	],
	city: [geo_data.city, Validators.compose([
	  Validators.required,
	 
	])
	],
	state: [geo_data.state, Validators.compose([
		Validators.required,
		
	  ])
	  ],
	country: [geo_data.country, Validators.compose([
	  Validators.required,
	  
	])
	],
	pincode: [geo_data.pincode, Validators.compose([
		Validators.required,
		
	  ])
	  ],
	
	district:[geo_data.district,Validators.compose([
	 Validators.required
		])
		],
		taluk:[geo_data.taluk,Validators.compose([
	 Validators.required
		])
		],
		alt_mobile_no:[this.edit_id.alt_mobile_no,Validators.compose([
	  Validators.pattern(this.phonePattern)
		])
		],
		manger:[this.selectedItems,Validators.compose([
	 Validators.required
		])
		]
		});
		console.log(this.selectedItems)
	 /*var state12=csc.  :(geo_data.state);
	var district12=csc.getDistrictsById(geo_data.district);
	var taluk12=csc.getTalukById(geo_data.taluk);*/
	this.district=csc.getDistrictById(geo_data.state);
	this.taluk=csc.getTalukOfDistrict(geo_data.district);
	this.state=csc.getStatesOfCountry(geo_data.country);
   
	this.dd_as=csc.getTalukById(geo_data.taluk);
	//this.dd_manger()
	//this.dropdown(this.dd_as.id);
	console.log(this.selectedItems);
	//console.log(dd_as.name);
	
	}
	
	onEdit()
	{
	
if(this.merchand.invalid)
{
	Swal.fire(
       'Error!',
       'Please Check ',
       'error'
     )
}
else
{
	
	this.activeModal.dismiss(this.merchand.value);
	this.commonSer.Modal_close();
}
	}
	use_data()
	{
		var data = "";
 var method = "post";
 var url = "get_merchand";
 this.auth.servicepost(data, method, url, 'application/json')
   .subscribe(data => {
	   for(let c of data.result){
		   if(c.role_id == 3)
		   {
	  ELEMENT_DATA.push(c);
   this.allDataExcel=ELEMENT_DATA;
	
 
		   }
		
	}
   });
	}

	// duplicate  
	applyFilter(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
		var sample= this.allDataExcel.filter(question => question.user_id.toLowerCase()== filterValue ) ;
		if(this.edit_id!=null  && sample.length>0)
		{
		if(sample.length>0 && sample[0]['user_id'].toLowerCase() ==this.edit_id.user_id.toLowerCase())
		{
			//consolelog(sample);
		 this.duplicated =false;
		}
		else{this.duplicated =true;}
	}
	else
	{
		if(sample.length >0)
		{
			//consolelog(sample);
		 this.duplicated =true;
		}
		else{this.duplicated =false;}
	}
	  }
	  applyFilter1(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
		var sample= this.allDataExcel.filter(question => question.email.toLowerCase() == filterValue ) ;
		
		if(this.edit_id!=null  && sample.length>0)
		{
		if(sample.length>0 && sample[0]['email'].toLowerCase() ==this.edit_id.email.toLowerCase())
		{
			//consolelog(sample);
		 this.duplicated1 =false;
		}
		else{this.duplicated1 =true;}
	}
	else
	{
		if(sample.length >0)
		{
			//consolelog(sample);
		 this.duplicated1 =true;
		}
		else{this.duplicated1 =false;}
	}
	  }
	  applyFilter2(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
		var sample= this.allDataExcel.filter(question => question.mobile_no== filterValue ) ;
		if(this.edit_id!=null  && sample.length>0)
		{
		if(sample.length>0 && sample[0]['mobile_no'] ==this.edit_id.mobile_no)
		{
			//consolelog(sample);
		 this.duplicated2 =false;
		}
		else{this.duplicated2 =true;}
	}
	else
	{
		if(sample.length >0)
		{
			//consolelog(sample);
		 this.duplicated2 =true;
		}
		else{this.duplicated2 =false;}
	}
	  }
}
//interface  data structure
export interface User {
	id:number;
  user_id: string;
  role_id:string;
    f_name: string;
	l_name:string;
    email:string;
	mobile_no:string;
category:string;
manger:string;
alt_mobile_no:string;
district:string;
taluk:string;
p_geo_data:string;
  }
var ELEMENT_DATA=[];
var manger:any=[];
var manger_name=[];