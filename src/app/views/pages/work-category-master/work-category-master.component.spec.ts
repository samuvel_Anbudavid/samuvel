import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkCategoryMasterComponent } from './work-category-master.component';

describe('WorkCategoryMasterComponent', () => {
  let component: WorkCategoryMasterComponent;
  let fixture: ComponentFixture<WorkCategoryMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkCategoryMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkCategoryMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
