import { NgModule, ViewChild, ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginator, MatSort } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectionModel } from '@angular/cdk/collections';
import{CommonSerService}from '../../../core/commonser';
import { AuthService } from '../../../core/auth';

//import{Http}from'@angular/http';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class WorkCategoryMasterModule { 
  displayedColumns = ['select', 'id', 'store_type', 'store_description',
  'actions'];
 dataSource = ELEMENT_DATA;
 // dataSource: UsersDataSource;
 // displayedColumns = ['select', 'id', 'store_type', 'store_description', 'fullname', '_roles', 'actions'];
 @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
 @ViewChild('sort1', {static: true}) sort: MatSort;
 // Filter fields
 @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
 //lastQuery: QueryParamsModel;
 constructor(private modalService: NgbModal,private service: AuthService,private common:CommonSerService) {}
 selection = new SelectionModel<User>(true, []);
 usersResult: User[] = [];

 openCentred(content) {
    // this.modalService.open(content, { centered: true } );
     this.common.openCentred(content,'md')
 }

 
ngOnInit() {
 this.retrieve_document_master;
}


retrieve_document_master() {
 var data = "";
 var method = "post";
 var url = "get_all_store";
 this.service.servicepost(data, method, url, 'application/json')
   .subscribe(
    
   )
   console.log('')

}

fetchUsers() {
 const messages = [];
 this.selection.selected.forEach(elem => {
   messages.push({
     text: `${elem.fullname}, ${elem.store_description}`,
     id: elem.id.toString(),
     status: elem.store_type
   });
 });
 // this.layoutUtilsService.fetchElements(messages);
}

/**
* Check all rows are selected
*/
isAllSelected(): boolean {
 const numSelected = this.selection.selected.length;
 const numRows = this.usersResult.length;
 return numSelected === numRows;
}

/**
* Toggle selection
*/
masterToggle() {
 if (this.selection.selected.length === this.usersResult.length) {
   this.selection.clear();
 } else {
   this.usersResult.forEach(row => this.selection.select(row));
 }
}
/** FILTRATION */
filterConfiguration(): any {
 const filter: any = {};
 const searchText: string = this.searchInput.nativeElement.value;

 filter.lastName = searchText;

 filter.store_type = searchText;
 filter.store_description = searchText;
 filter.fillname = searchText;
 return filter;
}


}




export interface User {
 id: number;
   store_type: string;
   store_description:string;
   fullname: string;
   
 
}
const ELEMENT_DATA: User[] = [
 {id: 1,fullname: 'Hydrogen', store_type: 's', store_description: 'H'},
 {id: 2,fullname: 'Helium', store_type: 's', store_description: 'He'},
 {id: 3,fullname: 'Lithium', store_type:'s', store_description: 'Li'},
 {id: 4,fullname: 'Beryllium', store_type: 'sam', store_description: 'Be'},
 {id: 5,fullname: 'Boron', store_type: 'medium', store_description: 'B'},
 {id: 6,fullname: 'Carbon', store_type: 'large',store_description: 'C'},
 {id: 7,fullname: 'Nitrogen', store_type: 'small', store_description: 'N'},
 {id: 8,fullname: 'Oxygen', store_type: 'large', store_description: 'O'},
 {id: 9,fullname: 'Fluorine', store_type: 'large', store_description: 'F'},
 {id: 10,fullname: 'Neon', store_type: 'lare', store_description: 'Ne'},
 {id: 11,fullname: 'Sodium', store_type:'small', store_description: 'Na'},
 {id: 12,fullname: 'Magnesium', store_type: 'feel', store_description: 'Mg'},
 {id: 13,fullname: 'Aluminum', store_type: 'tee', store_description: 'Al'},
 {id: 14,fullname: 'Silicon', store_type: 'hghfg', store_description: 'Si'},
 {id: 15,fullname: 'Phosphorus', store_type: 'mmn', store_description: 'P'},
 {id: 16,fullname: 'Sulfur', store_type: 'nbj', store_description: 'S'},
 {id: 17,fullname: 'Chlorine', store_type: 'kmk', store_description: 'Cl'},
 {id: 18,fullname: 'Argon', store_type: 'kjk', store_description: 'Ar'},
 {id: 19,fullname: 'Potassium', store_type: 'kjk', store_description: 'K'},
 {id: 20,fullname: 'Calcium', store_type: 'poio', store_description: 'Ca'},
];

