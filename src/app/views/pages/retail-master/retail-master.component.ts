import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../core/auth';
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import {TestingComponent} from '../testing/testing.component'
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {SelectionModel} from '@angular/cdk/collections';
import csc from 'country-state-city';
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import {LoaderComponent} from '../loader/loader.component';
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';
import{CommonSerService} from'../../../core/commonser';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
import swal from 'sweetalert2';
import _remove from 'lodash/remove';
import { from } from 'rxjs';
//import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

type AOA = any[][];


@Component({
  selector: 'kt-retail-master',
  templateUrl: './retail-master.component.html',
  styleUrls: ['./retail-master.component.scss']
})

export class RetailMasterComponent implements OnInit {
  recordcheck: boolean;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;

  private displayedColumns = ['select','s_no','retail_id','retailer','branding_elements',
  'promo','planogram','check_list','assigned_manager','action'];
  private displayedColumnsBrandingElements = ['s_no','name','material','dimensions','measurement','uom','quantity'];
  private displayedColumnsPromo = ['s_no','product_category','name','quantity'];
	private displayedColumnsProduct=[];
  private displayedColumnsCheckList = ['s_no','question','ans_category'];
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  private be: AOA = [[]];
  
  private dataSource;
  private dataSourceBE;
  private dataSourcePromo;
  private dataSourceProductsDisplay;
  private dataSourceProductsSellable;

  private dataSourceCheckList;
  showMyContainer: boolean = false;
private dataLength:boolean;
  private selection;
  private retail_list:FormGroup;
  private uploadGroup: FormGroup;

  private modalTitle:String;
  private models;
  private modalTableTitle:String;
  private allData:String;
  private loader:boolean=true;
  private filedataBasic:any;
  private filedataBE:any;
  private filedataPromo:any;
  private filedataCheckList:any;
  private filedataProductGrouping:any;
  private headerArrayBasic=['S.No','Retail ID','Retailer','Retail Category','Email ID','Landline Number',
  'Mobile Number','Address Line 1','Address Line 2','Town/Village','Taluk','District','State','Country',
  'Pincode','Business Partner Name','Business Partner Email','TL Name','TL Email ID'];
  private headerArrayBE=['S.No','Retail ID','Branding Element','Quantity'];
  private headerArrayPromo=['S.No','Retail ID','Promo Material','Quantity'];
  private headerArrayCheckList=['S.No','Retail ID','Questions'];
  private headerArrayProducts=['S.No','Retail ID','Product','Type','Quantity'];
  private Basic_Excel=[];
  private allHeadingsUploadExcel=['Basic Excel','Branding Elements','Promo Materials','Products','Check List'];
  private allFailedUploadExcel:any=['failed_retailids_basic','failed_be','failed_promo','failed_products','failed_checklist'];
  private allFailedUploadExcelNames=['be_ids','promo_ids','pro_gro_ids','question_ids'];

  private headerUploadExcel=['Basic Excel','Branding Elements','Promo Materials','Products','Check List'];

  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;

 /* private excelBasic:any;
  private excelBE:any;
  private excelPromo:any;
  private excelCheckList:any;*/


  private fileType:String;
  private targetFile:any;
  private allFiles=[];


 // public files: NgxFileDropEntry[] = [];
 // public fileList: NgxFileDropEntry[] = [];



private data: string = "I like this read-more component because it's very helpful. This tutorial so good. I will share it with others.I like this read-more component because it's very helpful. This tutorial so good. I will share it with others.thnks";
  dedata: any=[];
  prodata: any=[];
  progrpdata: any=[];
  checkdata: any=[];
  totalSize1: any;
  filterData: any;
  RawData: any;
  RawData_checks: any;
  RawData_pro: any;
  RawData_diplay: any[];
  RawData_sellable: any[];
 
 

  constructor(private modalService: NgbModal,private fb: FormBuilder,private cdr: ChangeDetectorRef,
    private auth: AuthService,private overlay: Overlay,private layoutUtilsService: LayoutUtilsService,private commonSer:CommonSerService ) { 
      this.dataSource = new MatTableDataSource();
      this.dataSourceBE = new MatTableDataSource();
      this.dataSourcePromo = new MatTableDataSource();
      this.dataSourceProductsDisplay = new MatTableDataSource();
      this.dataSourceProductsSellable = new MatTableDataSource();
      
      this.dataSourceCheckList = new MatTableDataSource();


      this.selection = new SelectionModel();

    }

  ngOnInit() {
    this.overlayRef = this.overlay.create({	
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),	
      hasBackdrop: true	
    });	
      this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.getRetailList();
    this.Checker_data();
    this.overlayRef.detach();
  }
 
  // Check data -promo,be,Etc
  Checker_data() 
  {
    this.overlayRef.attach(this.LoaderComponentPortal); 
    this.auth.serviceget('get_all_branding_elements').pipe(tap(result => { this.dedata= result['data']}),).subscribe();
    this.auth.servicepost('', 'post', 'get_all_promotional', 'application/json').subscribe(data => {this.prodata=data['result']});  
    this.auth.serviceget('get_all_products').pipe( tap(result => {this.progrpdata= result['data_r']}),).subscribe(); 
    this.auth.servicepost('', 'post', 'get_all_checklist_master', 'application/json').subscribe(data => { this.checkdata=data['result'] });
   // this.overlayRef.detach();
   if(this.dedata.length>0 && this.prodata.length>0 && this.progrpdata.length>0 && this.checkdata.length>0)
   {
     this.recordcheck=true;
   }
   else
   {
    this.recordcheck=false;
   }
   return  this.recordcheck;
  }
  
  openModal(data) {
 
if(this.dedata.length>0 && this.prodata.length>0 && this.progrpdata.length>0 && this.checkdata.length>0)
{
    const modalRef = this.modalService.open(TestingComponent,
     { windowClass : "myCustomModalClass"}
      
      );
    //  modalRef.componentInstance.id = 10;
      modalRef.result.then((result) => {
        //consolelog(result);
        this.getRetailList();
      }).catch((error) => {
        this.getRetailList();
      });
      ////consolelog(data);
      modalRef.componentInstance.edit_data=data;
      modalRef.componentInstance.all_data=this.allData;
    }
    else
    {
      swal.fire(
        'Warning!',
        //'Kindly Check  Below Table: \n 1.Branding Elements \n 2.Promo Materials \n 3.Planogram  & \n 4.Check List',
        'Masters - Branding Elements,Promo-Materials,Planogram,Check List should not be empty',
        'warning'
    )
    }
  }

  getRetailList(){
    this.overlayRef.attach(this.LoaderComponentPortal);
    console.log(this.dataSource)
    const url='get_all_retails';
    this.auth.serviceget(url).pipe(
	//	this.auth.getRetailList().pipe(
		  tap(result => {
      //consolelog(result);
      this.dataSource.data=result['data'];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;		
      this.allData=result['data'];
      this.cdr.detectChanges();		
      this.loader=false;
      this.selection = new SelectionModel(true, this.dataSource.data.filter(t=> t.IsAssigned));

		  }),
   
  
    ).subscribe();
    this.overlayRef.detach();
    }



    viewBasicInfo(index){
      
      this.modalTitle='Basic Information';
      this.models=this.dataSource.data[index];
      //consolelog(index);
      //consolelog(this.models);
      //consolelog(JSON.parse(this.models['geo']));
      var geo =  JSON.parse(this.models['geo']);
      var country=csc.getCountryById(geo['country']);
      this.models.country=country.name;

      var state=csc.getStateById(geo['state']);
      this.models.state=state.name;

      var district=csc.getDistrictsById(geo['district']);
      this.models.district=district.name;

      var taluk=csc.getTalukById(geo['taluk']);
      this.models.taluk=taluk.name;

      this.models.city=geo['city'];
      this.models.pincode=geo['pincode'];

    }
    viewBEInfo(index){
this.modalTableTitle='Assigned Branding Elements';

this.RawData=JSON.parse(this.dataSource.data[index]['branding_elements']);
this.dataSourceBE.data=this.commonSer.iterator(this.RawData,this.currentPage,this.pageSize)
this.totalSize=this.dataSourceBE.data.length;
console.log(this.dataSourceBE.data)
this.cdr.detectChanges();
console.log(this.dataSourceBE.data)
//consolelog(this.dataSourceBE.data);
    }
    viewCheckListInfo(index){
      this.modalTableTitle='Assigned Check List';
      this.RawData_checks=JSON.parse(this.dataSource.data[index]['check_list']);
      this.dataSourceCheckList.data=this.commonSer.iterator(this.RawData_checks,this.currentPage,this.pageSize);
      console.log(this.dataSourceCheckList)
     this.totalSize=this.dataSourceCheckList.data.length;
     this.cdr.detectChanges();
     console.log(this.dataSourceCheckList)

    }
 
    viewPromoInfo(index){
      this.modalTableTitle='Assigned Promo Materials';
      this.RawData_pro=JSON.parse(this.dataSource.data[index]['promo']);
      this.dataSourcePromo.data=this.RawData_pro;
      this.totalSize=this.dataSourcePromo.data.length;
      this.cdr.detectChanges();
      
          }
          viewProInfo(index){
            this.modalTableTitle='Planogram';
            var test = JSON.parse(this.dataSource.data[index]['products']);
            var displayProducts=[];
            var sellableProducts=[];
            this.displayedColumnsProduct=Object.keys(test[0]);
            test.forEach(element => {
              if(element['Type']==='Display'){
                displayProducts.push(element);
              }
              else{
                sellableProducts.push(element);
              }
            });
     
            // this.dataSourceProductsDisplay.data=displayProducts;
            // this.dataSourceProductsSellable.data=sellableProducts;

            this.RawData_diplay=displayProducts;
            this.RawData_sellable=sellableProducts;
            this.dataSourceProductsDisplay.data=this.commonSer.iterator(displayProducts,this.currentPage,this.pageSize)
            this.totalSize=this.dataSourceProductsDisplay.data.length;
            this.dataSourceProductsSellable.data=this.commonSer.iterator(sellableProducts,this.currentPage,this.pageSize)
            this.totalSize1=this.dataSourceProductsSellable.data.length;
            this.cdr.detectChanges();
          }
   

  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;		
  }
  openLarge(content,size) {
    //consolelog(size);
    this.modalService.open(content, { windowClass : "myCustomModalClass"});
  }
  deleteRetail(item:number){
    swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Record!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
  
      if (result.value) {
        this.overlayRef.attach(this.LoaderComponentPortal);
  
      this.auth.deleteRetail(item).pipe(
        tap(result => {
          this.overlayRef.detach();
  
          if(result['status']=="success"){
            const message='Your record has been deleted.';
            this.getRetailList();
            swal.fire('Success',message,'success');
                  }
          else{ 
  
            const message='Failed to delete.';
            swal.fire('Error',message,'error');
                  }
        }),
        
      ).subscribe(err => {
        //consolelog(err);
      });
      }
    })  
  
  
  }
  fetchRetails(){
    const messages = [];
    const sNos = [];
    this.selection.selected.forEach(element => {
      sNos.push(element['s_no']);
    });
    
    swal.fire({
      title: "Are you sure?",
      text: " You want to Delete the selected records!",
      icon:"question",
      showConfirmButton: true,
      showCancelButton: true     
      })
      .then((willDelete) => {
       
      if(willDelete.value){
        this.overlayRef.attach(this.LoaderComponentPortal);
  
        this.auth.bulkDeleteRetails(sNos).pipe(
          tap(result => {
            this.overlayRef.detach();
  
            this.getRetailList();
            if(result['status']==="success"){
            const message ="Deleted successfully!"
            swal.fire('Success',message,'success');
          //  this.layoutUtilsService.showActionNotification(message);  
            }
            else{
              const message ="Deletion failed!"
              swal.fire('Success',message,'error');
            }
          }),
      
        ).subscribe();
           
        //  swal.fire("Success");
      }else{
        const message ="Deletion Rejected!"
        this.layoutUtilsService.showActionNotification(message); 
      }
    
    });
  }

  masterToggle() {
		if (this.selection.selected.length === this.dataSource.data.length) {
			this.selection.clear();
		} else {
			this.dataSource.data.forEach(row => this.selection.select(row));
		}
  }
  isAllSelected(): boolean {
	const numSelected = this.selection.selected.length;
	const numRows = this.dataSource.data.length;
	return numSelected === numRows;
}
applyFilter(filterValue: string) {
  
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}



arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;
    for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}
  openMedium(content,size) {
    //consolelog(size);
    // this.modalService.open(content, { windowClass : "myCustomModalClass"});
    this.commonSer.openCentred(content,'lg');
  }
openLg(content) {
 this.commonSer.openCentred(content,'lg');
}
onFileChange(evt: any) {
  //this.overlayRef.attach(this.LoaderComponentPortal);

  const target: DataTransfer = < DataTransfer > (evt.target);
  var excelBasic,excelBE,excelPromo,excelCheckList,excelProducts:any=[];
  var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
  if (target.files.length !== 1) {
     // this.overlayRef.detach();

      swal.fire('Multiple files not allowed.');
      return false;
  }
  if (validExts.indexOf(target.files[0]['type']) < 0) {

   //   this.overlayRef.detach();

      swal.fire('Only file with extensions .xlsx, .xls, .csv is allowed.');
      return false;

  } else {
      var sheet = name;


      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
          /* read workbook */
          const bstr: string = e.target.result;
          const wb: XLSX.WorkBook = XLSX.read(bstr, {
              type: 'binary'
          });

          /* grab first sheet */
          const wsname: string = wb.SheetNames[0];
          const ws: XLSX.WorkSheet = wb.Sheets[wsname];

          /* save data */
          this.be = < AOA > (XLSX.utils.sheet_to_json(ws, {
              header: 1,
              blankrows: false,
              defval: "",
          }));
          if (this.be.length === 0) {
              this.incorrectSheetAlert();
              return false;
          }
          this.targetFile=target.files[0]['name'];

          switch (this.fileType) {
              case 'Basic': {
                this.filedataBasic='';
                this.allFiles['basic']=[];

                  var resultBasic = this.arraysEqual(this.headerArrayBasic, this.be[0]);
                  if (resultBasic === true) {
                      this.filedataBasic = target.files[0]['name'];
                      excelBasic = this.be;
                      if(excelBasic.length>1){
                        for(var i=1;i<excelBasic.length;i++){
                          var stateId=csc.getStatesByName(excelBasic[i][12]);
                          var districtId=csc.getDistrictByName(excelBasic[i][11]);
                          var talukId=csc.getTalukByName(excelBasic[i][10]);
                          excelBasic[i][13]=101;
                          excelBasic[i][12]=stateId[0]['id'];
                          excelBasic[i][11]=districtId[0]['id'];
                          excelBasic[i][10]=talukId[0]['id'];
                      
                        }
                
                        this.allFiles['basic']=excelBasic;
                      }
                      else{
                        this.incorrectSheetAlert();
                      }
                  } else {
                      this.incorrectSheetAlert();
                  }

                  break;
              }
              case 'Branding_Element': {
                this.filedataBE='';
                var resultBE=[];
                this.allFiles['be']=[];

                resultBE=this.uploadExcelvalidation(excelBE,this.headerArrayBE);
                if(resultBE.length>1){
                  this.filedataBE = target.files[0]['name'];
                  excelBE=resultBE;
                 // if(excelBE.length>1){
                    this.allFiles['be']=excelBE;
                //  }
                }
                else{
                  this.incorrectSheetAlert();
                }
              
               


                  break;
              }
              case 'Promo_Material':{
                this.filedataPromo='';
                this.allFiles['promo']=[];

                var resultPro=[];
                resultPro=this.uploadExcelvalidation(excelPromo,this.headerArrayPromo);

                if(resultPro.length>1){
                  this.filedataPromo = target.files[0]['name'];
                  excelPromo=resultPro;
                  //if(excelPromo.length>1){
                    this.allFiles['promo']=excelPromo;

                   // this.allFiles.push({'promo': excelPromo});

               //   }
                }
                else{
                  this.incorrectSheetAlert();
                }
              
                break;

              }

              case 'Product_Grouping':{
                this.filedataProductGrouping='';
                this.allFiles['products']=[];
                var resultProducts=[];
                resultProducts=this.uploadExcelvalidationProducts(excelProducts,this.headerArrayProducts);
                //consolelog(resultProducts);
                //return false;
                if(resultProducts.length>1){
                  this.filedataProductGrouping = target.files[0]['name'];
                  excelProducts=resultProducts;
                    this.allFiles['products']=excelProducts;


               
                }
                else{
                  this.incorrectSheetAlert();
                }
              

              break;

              }
              case 'Check_List':{
                this.filedataCheckList='';
                this.allFiles['check_list']=[];
                var resultCheckList = this.arraysEqual(this.headerArrayCheckList, this.be[0]);
                if (resultCheckList === true) {
                    this.filedataCheckList = target.files[0]['name'];
                    excelCheckList = this.be;
                    //consolelog(excelCheckList);
                    if(excelCheckList.length>1){
                      this.allFiles['check_list']=excelCheckList;
                     // this.allFiles.push({'check_list': excelCheckList});

                    }
                    else{
                      this.incorrectSheetAlert();
                    }
                } else {
                    this.incorrectSheetAlert();
                }

                break;
              }

             

          }
          //consolelog(this.allFiles);
        
         



      };
      reader.readAsBinaryString(target.files[0]);
  }
//  evt.target = [];

}
closeModal(){
  this.commonSer.Modal_close();
  this.filedataBasic='';
  this.filedataBE='';
  this.filedataProductGrouping='';
  this.filedataPromo='';
  this.filedataCheckList='';
  //,this.filedataBE,this.filedataPromo,this.filedataProductGrouping,this.filedataCheckList='';
}
UploadDone(warningPop){
//  if(this.allFiles.length)this.commonSer.Modal_close();

  
  //consolelog(this.allFiles);
  if((typeof this.allFiles['basic'] !== 'undefined' && this.allFiles['basic'].length!=0)||
  (typeof this.allFiles['be'] !== 'undefined' &&this.allFiles['be'].length!=0)||
  (typeof this.allFiles['promo'] !== 'undefined' &&this.allFiles['promo'].length!=0)||
  (typeof this.allFiles['check_list'] !== 'undefined' &&this.allFiles['check_list'].length!=0)||
  (typeof this.allFiles['products'] !== 'undefined' &&this.allFiles['products'].length!=0)){


  const formData: FormData = new FormData();

  formData.append('basic', JSON.stringify(this.allFiles['basic']));
  formData.append('be', JSON.stringify(this.allFiles['be']));
  formData.append('promo', JSON.stringify(this.allFiles['promo']));
  formData.append('check_list', JSON.stringify(this.allFiles['check_list']));
  formData.append('products', JSON.stringify(this.allFiles['products']));


  this.auth.uploadRetaildetails(formData).pipe(
    tap(result => {
      //consolelog(result);
      if(result['status']=="success"){
        this.allFailedUploadExcel[0]=result['failed_retailids_basic'];
        this.allFailedUploadExcel[1]=result['failed_be'];
        this.allFailedUploadExcel[2]=result['failed_promo'];
        this.allFailedUploadExcel[3]=_remove(result['failed_products'], x => x['pro_gro_ids'].length !== 0);
       // //consolelog(this.allFailedUploadExcel[3]);
       // this.allFailedUploadExcel[3]=result['failed_products'];
       this.allFailedUploadExcel[4]=_remove(result['failed_checklist'], x => x['question_ids'].length !== 0);

      //  this.allFailedUploadExcel[4]=result['failed_checklist'];



this.openLg(warningPop);
//this.overlayRef.detach();
//this.ngOnInit();
        this.getRetailList();
  //    this.commonSer.Modal_close();

      }
      else{
        //  this.layoutUtilsService.showActionNotification(`Failed to upload. Please try again.`, MessageType.Update, 5000, true, true);
       // this.overlayRef.detach();
    
          swal.fire('Failed to upload. Please try again.');
      }
      
    }),

  ).subscribe();
this.allFiles=[];
  }
  else
  {
    swal.fire('No files have been chosen / File should not be empty. Please try again.');
  }
}
uploadExcelvalidationProducts(currentlyUploaded,actualHeaders){
  var divisibility = (((this.be[0].length - 2) % 3) === 0);
  //consolelog(this.be[0].length);
  //consolelog(((this.be[0].length - 2) % 3));
  if ((divisibility === true) && (this.be[0].length > 2)) {
    if (this.headerCheckProducts(this.be[0],actualHeaders) === true) {
      alert("if");
      currentlyUploaded = this.be;
     
    } else {
      alert("else");
      currentlyUploaded=[];
        this.incorrectSheetAlert();

    }
} else {
  alert("else else");
  currentlyUploaded=[];
    this.incorrectSheetAlert();

}
return currentlyUploaded;
}

uploadExcelvalidation(currentlyUploaded,actualHeaders){
  var divisibility = (((this.be[0].length - 2) % 2) == 0);

  if ((divisibility === true) && (this.be[0].length > 2)) {
      if (this.headerCheck(this.be[0],actualHeaders) === true) {
        currentlyUploaded = this.be;
       
      } else {
        currentlyUploaded=[];
          this.incorrectSheetAlert();

      }
  } else {
    currentlyUploaded=[];
      this.incorrectSheetAlert();

  }
  return currentlyUploaded;
}
headerCheckProducts(headers,actualHeaders){
  var result:boolean=true;
  for (var i = 0; i < headers.length; ++i) {
    
    if(i<2){
      //consolelog("inside 1");
      //consolelog(result);
      if ((headers[i].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()) !== (actualHeaders[i].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())) result = false;
    }
    else{
      if((i === 2) || (i % 3 === 2)) { // ID check
     
       if((headers[i].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()) !== (actualHeaders[2].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())){
         result = false;
       }
       //consolelog(result);
    }
    if(i % 3 === 0) { // Type check
    
      if((headers[i].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()) !== (actualHeaders[3].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())) result = false;
   }
    if(i % 3 === 1) { // Quantity check
  
      if((headers[i].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()) !== (actualHeaders[4].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())) result = false;
   }
   
  }
  
  }
  return result;

}

incorrectSheetAlert(){
  this.overlayRef.detach();

  swal.fire('Incorrect Sheet Uploaded.Refer Sample Sheet!.');
}

headerCheck(headers,actualHeaders){
  var result:boolean=true;
  for (var i = 0; i < headers.length; ++i) {
    
    if(i<2){
      //consolelog("inside 1");
      if (headers[i] !== actualHeaders[i]) result = false;
    }
    else{
      if(i % 2 === 0) { // index is even
        //consolelog("inside 2");
       if( headers[i] !== actualHeaders[2]) result = false;
    }
    if(i % 2 === 1) { // index is odd
      //consolelog("inside 3");
      if( headers[i] !== actualHeaders[3]) result = false;
   }
  }
  
  }
  return result;

}

export(action): void {
  var myClonedArray1 = JSON.parse(JSON.stringify(this.allData));
  var myClonedArray = myClonedArray1;
  //consolelog(myClonedArray);
    var sheetName:String;
    var allSheets=[];
    var allSheetsNames=["Retail_Basic","Retail_Branding_Elements","Retail_Promo_Details",
    "Retail_Product_Grouping","Retail_Checklist"];
    var commonColms=['S.No','Retail ID'];
    var ext=".xlsx";
    var downloadArray= [];
    var testArray= [];

    var downloadArrayPromo= [];
    var testArrayPromo= [];
    var allPromo=[];
    var objKeysPromo;
    var alllengthPromo=[];

    var downloadArrayBE= [];
    var testArrayBE= [];
    var allBE=[];
    var objKeysBE;
    var alllengthBE=[];

    var downloadArrayCheckList= [];
    var testArrayCheckList= [];
    var allCheckList=[];
    var objKeysCheckList;
    var alllengthCheckList=[];

    var downloadArrayProducts= [];
    var testArrayProducts= [];
    var allProducts=[];
    var objKeysProducts;
    var alllengthProducts=[];





    var txt;
    switch(action) { 
      case 'db': { 
        txt='You want to Download the data!';
        testArray.push(this.headerArrayBasic);
       
        for(var k=0;k<myClonedArray.length;k++) {
          var geo=JSON.parse(myClonedArray[k]['geo']);
          

      //Basic
   
      var taluk=csc.getTalukById(String(geo['taluk']));
      var district=csc.getDistrictsById(String(geo['district']));
      var state=csc.getStateById(String(geo['state']));
      var country=csc.getCountryById(String(geo['country']));
     


          downloadArray=[k+1,myClonedArray[k]['retail_id'],myClonedArray[k]['retailer'],
          myClonedArray[k]['retail_category'],myClonedArray[k]['email'],
          myClonedArray[k]['landline'],myClonedArray[k]['mobile_no'],myClonedArray[k]['address_line1'],
          myClonedArray[k]['address_line2'],geo['city'],taluk['name'], district['name'],state['name'],country['name'],
          geo['pincode'],myClonedArray[k]['business_partner_name'],myClonedArray[k]['business_partner_email'],
          myClonedArray[k]['tl_name'],myClonedArray[k]['tl_email']];
          testArray.push(downloadArray);

          //start of Branding Elements
          var bedwn=JSON.parse(myClonedArray[k]['branding_elements']);
          alllengthBE.push(bedwn.length);
          objKeysBE=Object.keys(bedwn[0]);
          downloadArrayBE=[k+1,myClonedArray[k]['retail_id']];
          var testingbe=[];
          var joinbe=[];
          bedwn.forEach((element,index) => {
            var beValues:any=Object.values(element);
              beValues.forEach((element1,index1) => {
              if(element1=="null"){
               beValues[index1]="NA";
             

              }
              if(index1===3){
                element1.forEach(function(item, i) {
                   if (item == "null") element1[i] = "NA";
                   });
                   if(beValues[2]=="1-d") beValues[index1]=element1[0];
               
                   if(beValues[2]=="2-d") beValues[index1]=element1[0]+'*'+element1[1];

                   if(beValues[2]=="3-d") beValues[index1]=element1[0]+'*'+element1[1]+'*'+element1[2];

                   if(beValues[2]=="NA") beValues[index1]="NA";


              }
            
            });
           
            testingbe.push(beValues);
          });
          joinbe.push(testingbe.reduce((acc, val) => acc.concat(val), []));
          allBE.push(downloadArrayBE.concat(joinbe[0]));
          //end of Branding Elements
          

            //start of promo
            var promodwn=JSON.parse(myClonedArray[k]['promo']);
            alllengthPromo.push(promodwn.length);
            objKeysPromo=Object.keys(promodwn[0]);
            downloadArrayPromo=[k+1,myClonedArray[k]['retail_id']];
            var testingpto=[];
            var joinpromo=[];
            promodwn.forEach((element,index) => {
              var promoValues=Object.values(element);
              testingpto.push(promoValues);
       
            });
            joinpromo.push(testingpto.reduce((acc, val) => acc.concat(val), []));
            allPromo.push(downloadArrayPromo.concat(joinpromo[0]));
            allSheets=[testArray];
            //end of promo

             //start of product
          var produtsdwn=JSON.parse(myClonedArray[k]['products']);
          alllengthProducts.push(produtsdwn.length);
          objKeysProducts=Object.keys(produtsdwn[0]);
          downloadArrayProducts=[k+1,myClonedArray[k]['retail_id']];
          var testingproducts=[];
          var joinproducts=[];
          produtsdwn.forEach((element,index) => {
            var productsValues=Object.values(element);
            testingproducts.push(productsValues);
          });
          joinproducts.push(testingproducts.reduce((acc, val) => acc.concat(val), []));
          allProducts.push(downloadArrayProducts.concat(joinproducts[0]));
          //end of product


          //start of checklist
          var checklistdwn=JSON.parse(myClonedArray[k]['check_list']);
          alllengthCheckList.push(checklistdwn.length);
          objKeysCheckList=Object.keys(checklistdwn[0]);
          downloadArrayCheckList=[k+1,myClonedArray[k]['retail_id']];
          var testingchecklist=[];
          var joinchecklist=[];
          checklistdwn.forEach((element,index) => {
            var checklistValues=Object.values(element);
            testingchecklist.push(checklistValues);
          });
          joinchecklist.push(testingchecklist.reduce((acc, val) => acc.concat(val), []));
          allCheckList.push(downloadArrayCheckList.concat(joinchecklist[0]));
          //end of checklist
          
        }

        
        
        var maxBELength=Math.max(...alllengthBE);//arr.shift()
       // objKeysBE=objKeysBE.splice(0, 1);
        //consolelog(objKeysBE);
        //consolelog(commonColms);
        var repeatedBE = [].concat(... new Array(maxBELength).fill(objKeysBE));
        testArrayBE.push(commonColms.concat(repeatedBE));
        allSheets.push(testArrayBE.concat(allBE));

        var maxPromoLength=Math.max(...alllengthPromo);
        var repeatedPromo = [].concat(... new Array(maxPromoLength).fill(objKeysPromo));
        testArrayPromo.push(commonColms.concat(repeatedPromo));
        allSheets.push(testArrayPromo.concat(allPromo));

        var maxProductsLength=Math.max(...alllengthProducts);
        var repeatedProducts = [].concat(... new Array(maxProductsLength).fill(objKeysProducts));
        testArrayProducts.push(commonColms.concat(repeatedProducts));
        allSheets.push(testArrayProducts.concat(allProducts));

        var maxCheckListLength=Math.max(...alllengthCheckList);
        var repeatedChecklist = [].concat(... new Array(maxCheckListLength).fill(objKeysCheckList));
        testArrayCheckList.push(commonColms.concat(repeatedChecklist));
        allSheets.push(testArrayCheckList.concat(allCheckList));

      
        
//consolelog(allSheets);

        break;
      }
      case 'sample':{
        var be=['Branding Element','Quantity','Branding Element','Quantity'];
        var promo=['Promo Material','Quantity','Promo Material','Quantity'];
        var prod=['Product','Type','Quantity','Product','Type','Quantity'];
        var besampleData=['1','Retail_ID 1','Branding Element ID 1','2','Branding Element ID 2','5','Branding Element ID 3','4'];
        var promosampleData=['1','Retail_ID 1','Promo Material ID 1','2','Promo Material 2','5','Promo Material 3','4'];
        var prosampleData=['1','Retail_ID 1','Product ID 1','Sellable','2','Product ID 2','Sellable','5','Product ID 3','Display','4'];
        var checklistsampleData=['1','Retail_ID 1','1,2,3'];



        

        allSheets=[[this.headerArrayBasic],
        [this.headerArrayBE.concat(be),besampleData],
        [this.headerArrayPromo.concat(promo),promosampleData],
        [this.headerArrayProducts.concat(prod),prosampleData],
        [this.headerArrayCheckList,checklistsampleData]];
        
        txt='You want to Download sample sheet!';
        
      break;
      }
      default: { 
   
        break;              
     } 
    }
    
    swal.fire({
      title: "Are you sure?",
      text: txt,
      icon:'question',
      showConfirmButton: true,
      showCancelButton: true     
      })
      .then((willDelete) => {
    
      if(willDelete.value){
            const message ="Downloaded successfully!"
            allSheets.forEach((element,index) => {
              //consolelog(element)
              sheetName=allSheetsNames[index]+ext;
              this.generateSheet1(element,sheetName,action);
            });
           
          swal.fire('Success',message,'success');
      }else{
        const message ="Download Rejected!"
         swal.fire('Rejected!',message,'error');
      }
    
      
    });
  }
  generateSheet1(sheetData,sheetName,name){
    /* generate worksheet */
    
  if((sheetName=='Retail_Basic.xlsx') && (name=='sample'))
  {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', '/assets/sample/Retail_Basic_Sample.xlsx');
    link.setAttribute('download', `Retail_Basic.xlsx`);
    document.body.appendChild(link);
    link.click();
    link.remove();
    
    
  }
  else{
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(sheetData);
  
    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    
    /* save to file */
    XLSX.writeFile(wb, sheetName);
  }
  }
  
  generateSheet(sheetData,sheetName){
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(sheetData);
  
    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    
    /* save to file */
    XLSX.writeFile(wb, sheetName);
  }
 initRegistrationForm() {

    this.retail_list = this.fb.group({
      retail_id: ['', [ Validators.required ] ],
 
  retailer: ['', Validators.compose([
    Validators.required,
    Validators.minLength(5),
    Validators.maxLength(20)
  ])
 ],
 retail_category: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
],
landline: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
],
mobile_no: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
],
email: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
],
address_line1: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
],
address_line2: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
],
business_partner_name: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
],
tl_name: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
],
fileUpload: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
]

 
});
/*this.uploadGroup = this.fb.group({
  check1: ['', Validators.compose([
      Validators.required
  ])]
});*/
 }
 basic()
{
  this.fileType='Basic';
}
brandingElements(){
  this.fileType='Branding_Element';

}
promoMaterial(){
   this.fileType='Promo_Material';

}
checkList(){
  this.fileType='Check_List';

}
productGrouping(){
  this.fileType='Product_Grouping';

}
/*isControlHasError(controlName: string, validationType: string): boolean {
  const control = this.retail_list.controls[controlName];
  if (!control) {
    return false;
  }

  const result = control.hasError(validationType) && (control.dirty || control.touched);
  return result;
}*/

viewmore(data:string){
 
  //consolelog(this.data);
  this.dataLength = !(this.data.length > 30)
  //consolelog(this.dataLength);
  // //consolelog(this.isCollapsed);
  // this.isCollapsed = !this.isCollapsed;
  // //consolelog(this.isCollapsed);

  //  var myContent= document.getElementById("listItem").innerHTML;
  //  if(myContent>30){
     
  //  }
  //  //consolelog(myContent);
}

page_chage(events,data)
{
  
  
  var range=this.commonSer.handlePage(events,data);
  this.dataSourceBE=this.commonSer.iterator(this.RawData,range[0]['start'],range[0]['end']);
  this.dataSourceCheckList=this.commonSer.iterator(this.RawData_checks,range[0]['start'],range[0]['end'])
  this.dataSourcePromo=this.commonSer.iterator(this.RawData_pro,range[0]['start'],range[0]['end'])
  this.dataSourceProductsDisplay=this.commonSer.iterator(this.RawData_diplay,range[0]['start'],range[0]['end'])
  this.dataSourceProductsSellable=this.commonSer.iterator(this.RawData_sellable,range[0]['start'],range[0]['end'])
  this.cdr.detectChanges();
  
}


}
