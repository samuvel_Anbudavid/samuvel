// import { Component, OnInit } from '@angular/core';
import { AfterViewInit, AfterViewChecked, ViewChild } from '@angular/core';
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';
// Angular
import { Component, OnInit, ElementRef, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef,ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatSnackBar,MatTableDataSource ,MatButtonToggleModule,MatRadioModule} from '@angular/material';
import { AuthService } from '../../../core/auth';
import { FormGroup, FormControl, Validators,FormBuilder } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { HttpserviceService } from '../../../httpservice.service';
//import { Http, ResponseContentType, Headers, RequestOptions } from '@angular/http';
// import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import * as XLSX from 'xlsx';
import { from } from 'rxjs';
//import { map } from 'rxjs/operators';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import {LoaderComponent} from '../loader/loader.component';
import { count } from 'rxjs/operators';
import{CommonSerService} from'../../../core/commonser';
// import { OverlayRef } from '@angular/cdk/overlay';

@Component({
  selector: 'kt-check-list-master',
  templateUrl: './check-list-master.component.html',
  styleUrls: ['./check-list-master.component.scss']
})
export class CheckListMasterComponent implements OnInit {

  displayedColumns = ['select', 's_no', 'question_id', 'question_description','answer_category','actions'];
  dataSource=new MatTableDataSource<Question>(ELEMENT_DATA);
 // dataSource=ELEMENT_DATA;
//private dataSource;
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  // Filter fields
	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  //lastQuery: QueryParamsModel;
  //Variable Declare
  selection = new SelectionModel<Question>(true, []);
  usersResult: Question[] = [];
  private allDataExcel= [];
  private fileName: string = 'Checklist Master.xlsx';
  Edit_Question_form:FormGroup;
  Question_form:FormGroup;
  private be: any;
  public valid_dat=[];
  cond=false;
  overlayRef: OverlayRef; 
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  constructor(private modalService: NgbModal, private service: AuthService,private fb: FormBuilder,private layoutUtilsService: LayoutUtilsService,private overlay: Overlay,private common:CommonSerService) {
  
    
  }
//Modal box functions
   openCentred(content) {
      // this.modalService.open(content, { centered: true, size:'lg'} );
     this.common.openCentred(content,'lg');
      this.cond=false;

  }
  //paginator
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

 
 
 ngOnInit() {
this.overlayRef = this.overlay.create({	
    positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),	
    hasBackdrop: true	
  });	
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
	  this.dataSource=new MatTableDataSource<Question>(ELEMENT_DATA);
this.get_category();

console.log( this.dataSource);

this.Question_form =this.fb.group({
  question_id:new FormControl('', [Validators.required ]),
  question_description: new FormControl('', [ Validators.required]),
  answer_category:new FormControl('', [Validators.required]),

});
this.overlayRef.detach();
      
}
// form Reset modal
reset()
{
	this.overlayRef.detach();
this.Question_form =this.fb.group({
  question_description:  new FormControl('', [Validators.required ]),
  question_id:  new FormControl('', [ Validators.required]),
  answer_category:new FormControl('', [Validators.required]),

});
this.cond=false;
}
// get MAx  value  category increment

//datatable datas
get_category()
{
  const ELEMENT_DATA:Question[]=[];
    this.overlayRef.attach(this.LoaderComponentPortal);
  var data = "";
  var method = "post";
  var url = "get_all_checklist_master";
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => {  
    this.allDataExcel= data.result;  
    for(let c of data.result){
         ELEMENT_DATA.push(c);
         this.dataSource=new MatTableDataSource(ELEMENT_DATA);
  this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
	 this.overlayRef.detach();
        }
      
  });    
}					
//Checkbox function																															

isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
  }
/** FILTRATION */
filterConfiguration(): any {
  const filter: any = {};
  const searchText: string = this.searchInput.nativeElement.value;

  filter.question_description = searchText;

  filter.question_id = searchText;
  filter.answer_category = searchText;
  //filter. = searchText
  return filter;
}

 applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  // duplicated Remover
duplicate()
{
  this.cond=false;

}
//Validation
get log() { return this.Question_form.controls;  }
get editlog() {  return this.Edit_Question_form.controls;}

//Edit modal Fecth value
editQuestion(id,content)
{
console.log(id);
//this.openCentred(content);
this.common.openCentred(content,'lg') 
this.  Edit_Question_form =this.fb.group({
   
    s_no:new FormControl(id.id, [ ]),
    question_id:new FormControl(id.question_id, [Validators.required ]),
  question_description: new FormControl(id.desc, [ Validators.required]),
  answer_category:new FormControl(id.answer_category, [Validators.required]),

});
 

  
}
//crud function

add_question_master()
{
	if (this.Question_form.invalid) {
		document.getElementById("question_id").focus();
      this.Question_form.get('question_description').markAsTouched();
      this.Question_form.get('answer_category').markAsTouched();
      this.Question_form.get('question_id').markAsTouched();
	  
    }
	else
	{ 
    
    for(let data1 of this.allDataExcel)
{  
  const valid_dat=[];
  this.cond=false;
  if(data1.question_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()==this.Question_form.value.question_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())
  {
   this.valid_dat.push(data1);
    
  }
//  return this.valid_dat;
}
    if(this.valid_dat.length==0)
   {
    console.log(this.Question_form.value.store_name);
this.overlayRef.attach(this.LoaderComponentPortal);
var data = this.Question_form.value;
 var method = "post";
 var url = "save_checklist_master";
 
 //this.modalService.dismissAll();
 this.common.Modal_close();
 this.service.servicepost(data, method, url, 'application/json')
  .subscribe(data => { 

this.overlayRef.detach();
Swal.fire(
 
      'Save!',
      'Your record has been saved.',
      'success'
    )
 
this.ngOnInit();
  
       
      
  });
}
else
{
 this.cond=true;
 this.valid_dat=[];
}
  }

}
 
edit_question_master()
{
  this.valid_dat=[];
  for(let data1 of this.allDataExcel)
  {  
    const valid_dat=[];
    this.cond=false;
    if(data1.question_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()==this.  Edit_Question_form.value.question_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())
    {
     this.valid_dat.push(data1);
      
    }
  //  return this.valid_dat;
  }
  console.log( this.valid_dat);
  
  if((this.valid_dat.length==0)||(this.valid_dat.length==1 && this.valid_dat[0]['question_id']== this.  Edit_Question_form.value .question_id && this.valid_dat[0]['id']== this.  Edit_Question_form.value .s_no ))
 {
  this.overlayRef.attach(this.LoaderComponentPortal);
 var data = this.  Edit_Question_form.value;
  var method = "post";
  var url = "edit_checklist_master";
  
 // this.modalService.dismissAll();
 this.common.Modal_close();
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1){
      this.overlayRef.detach();
Swal.fire(
      'Edit!',
      'Your record has been Updated.',
      'success'
    )

this.ngOnInit();}
  
}); 
 }
 else
 {
  this.cond=true;
  this.valid_dat=[];

 }
}

fetchUsers() {
  const messages = [];
  
  this.selection.selected.forEach(elem => {
    messages.push({
      question_id: elem.question_id,
      question_description:elem.question_description,
      s_no:elem.id
    });
console.log(elem);      
  });
   
Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this  Record!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {

  if (result.value) {
    this.overlayRef.attach(this.LoaderComponentPortal);
var data = messages;
  var method = "post";
  var url = "bulk_delete_checklist";
  
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1)
    {
   	
    
    
this.dataSource=new MatTableDataSource<Question>();
 
//this.isAllSelected();  
//this.masterToggle();
this.selection.selected.length=0;
this.overlayRef.detach();
this.ngOnInit();
this.selection.clear() ;
//this.overlayRef.detach();

 
}
  });
}
})
  // this.layoutUtilsService.fetchElements(messages);
//this.selection.selected.length=0;
}


deletestore(del)
{
	
Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this  Record!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {
  if (result.value) {
    this.overlayRef.attach(this.LoaderComponentPortal);
var data = del;
  var method = "post";
  var url = "delete_checklist";
  
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1){Swal.fire(
      'Deleted!',
      'Your record has been deleted.',
      'success'
    )

this.dataSource=new MatTableDataSource<Question>();
this.selection.clear() ;
this.overlayRef.detach();
this.ngOnInit();


}
  });
}
})
}

//Download  excel
export(): void {
//  const _title = 'Download';
  //const _description = 'Are you sure to download the user data?';
//  const _waitDesciption = 'Data is downloading...';
//  const _deleteMessage = `User has been deleted`;
  var downloadArray= [];
  var testArray= [];
  var headerArray= [];
  headerArray=['S.No','Question ID','Question Name','Answer Category'];
  testArray.push(headerArray);
  
  for(var k=0;k<this.allDataExcel.length;k++) {
    //downloadArray=[k+1,this.allDataExcel[k]['category'],this.allDataExcel[k]['store_name']];
    downloadArray=[k+1,this.allDataExcel[k]['question_id'],this.allDataExcel[k]['desc'],this.allDataExcel[k]['answer_category']];
    
    testArray.push(downloadArray);
  }


Swal.fire({
  title: "Are you sure?",
  text: " You want to Download this page!",
  showConfirmButton: true,
  showCancelButton: true     
  })
  .then((willDelete) => {

  if(willDelete.value){
        const message ="Downloaded successfully!"
        this.generateSheet(testArray);
this.layoutUtilsService.showActionNotification(message);  
 this.overlayRef.detach();
    //  swal.fire("Success");
  }else{
    const message ="Download Rejected!"
    this.layoutUtilsService.showActionNotification(message); 
  //   swal.fire("Fail");
  }

  console.log(willDelete)
});
}
generateSheet(sheetData){
  /* generate worksheet */
this.overlayRef.attach(this.LoaderComponentPortal);
  const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(sheetData);
  ws['!cols'] = fitToColumn(sheetData);

  function fitToColumn(sheetData) {
      // get maximum character of each column
      return sheetData[0].map((a, i) => ({ wch: Math.max(...sheetData.map(a2 => a2[i].toString().length)) }));
  }
  /* generate workbook and add the worksheet */
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  
  /* save to file */
  XLSX.writeFile(wb, this.fileName);
  
  
}

//uploading
onFileChange(evt: any) {
  console.log(evt.target.files.length);
  console.log(evt.target.files[0]['type']);
  const target: DataTransfer = <DataTransfer>(evt.target);
  var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
  if (target.files.length !== 1) {
  this.layoutUtilsService.showActionNotification(`Multiple files not allowed.`, MessageType.Update, 5000, true, true);
  }
  if (validExts.indexOf(target.files[0]['type']) < 0)
   {
  this.layoutUtilsService.showActionNotification(`Only file with extensions .xlsx, .xls, .csv is allowed.`, MessageType.Update, 5000, true, true);

  }
  else{

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
    /* read workbook */
    const bstr: string = e.target.result;
    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
  
    /* grab first sheet */
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
  
    /* save data */
    this.be = (XLSX.utils.sheet_to_json(ws, { header: 1 }));
    this.be.splice(0, 1);
    console.log(this.be);
this.overlayRef.attach(this.LoaderComponentPortal);
  var data = this.be;
  var method = "post";
  var url = "bulk_upload_checklist";
  
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.msg=="save" || data.result==[ ]  ){
      Swal.fire(
       'Save!',
       'Your record has been saved.',
       'success'
     )
     
 
  this.overlayRef.detach();
 this.ngOnInit();
 
 }
 else
 {
   if(data.result==2)
   {
    this.layoutUtilsService.showActionNotification("Upload Document  is Empty" ); 
    this.overlayRef.detach();
   this.ngOnInit();
   }
   else{
   const dat_valid=[];
   for(let f of data.result ){
     console.log(f);
   
  dat_valid.push(f);
  
   }
   Swal.fire(
    'Error!',
    'Already Exist Category Name:' +dat_valid,
    'error'
  )
 // this.layoutUtilsService.showActionNotification("Already Exist Category Name:" +dat_valid); 
   this.overlayRef.detach();
   this.ngOnInit();
 }
 }
   });
 
 };
 reader.readAsBinaryString(target.files[0]);
 }
 evt.target.value = ''
 }
//sample template
downloadFile() {

var downloadArray= [];
  var testArray= [];
  var headerArray= [];
  headerArray=['S.No','Question ID','Question Name','Answer Category'];
  testArray.push(headerArray);
  this.generateSheet(testArray);
       this.overlayRef.detach();
  }


}

//Interface
export interface Question {
 id: number;
  question_id: string;
  question_description:string;
  answer_category:string;
  created_at:string;
  update_at:string;
  }
  
const ELEMENT_DATA:Question[]=[];
