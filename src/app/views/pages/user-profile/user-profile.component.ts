// Angular
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    FormControl,
    Validators,
    NumberValueAccessor,NgForm
} from '@angular/forms';;
// Layout
import { LayoutConfigModel, LayoutConfigService } from '../../../core/_base/layout';
import { AuthService } from '../../../core/auth';
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import swal from 'sweetalert2';
import { ConfirmPasswordValidator } from '../auth/register/confirm-password.validator';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'kt-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
// Public properties
model: LayoutConfigModel;
private userId:any;
private roleId:any;
private s_no:any;
private personal_details: FormGroup;
private password: FormGroup;

private emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
private phonePattern: RegExp = /^[0-9-+]{10,12}$/;
private allData:any;
 dupEmail: boolean ;
 dupMobileNum: boolean;
 Userid: boolean;


@ViewChild('form', {static: true}) form: NgForm;
  constructor(private layoutConfigService: LayoutConfigService,private auth: AuthService,
  private fb: FormBuilder,private router: Router) {

   }
/**=
	 * On init
	 */
	ngOnInit(): void {
		this.model = this.layoutConfigService.getConfig();
	//	this.email=localStorage.getItem('user_email');
		this.roleId=localStorage.getItem('role_id');
		this.s_no=localStorage.getItem('s_no');
		
		this.initRegistrationForm();
		 this.getUserDetails();
		 this.dupEmail=false;
		 this.dupMobileNum=false;
		 this.Userid=false;

		
  }
  getUserDetails(){

		this.auth.getUserDetails(this.s_no,this.roleId).pipe(
		  tap(result => {
      console.log(result);
	  this.allData=result['data'];
	  this.appendData();
      

		  }),
	
		).subscribe();
    }
	resetDetails(name){
		console.log(name);
	
		 switch (name) {
            case 'personal_details': {
				this.appendData();
				break;
			}
			case 'password_details': {
			   this.password.reset();
				break;
			}
			}
		

	}
	  isControlHasError(controlName: string, validationType: string): boolean {
        const control = this.personal_details.controls[controlName];
        if (!control) {
            return false;
        }

        const result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
	}
	
	isControlHasErrorPassword(controlName: string, validationType: string): boolean {
        const control = this.password.controls[controlName];
        if (!control) {
            return false;
        }

        const result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    }
	appendData(){
		 this.personal_details.patchValue({
            'f_name': this.allData['first_name'],
			'l_name': this.allData['last_name'],
			'mobile': this.allData['mobile'],
			'alt_mobile': this.allData['alternate_mobile'],
			'email': this.allData['email_id'],
			
		 })
	}

	 

submitPersonalInfo(){
	const controls = this.personal_details.controls;
		/** check form */
		if (this.personal_details.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		 const formData: FormData = new FormData();
            formData.append('f_name', controls['f_name'].value);
			formData.append('l_name', controls['l_name'].value);
			formData.append('mobile', controls['mobile'].value);
			formData.append('alt_mobile', controls['alt_mobile'].value);
			formData.append('email', controls['email'].value);
			formData.append('role_id', this.roleId);
			formData.append('s_no', this.s_no);
			 this.auth.savePersonalDetails(formData).pipe(
                tap(result => {
					console.log(result);
                    if (result['code'] === 200) {
                       
                        this.getUserDetails();
                        var title=result['title'];
                        var msg=result['message'];
						this.dupEmail=false;
						this.dupMobileNum=false;
                        swal.fire(
                            title,
                            msg,
                            'success'
                        )
                    }
				else{
					
				this.duplicate_check(result['num']);	
						// this.dupEmail=true;
					}

                }),

            ).subscribe();
}
duplicate_check(dup)

{
	switch (dup) {
		case 1: {
			console.log('case 1');
			this.dupEmail=true;
			this.dupMobileNum=true;
			break;
		}
		case 2: {
			console.log('case 2');
			this.dupEmail=true;
			this.dupMobileNum=false;
			break;
		}
		case 3: {
			console.log('case 3');
			this.dupEmail=false;
			this.dupMobileNum=true;
			break;
		}
					 }
}
updatePassword(){
	const controls = this.password.controls;
		/** check form */
		if (this.password.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
	
		this.auth.updateUserPassword(this.s_no,this.roleId,controls['c_pwd'].value,controls['password'].value).pipe(
			tap(result => {
				if (result['code'] === 200) {
					var title=result['title'];
                    var msg=result['message'];
					swal.fire(
						title,
						msg,
						'success'
					)
				}
				else{
					swal.fire(
						title,
						msg,
						'error'
					)
					localStorage.clear();
	
	this.router.navigateByUrl('/auth/login');

				}
		console.log(result);
		
		
			}),
	  
		  ).subscribe();
}

	  initRegistrationForm() {

        this.personal_details = this.fb.group({
            f_name: ['', Validators.compose([
                Validators.required
            ])],
			 l_name: ['', Validators.compose([
                Validators.required
            ])],
			  mobile: ['', Validators.compose([
                Validators.required,
                Validators.pattern(this.phonePattern)

            ])],
			 alt_mobile: ['', Validators.compose([
                Validators.required,
                Validators.pattern(this.phonePattern)

            ])],
			email: ['', Validators.compose([
                Validators.required,
                Validators.email
            ])],
		
		});

		 this.password = this.fb.group({
			 
           c_pwd: ['', Validators.compose([
				Validators.required
			])
			],
			password: ['', Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(100)
			])
			],
			confirmPassword: ['', Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(100)
			])
			]
		},
		 {
			validator: ConfirmPasswordValidator.MatchPassword
		});
		
	  }
	

  

}
