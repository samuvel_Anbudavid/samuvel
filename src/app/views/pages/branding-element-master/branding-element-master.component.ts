import { Component, OnInit, Input, ViewChild, ElementRef} from '@angular/core';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../core/auth';
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
// Services
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import * as XLSX from 'xlsx';
import swal from 'sweetalert2';
import {SelectionModel} from '@angular/cdk/collections';
import {LoaderComponent} from '../loader/loader.component';
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import{CommonSerService} from'../../../core/commonser';
type AOA = any[][];

@Component({
  selector: 'kt-branding-element-master',
  templateUrl: './branding-element-master.component.html',
  styleUrls: ['./branding-element-master.component.scss']
})
export class BrandingElementMasterComponent implements OnInit {
	@ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
	@ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  displayedColumns = ['select','ind','idd','name','material','dimensions','measurement','uom','actions'];
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;

  private branding_elements:FormGroup;
  private dataSource;
  private selection;
  private allDataExcel= [];
  private allDataExcel1= [];
  private be: AOA = [[]];
  private checked:boolean = false;
  alertMessage = '';
  private type:string;
  private s_no=null;
  private showCheckbox:boolean = true;
  private myVariable;
  private fileName: string = 'Branding Elements.xlsx';
  private headerArray=['S.N0','ID','Name','Material','Dimension','Length','Height','Width','Unit Of Measurement'];
  private modalTitle;
  private dupId:boolean=false;
  private dimType:String;
 // private loader:boolean=true;
 // assigned: string = '';


  constructor(private modalService: NgbModal,	private fb: FormBuilder,
    private auth: AuthService,private layoutUtilsService: LayoutUtilsService,
    private overlay: Overlay,private common:CommonSerService) {
      this.dataSource = new MatTableDataSource();
      this.selection = new SelectionModel();
     }
  

  ngOnInit() {
    this.overlayRef = this.overlay.create({	
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),	
      hasBackdrop: true	
    });	
      this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
          this.overlayRef.attach(this.LoaderComponentPortal);
    this.initRegistrationForm();
    this.getBE();
    console.log(this.branding_elements.controls);

  }
  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;		
  }
  getBE(){
    const url='get_all_branding_elements';
    this.auth.serviceget(url).pipe(
      tap(result => {
        console.log(result);
        this.overlayRef.detach();
        //this.allDataExcel=result['data'];
        this.allDataExcel1  = Object.assign([], result['data']);
        
        this.dataSource = new MatTableDataSource(this.allDataExcel1);
        this.selection = new SelectionModel(true, this.allDataExcel1.filter(t=> t.IsAssigned));
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;		
      }),

    ).subscribe();
  }
  initRegistrationForm() {

    this.branding_elements = this.fb.group({
      be_id: ['', Validators.compose([
     Validators.required,
     Validators.minLength(5),
     Validators.maxLength(20)
   ])
  ],
  be_name: ['', Validators.compose([
    Validators.required,
    Validators.minLength(5),
    Validators.maxLength(20)
  ])
 ],
 be_material: ['', Validators.compose([
  Validators.required,
  Validators.minLength(5),
  Validators.maxLength(20)
])
],
be_dim: [null, [ Validators.required ]],
//be_uom: [{value:'',disabled: true}, [ Validators.required ] ]
be_uom: [null, [ Validators.required ] ],
be_l: ['', Validators.compose([
  Validators.required
])
],
be_h: ['', Validators.compose([
  Validators.required
])
],
be_w: ['', Validators.compose([
  Validators.required
])
],
 
});
 
}
isControlHasError(controlName: string, validationType: string): boolean {
  const control = this.branding_elements.controls[controlName];
  if (!control) {
    return false;
  }

  const result = control.hasError(validationType) && (control.dirty || control.touched);
 // this.setValidation=result
  return result;
}
dimensionChanged(e){
 
  this.dimType=e.value;
  
}

checkBoxClicked(text){
console.log(text);
console.log(this.branding_elements);
}



/**
	 * Toggle selection
	 */
	masterToggle() {
		if (this.selection.selected.length === this.allDataExcel1.length) {
			this.selection.clear();
		} else {
			this.allDataExcel1.forEach(row => this.selection.select(row));
		}
  }
  
  	/**
	 * Check all rows are selected
	 */
	isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.allDataExcel1.length;
		return numSelected === numRows;
	}

fetchUsers() {
  const messages = [];
  const sNos = [];
  this.selection.selected.forEach(element => {
    sNos.push(element['s_no']);
  });
  
  swal.fire({
    title: "Are you sure?",
    text: " You want to Delete the selected records!",
    showConfirmButton: true,
    showCancelButton: true     
    })
    .then((willDelete) => {
     
    if(willDelete.value){
      this.overlayRef.attach(this.LoaderComponentPortal);

      this.auth.bulkDeleteBrandingElements(sNos).pipe(
        tap(result => {
          this.overlayRef.detach();

          this.getBE();
          if(result['status']==="success"){
          const message ="Deleted successfully!"
          swal.fire(message);
        //  this.layoutUtilsService.showActionNotification(message);  
          }
          else{
            const message ="Deletion failed!"
            swal.fire(message);
          }
        }),
    
      ).subscribe();
         
      //  swal.fire("Success");
    }else{
      const message ="Deletion Rejected!"
      this.layoutUtilsService.showActionNotification(message); 
    }
  
  });
  
  }
  




assignType(type:string,s_no){
  this.dupId=false;
  this.dimType='';
  console.log(type);
  console.log(this.checked);
  console.log(this.showCheckbox);
  this.type=type;
  this.s_no=null;
 // this.s_no=s_no;
if(this.type==="new"){
  this.modalTitle='Add Branding Elements';
  this.checked=false;
  this.showCheckbox=true;
  this.branding_elements.reset();

}
else{
  this.s_no=s_no;
  this.branding_elements.reset();
  this.modalTitle='Edit Branding Element';
}
}

checkId(valid_data){
  console.log(this.s_no);
  for(let data1 of this.allDataExcel1)
  {  
    if(this.s_no==null){
    
   if(data1.idd.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()==this.branding_elements.value.be_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())
    {
     valid_data.push(data1);
      
    }
  }
  else{
    if((data1.s_no !== this.s_no)&&(data1.idd.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()==this.branding_elements.value.be_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()))
    {
     valid_data.push(data1);
      
    }
  }
  }
  return valid_data;
}
onSubmitBrandingelements(content7,contentSmallAlert){
  const controls = this.branding_elements.controls;
  var valid_data=[];
  if(this.checked===true){
    if((!controls['be_id'].value) || (!controls['be_name'].value) || (!controls['be_material'].value) || (!controls['be_dim'].value) || (!controls['be_uom'].value)){
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
        );
    }
    else{
      switch(controls['be_dim'].value.value) { 
        case '1-d': { 
          if(!controls['be_l'].value){
            controls['be_l'].markAsTouched();
          }
          else{
            var result=this.checkId(valid_data);
            if(result.length===0){
            //this.modalService.dismissAll();
            this.common.Modal_close();
            this.overlayRef.attach(this.LoaderComponentPortal);
            this.saveBrandingElements(controls,content7,contentSmallAlert);
            }
            else{
            this.dupId=true;
            }
          }
          break;
        }
        case '2-d': { 
          console.log(controls['be_l'].value);
          console.log(controls['be_h'].value);
          if((!controls['be_l'].value) && (controls['be_h'].value)){
            controls['be_l'].markAsTouched();
          }
          else if((controls['be_l'].value) && (!controls['be_h'].value)){
            controls['be_h'].markAsTouched();
          }
          else if((!controls['be_l'].value) && (!controls['be_h'].value)){
            controls['be_l'].markAsTouched();
            controls['be_h'].markAsTouched();
          }
        
          else{
            var result=this.checkId(valid_data);
            if(result.length===0){
          //  this.modalService.dismissAll();
            this.common.Modal_close();
            this.overlayRef.attach(this.LoaderComponentPortal);
            this.saveBrandingElements(controls,content7,contentSmallAlert);
            }
            else{
            this.dupId=true;
            }
          }
          break;
        }
        case '3-d': { 
          if((!controls['be_l'].value) && (controls['be_h'].value) && (controls['be_w'].value)){
            controls['be_l'].markAsTouched();
          }
          else if((controls['be_l'].value) && (!controls['be_h'].value) && (controls['be_w'].value)){
            controls['be_h'].markAsTouched();
          }
          else if((controls['be_l'].value) && (controls['be_h'].value) && (!controls['be_w'].value)){
            controls['be_w'].markAsTouched();
           
          }
          else if((!controls['be_l'].value) && (!controls['be_h'].value) && (controls['be_w'].value)){
            controls['be_l'].markAsTouched();
            controls['be_h'].markAsTouched();
           
          }
          else if((controls['be_l'].value) && (!controls['be_h'].value) && (!controls['be_w'].value)){
            controls['be_h'].markAsTouched();
            controls['be_w'].markAsTouched();
           
          }
          else if((!controls['be_l'].value) && (controls['be_h'].value) && (!controls['be_w'].value)){
            controls['be_l'].markAsTouched();
            controls['be_w'].markAsTouched();
           
          }
          else if((!controls['be_l'].value) && (!controls['be_h'].value) && (!controls['be_w'].value)){
            controls['be_l'].markAsTouched();
            controls['be_h'].markAsTouched();
            controls['be_w'].markAsTouched();
           
          }
          else{
            var result=this.checkId(valid_data);
            if(result.length===0){
            // this.modalService.dismissAll();
            this.common.Modal_close();
            this.overlayRef.attach(this.LoaderComponentPortal);
            this.saveBrandingElements(controls,content7,contentSmallAlert);
            }
            else{
            this.dupId=true;
            }
          }
        
          break;
        }
      }
    }

  }
  else if ((!controls['be_id'].value) || (!controls['be_name'].value) || (!controls['be_material'].value)) {
    controls['be_id'].markAsTouched();
    controls['be_name'].markAsTouched();
    controls['be_material'].markAsTouched();
    }
  else{
    console.log("else");
    var result=this.checkId(valid_data);
    if(result.length===0){
   // this.modalService.dismissAll();
   this.common.Modal_close();
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.saveBrandingElements(controls,content7,contentSmallAlert);
    }
    else{
      this.dupId=true;
    }
  }
}



editBE(_item: number,content){
  this.auth.editBrandingElements(_item).pipe(
    tap(result => {
      this.overlayRef.detach();
      this.showCheckbox=false;
     // this.openLarge(content);
     this.common.openCentred(content,'lg');
      this.branding_elements.controls['be_id'].setValue(result['data'][0]['idd']);
      this.branding_elements.controls['be_name'].setValue(result['data'][0]['name']);
      this.branding_elements.controls['be_material'].setValue(result['data'][0]['material']);
      //this.branding_elements.controls['be_dim'].setValue(result['data'][0]['dimensions']);
      if((result['data'][0]['uom']==="null")&&(result['data'][0]['dimensions']==="null")){
       this.checked=false;
      }
      else{
        const result1 = this.be_uom.find( ({ value }) => value === result['data'][0]['uom'] );
        const result2 = this.be_dim.find( ({ value }) => value === result['data'][0]['dimensions'] );
        console.log(this.be_uom[result1['index']]);
        this.branding_elements.controls['be_uom'].setValue(this.be_uom[result1['index']]);
        this.branding_elements.controls['be_dim'].setValue(this.be_dim[result2['index']]);
          if(result['data'][0]['dimensions']=='1-d'){
            this.dimType='1-d';
            this.branding_elements.controls['be_l'].setValue(result['data'][0]['length']);
          }
          else if(result['data'][0]['dimensions']=='2-d'){
            this.dimType='2-d';
            this.branding_elements.controls['be_l'].setValue(result['data'][0]['length']);
            this.branding_elements.controls['be_h'].setValue(result['data'][0]['height']);
          }
          else{
            this.dimType='3-d';
            this.branding_elements.controls['be_l'].setValue(result['data'][0]['length']);
            this.branding_elements.controls['be_h'].setValue(result['data'][0]['height']);
            this.branding_elements.controls['be_w'].setValue(result['data'][0]['width']);
          }
    this.checked=true;


      }
      

    }),
    
  ).subscribe();
  }
saveBrandingElements(controls,content7,contentSmallAlert){
  const formData: FormData = new FormData();

  formData.append('be_id', controls['be_id'].value);
  formData.append('be_name', controls['be_name'].value);
  formData.append('be_material', controls['be_material'].value);
//  formData.append('be_dim', controls['be_dim'].value);
  formData.append('type', this.type);
  formData.append('s_no', this.s_no);
  if((controls['be_uom'].value===null) || (controls['be_dim'].value===null)){
  formData.append('be_uom', null);
  formData.append('be_dim', null);
  formData.append('be_l', null);
  formData.append('be_h', null);
  formData.append('be_w', null);
  }else{
    
  formData.append('be_uom', controls['be_uom'].value.value);
  formData.append('be_dim', controls['be_dim'].value.value);
  if(controls['be_dim'].value.value==='1-d'){
   
    formData.append('be_l', controls['be_l'].value);
    formData.append('be_h', null);
    formData.append('be_w', null);
  }
  else if(controls['be_dim'].value.value==='2-d'){
    formData.append('be_l', controls['be_l'].value);
    formData.append('be_h', controls['be_h'].value);
    formData.append('be_w', null);

  }
  else{
    formData.append('be_l', controls['be_l'].value);
    formData.append('be_h', controls['be_h'].value);
    formData.append('be_w', controls['be_w'].value);
  }
  }

  this.auth.storeBrandingElements(formData).pipe(
    tap(result => {
      console.log(result);
      this.overlayRef.detach();

      if(result['code']===200){
        this.getBE();

        this.branding_elements.reset();
        this.checked=false;
     //   this.modalService.dismissAll();
     this.common.Modal_close();   
     console.log(result);
       
        if(result['message']==='Branding Element Added Successfully'){
          swal.fire(
 
            'Save!',
            'Your record has been saved.',
            'success'
          )
        }
        else{
          swal.fire(
 
            'Edit!',
            'Your record has been updated.',
            'success'
          )
        }
       
       
      }
      else{
        this.alertMessage=result['message'];
       // this.openSmall(contentSmallAlert);
       this.common.openCentred(contentSmallAlert,'md')
      }
    }),

  ).subscribe();
  
}

deleteBE(_item: number) {

  swal.fire({
    title: 'Are you sure?',
    text: 'You will not be able to recover this Record!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, keep it'
  }).then((result) => {

    if (result.value) {
      this.overlayRef.attach(this.LoaderComponentPortal);

    this.auth.deleteBrandingElements(_item).pipe(
      tap(result => {
        this.overlayRef.detach();

        if(result['status']=="success"){
          const message='Your record has been deleted.';
          this.getBE();
          swal.fire(message);
                }
        else{ 

          const message='Failed to delete.';
          swal.fire(message);
                }
      }),
      
    ).subscribe(err => {
      console.log(err);
    });
    }
  })  



  
}
onFileChange(evt: any,contentSmallAlert) {
console.log("sample check")
  this.overlayRef.attach(this.LoaderComponentPortal);

  const target: DataTransfer = <DataTransfer>(evt.target);
  var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
  console.log(target.files);
  if (target.files.length !== 1) {
    this.overlayRef.detach();

    swal.fire('Multiple files not allowed.');
    return false;
//	this.layoutUtilsService.showActionNotification(`Multiple files not allowed.`, MessageType.Update, 5000, true, true);
  }
  if (validExts.indexOf(target.files[0]['type']) < 0)
   {
    this.overlayRef.detach();

    swal.fire('Only file with extensions .xlsx, .xls, .csv is allowed.');
    return false;
//	this.layoutUtilsService.showActionNotification(`Only file with extensions .xlsx, .xls, .csv is allowed.`, MessageType.Update, 5000, true, true);

  }
  else{

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
    /* read workbook */
    const bstr: string = e.target.result;
    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
  
    /* grab first sheet */
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
  
    /* save data */
    this.be = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
   
    var result = this.arraysEqual(this.headerArray,this.be[0]);
  
    if(result===true){
      this.be.splice(0, 1);
      console.log(this.be);
     
this.auth.uploadBrandingElements(this.be).pipe(
  tap(result => {
    this.overlayRef.detach();
    this.getBE();
    if(result['status']=="success"){
    if(result['failed_products'].length===0){
     // this.layoutUtilsService.showActionNotification(`Branding Elements successfully saved.`, MessageType.Update, 5000, true, true);
     swal.fire(
 
      'Save!',
      'Your record has been saved.',
      'success'
    )
  }
  else{
    let ids='';
    result['failed_products'].forEach((element,index) => {
   //   ids += String((index+1))+".\n"+element+"\n";
   ids += String((index+1))+". "+element+"\n";
    });
    this.alertMessage='Following Product with Product IDs already exists.'+"\n"+ids;
   
   // this.openSmall(contentSmallAlert);
   this.common.openCentred(contentSmallAlert,'md')
  }
    }
    else{
      this.overlayRef.detach();

      swal.fire('Failed to upload. Please try again.');
   //   this.layoutUtilsService.showActionNotification(`Failed to upload. Please try again.`, MessageType.Update, 5000, true, true);
    }
  }),
  
  ).subscribe();
 this.overlayRef.detach();
this.ngOnInit();
}
else{
  this.overlayRef.detach();

      swal.fire('Incorrect Sheet Uploaded.Refer Sample Sheet!.');
}
};
reader.readAsBinaryString(target.files[0]);
}
evt.target.value = '';
}

arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;
    for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}


export(action): void {
  console.log(this.allDataExcel1);
  var myClonedArray1 = JSON.parse(JSON.stringify(this.allDataExcel1));
 // const myClonedArray1  = Object.assign([], this.allDataExcel1);
  var myClonedArray = myClonedArray1;
  console.log(myClonedArray);
    var downloadArray= [];
    var testArray= [];
    var txt;
    switch(action) { 
      case 'db': { 
        txt='You want to Download the data!';
        testArray.push(this.headerArray);



        for(var k=0;k<myClonedArray.length;k++) {
          var allKeys=Object.keys(myClonedArray[k]);
          //var allValues=Object.values(this.allDataExcel[k]);
          Object.values(myClonedArray[k]).forEach((element,index) => {
          if(element == 'null'){
            console.log(allKeys[index]);
            myClonedArray[k][allKeys[index]] = 'NA';
          }
        });

          downloadArray=[k+1,myClonedArray[k]['idd'],myClonedArray[k]['name'],
          myClonedArray[k]['material'],myClonedArray[k]['dimensions'],
          myClonedArray[k]['length'],myClonedArray[k]['height'],myClonedArray[k]['width'],
          myClonedArray[k]['uom']];
          testArray.push(downloadArray);
          console.log(this.allDataExcel1);
        }
        break;
      }
      case 'sample':{
        txt='You want to Download sample sheet!';
        testArray=[this.headerArray];
      break;
      }
      default: { 
   
        break;              
     } 
    }
    
    swal.fire({
      title: "Are you sure?",
      text: txt,
      showConfirmButton: true,
      showCancelButton: true     
      })
      .then((willDelete) => {
    
      if(willDelete.value){
            const message ="Downloaded successfully!"
            console.log(testArray);
            this.generateSheet(testArray);
          swal.fire(message);
      }else{
        const message ="Download Rejected!"
         swal.fire(message);
      }
    
      
    });
  }
  

  generateSheet(sheetData){
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(sheetData);
  
    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    
    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }
applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}

openLarge(content) {
  // this.modalService.open(content, {
  //   size: 'lg',
  // });
  this.common.openCentred(content,'lg');
}

// openSmall(content) {
//   this.modalService.open(content, {
//       size: 'md'
//   });
// }
be_uom: common[] = [
  {value: 'mm', viewValue: 'mm', index: 0},
  {value: 'cm', viewValue: 'cm', index: 1},
  {value: 'm', viewValue: 'm', index: 2},
  {value: 'inches', viewValue: 'inches', index: 3},
  {value: 'feet', viewValue: 'feet', index: 4}
 

];
be_dim: common[] = [
  {value: '1-d', viewValue: '1-d', index: 0},
  {value: '2-d', viewValue: '2-d', index: 1},
  {value: '3-d', viewValue: '3-d', index: 2}
  
 

];
}
export interface common {
  value: string;
  viewValue: string;
  index: number
}
