import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandingElementMasterComponent } from './branding-element-master.component';

describe('BrandingElementMasterComponent', () => {
  let component: BrandingElementMasterComponent;
  let fixture: ComponentFixture<BrandingElementMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandingElementMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandingElementMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
