import { Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef,QueryList, ViewChildren } from '@angular/core';
import { NgbModal,ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../core/auth';
import{CommonSerService} from'../../../core/commonser';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {SelectionModel} from '@angular/cdk/collections';
import swal from 'sweetalert2';
import {LoaderComponent} from '../loader/loader.component';
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import {animate, state, style, transition, trigger} from '@angular/animations';
//import { MatMenuTrigger } from '@angular/material';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CDK_DESCRIBEDBY_HOST_ATTRIBUTE } from '@angular/cdk/a11y';
@Component({
  selector: 'kt-audit-list',
  templateUrl: './audit-list.component.html',
  styleUrls: ['./audit-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AuditListComponent implements OnInit,AfterViewInit{
 
  

  @ViewChild('closebutton', {static: false}) closebutton
  @ViewChild(MatPaginator, {static: false}) paginator:MatPaginator;
  //@ViewChild(MatPaginator, {static: false}) paginator1:MatPaginator;
  //@ViewChild(MatMenuTrigger, {static: false}) trigger01: MatMenuTrigger;

  @ViewChild('sort1', {static: true}) sort: MatSort;
 // @ViewChild('TableOnePaginator', {static: false}) tableOnePaginator: MatPaginator;
 // @ViewChild('TableTwoPaginator', {static: false}) tableTwoPaginator: MatPaginator;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;
  private displayedColumns = ['dd','select','s_no','retailer','audit_type',
  'no_of_audits','action'];
  private displayedColumns1 = ['s_no','retailer','s_date',
  'e_date','ins','action','actions'];
  retailer_data: any;
 // private showFirst:boolean=true;
  private acceptanceTime=2;
  public form: FormGroup;
  private max_date: any;
  private min_date: any;
  private overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  private new_audit_standalone: FormGroup;
  private new_audit_periodic: FormGroup;
  private Edit_reschedule: FormGroup;
  private audit_overdue: FormGroup;
  requiredField: boolean = true;
  requiredField1: boolean = true;
  private requiredField_stand: boolean = true;
  //private requiredField_auditoverdue: boolean = true;

  private email: string;
  private userID: string;
  private dataSource;
  private dataSource1;
  private requiredField_periodic: boolean=true;
  private list: any[];
  private reschedule_check: boolean=false;
  private range: any;
  private range1: any;
  private range2: any;


  private selection;
  private dropdownList = [];
  private selectedItems = [];
  private selectedItems1 = [];
  private dropdownSettings :IDropdownSettings= {};
  private inspList = []; 
  private statusList = [];
  private typesList = [];
  private panelOpenState: boolean = false;
  private expandedElement;
  private allData=[];
  private allDataModal=[];
  private modalRef:any;
  
  public currentDate;
  current_r: number;
  current_rh: number;
  private timepartStandalone:any;
  private timpepartPeriodic:any
  private timpepartAuditOverdue:any
  private auditOverdueServerData: AuditOverdueAssign[]=[];

  private currentDateStartDate: any;
  private auditScheduledDateTime=[];
  private auditScheduledDateTimeInTsp=[];
  private auditScheduledEndDateTimeInTsp=[];
  private inspectorAcceptanceTimePeriodic=[];
  private inspectorAcceptanceTimeStandalone:any;
  private auditOverdueInspectorAcceptanceTime:any;
  public activeModal: NgbActiveModal;
  private selectedAuditList=[];
  private selectedRowStatusTable=[];
	//@Input() data: Timeline2Data[];
  @Input() data:any[];
  constructor(private modalService: NgbModal,	private fb: FormBuilder,private datePipe: DatePipe,
   
    private auth: AuthService,
    private timediff: CommonSerService,
    private overlay: Overlay,private cdr: ChangeDetectorRef) { 
      this.dataSource = new MatTableDataSource();
      this.selection = new SelectionModel();
      this.dataSource1 = new MatTableDataSource<any>();
    }

  
   ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
      //this.dataSource1.paginator = this.paginator1;
    this.data;
     this.cdr.detectChanges();
    }
  
  ngOnInit() { 
    this.email=localStorage.getItem('user_email');
    this.userID=localStorage.getItem('user_id');
    console.log(this.userID);
    this.initRegistrationForm();
    
    this.new_audit_standalone.get('startDate').enable();
    this.new_audit_periodic.get('startDate').enable();

    this.getAllAuditList(null);
    this.getRetailList();
  //  this.dataSource1.paginator = this.tableTwoPaginator;
    this.getAuditTypesList();
    this.getAuditStatusList();
    this.getInspectorList();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'retail_id',
      textField: 'display_txt',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };  
    this.setDate();

  } 
 

   getAllAuditList(data){
    this.auth.getAllAuditList(this.userID,this.email).pipe(
      tap(async result => {
   this.dataSource.data=result['result']['f_t'];
   this.allDataModal=result['result']['m_t'];
   this.expandedElement=result['result']['f_t'];
   
   this.dataSource.paginator = this.paginator;


   this.cdr.detectChanges();	

   this.selection = new SelectionModel(true, this.dataSource.data.filter(t=> t.IsAssigned));
   this.allData=result['result']['f_t'];
   if(data!=null){
    await this.model_data(data);
    }
  
      }),
   
   ).subscribe();
  
  }
  
  setDate(){
    var dt = new Date(Date.now());
   // dt.setHours( dt.getHours() + this.acceptanceTime );
   dt.setHours( dt.getHours() );
    this.currentDate=dt;
  
  }
  openBig(content) {
    this.modalService.open(content, {
      windowClass : "myCustomModalClass",
    });
    
  }

  openLarge(content) {
    
    // this.getInspectorList();
    // console.log(this.dropdownList.length)
    // if(this.dropdownList.length>0)
    // {
    this.modalService.open(content, 
      {
        size: 'lg',
        backdrop: 'static'
    });
  // }
  // else
  // {
  //   swal.fire(
  //     'Error',
  //     'Now Retailer Not Available',
  //     'error'
  // )
  // } 
}
 

  masterToggle() {
		if (this.selection.selected.length === this.dataSource.data.length) {
			this.selection.clear();
		} else {
			this.dataSource.data.forEach(row => this.selection.select(row));
		}
  }
  isAllSelected(): boolean {
	const numSelected = this.selection.selected.length;
	const numRows = this.dataSource.data.length; 
	return numSelected === numRows;
}
/*applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
// 07-10-2020 search correction
applyFilter1(filterValue: string) {
  this.dataSource1.filter = filterValue.trim().toLowerCase();

  if (this.pageSize) {
    this.pageSize;
  }
}*/


  initRegistrationForm(){
    this.new_audit_standalone = this.fb.group({
        retailer: ['',Validators.required],
        inspectors: [null, [Validators.required]],
        range: [{ value: '', disabled: true }, Validators.compose([
          Validators.required
      ])],
      time_period_per_audit: [{ value: '', disabled: true }, Validators.compose([
        Validators.required
    ])],
    inspector_accp_time_starts_at:[{ value: '', disabled: true }, Validators.compose([
      Validators.required
  ])],
        startDate: [{ value: '', disabled: true }, Validators.required],
        endDate: [{ value: '', disabled: true }, Validators.required],
        chosenDuration: [{ value: '', disabled: true }, Validators.compose([
          Validators.required
      ])]
      
  });
  this.new_audit_periodic = this.fb.group({
    retailer: ['',Validators.required],
    inspectors: [null, [Validators.required]],
    range: [{ value: '', disabled: true }, Validators.compose([
      Validators.required
  ])],
  time_period_per_audit: [{ value: '', disabled: true }, Validators.compose([
    Validators.required
])],
    startDate: [{ value: '', disabled: true }, Validators.required],
    endDate: [{ value: '', disabled: true }, Validators.required],
    no_of_cycles: ['', Validators.compose([
      Validators.required
  ])],
  chosenDuration: [{ value: '', disabled: false }, Validators.compose([
    Validators.required
])]

  
});
this.Edit_reschedule = this.fb.group({
  Date: ['',Validators.required],
  Inspector_name: ['', [Validators.required]],
  Update_at:['',[Validators.required]],
  inspectors: ['',[Validators.required]]

});
this.audit_overdue = this.fb.group({
 // accpt_time: ['',Validators.required],
  assigned_insp: ['', [Validators.required]],
  inspectors: [null,[Validators.required]],
  range: [{ value: '', disabled: false }, Validators.compose([
    Validators.required
])],
  chosenDuration: [{ value: '', disabled: false }, Validators.compose([
    Validators.required
])]

});
  }

  getInspectorList(){
    const url='get_all_inspectors_new_audit';
    this.auth.serviceget(url).pipe(
		// this.auth.getInspectorListNewAudit().pipe(
		  tap(result => {
      console.log(result);
      this.inspList=result['data'];
    }),
	
  ).subscribe();
  }

  getAuditStatusList(){
    const url='get_all_audit_status';
    this.auth.serviceget(url).pipe(
		
		  tap(result => {
      console.log(result);
      this.statusList=result['result'];
     
    }),
	
  ).subscribe();
  }
  getAuditTypesList(){
    const url='get_all_audit_types';
    this.auth.serviceget(url).pipe(
		
		  tap(result => {
      console.log(result);
      this.typesList=result['result'];
     
    }),
	
  ).subscribe();
  }
  // togglePanel() {
  //   console.log("clicked");
  //   this.panelOpenState = !this.panelOpenState
  // }
  getRetailList(){

		this.auth.getRetailListNewAudit(this.email,this.userID).pipe(
		  tap(result => {
      console.log(result);
      this.selectedItems = [];
      this.selectedItems1 = [];

   
      this.dropdownList=result['result'];
	 
		  }),
	
		).subscribe();
    }
    setStatus() {
      (this.selectedItems.length > 0) ? this.requiredField = true : this.requiredField = false;
    }
  
    onItemSelect(item: any) {
      //Do something if required
      //this.setClass();
    }
    onSelectAll(items: any) {
      //Do something if required
     // this.setClass();
    }
    onDeSelect(item: any) {
      //Do something if required
      //this.setClass();
    }
    onDeSelectAll(items: any) {
      //Do something if required
      //this.setClass();
    }
    setClass() {
      // this.setStatus();
      // if (this.selectedItems.length > 0) { return 'validField' }
      // else { return 'invalidField' }
      (this.selectedItems1.length > 0) ? this.requiredField1 = true : this.requiredField1 = false; 
    }
    /* onFilterChange(item: any) {
      console.log(item);
    }
   onDropDownClose(item: any) {
      console.log(item);
    }*/

onDateSelect(date,formgroupName:FormGroup)
{
  var control=formgroupName.controls;
  

//this.new_audit.get('endDate').setValue(date.value);
this.currentDateStartDate=Date.now();
control['endDate'].reset();
control['endDate'].enable();
control['time_period_per_audit'].reset();
control['range'].enable();
if(control['inspector_accp_time_starts_at']){
  control['inspector_accp_time_starts_at'].reset();
}
//control['inspector_accp_time_starts_at'].reset();
control['chosenDuration'].reset();


var dt = new Date(date.value);
    dt.setMinutes( dt.getMinutes() + 5 );
this.min_date=dt;	
//this.chosenPeriod(undefined,this.new_audit_periodic);
}
onDateSelect_end(date,formgroupName:FormGroup)
{
  var control=formgroupName.controls;

var startDate = control['startDate'].value;
var endDate = date.value;

//testing
 var timeDifferenceStartEnd=this.timediff.timeDiffCalc(endDate, startDate);
//formgroupName.get('range').setValue(this.timediff.timeDiffCalc(startDate, new Date(this.currentDateStartDate)));
//control['range'].setValue(this.timediff.timeDiffCalc(startDate, new Date(this.currentDateStartDate))[0]);
control['time_period_per_audit'].setValue(timeDifferenceStartEnd[0]);


}

assignPeriodicAudit(content){
  const control = this.new_audit_periodic.controls;
  this.auditScheduledDateTime=[];
  this.auditScheduledDateTimeInTsp=[];
  this.auditScheduledEndDateTimeInTsp=[];
  this.inspectorAcceptanceTimePeriodic=[];

  var strtTsp=Math.floor(control['startDate'].value / 1000); //milliseconds
  var date=new Date(strtTsp*1000);
  this.auditScheduledDateTime.push(date);

  this.inspectorAcceptanceDateValidtion(this.timpepartPeriodic,strtTsp,this.new_audit_periodic);
  this.auditScheduledDateTimeInTsp.push(strtTsp);
  
  var accp=this.timediff.durationDropdownSelection(this.timpepartPeriodic,strtTsp,this.range1);
  this.inspectorAcceptanceTimePeriodic.push(accp);
 // this.auditScheduledDateTimeInTsp.push(strtTsp+this.timediff.hoursToSecConversion(this.acceptanceTime));
  var timeDifferenceStartEnd=this.timediff.timeDiffCalc(control['endDate'].value, control['startDate'].value);
  var differenceInTimeStamp=this.timediff.timeStampConversion(timeDifferenceStartEnd[1],timeDifferenceStartEnd[2],timeDifferenceStartEnd[3]);
  var tspPerAuditCycle=differenceInTimeStamp/control['no_of_cycles'].value;
  for(var i=0;i<control['no_of_cycles'].value-1;i++){
    strtTsp=strtTsp+tspPerAuditCycle;
    var date=new Date(strtTsp*1000);
    this.inspectorAcceptanceDateValidtion(this.timpepartPeriodic,strtTsp,this.new_audit_periodic);
    this.auditScheduledDateTime.push(date);
    this.auditScheduledDateTimeInTsp.push(strtTsp);
    var accp=this.timediff.durationDropdownSelection(this.timpepartPeriodic,strtTsp,this.range1);
    this.inspectorAcceptanceTimePeriodic.push(accp);

   // this.auditScheduledDateTimeInTsp.push(strtTsp+this.timediff.hoursToSecConversion(this.acceptanceTime));
    this.auditScheduledEndDateTimeInTsp.push(strtTsp);


  }
  this.auditScheduledEndDateTimeInTsp.push(Math.floor(control['endDate'].value / 1000));
 

    if(this.formValidCheck(this.new_audit_periodic,2)!==false){
     // this.openLg(content);
      this.timediff.openCentred(content,'lg')

    }


}
closeModal(){
  const control = this.new_audit_periodic.controls;

  const formData: FormData = new FormData();
  formData.append('retailers', JSON.stringify(control['retailer'].value));
  formData.append('start_date', String(Math.floor(control['startDate'].value / 1000)));
  formData.append('end_date', String(Math.floor(control['endDate'].value / 1000)));
  formData.append('audit_cycles_start_date', JSON.stringify(this.auditScheduledDateTimeInTsp));
  formData.append('audit_cycles_end_date', JSON.stringify(this.auditScheduledEndDateTimeInTsp));
  formData.append('insp_accp_time_starts_at', JSON.stringify(this.inspectorAcceptanceTimePeriodic));

  formData.append('audit_type', String(2));
  formData.append('inspector', control['inspectors'].value);
  formData.append('manager', this.userID);
  formData.append('audit_status', String(1));
  // this.saveAudit(formData);
  
this.retailer_assign_check(formData);


}
saveAudit(formData){
  this.auth.saveNewAuditPeriodic(formData).pipe(
    tap(result => {
 console.log(result);
 var title=result['title'];
            var msg=result['message'];
       if (result['code'] === 200) {
           this.modalService.dismissAll();
           this.new_audit_periodic.reset();
           this.new_audit_standalone.reset();
           this.requiredField=true;
           this.requiredField1=true;
           this.selectedItems=[];
           this.selectedItems1=[];
            
 
            swal.fire(
                title,
                msg,
                'success'
            )
           
        }
        else{
         swal.fire(
           title,
           msg,
           'error'
       )
        }
        this.getAllAuditList(null);
 
 
    }),
 
 ).subscribe();
}
formValidCheck(form:FormGroup,type){
  console.log(form);
  const control = form.controls;
	if (form.invalid) {
  if(type===1)
  {
    this.requiredField_stand=false;
    this.requiredField=false;
  }
  else if(type===2)
  {
    this.requiredField1=false;
    this.requiredField_periodic=false;
    }
    else if(type===3){
    // this.requiredField_auditoverdue=false;
    }
    Object.keys(control).forEach(controlName =>
      control[controlName].markAsTouched()
    );
    return false;
  }
}
/*timeDuration(time){
  console.log(time);
  this.audit_overdue.controls['chosenDuration'].setValue(time);


}
selectedMenuDuration(time:number,dur:string){
  this.audit_overdue.controls['chosenDuration'].setValue(time+dur);

}*/
value_pass(range,form,form_type)
{
  var control=form.controls;
  

  if(form_type==1 )
  {
    control['inspector_accp_time_starts_at'].reset();
    this.range=range;
  if(this.timepartStandalone!=undefined || this.timepartStandalone!='' )
  {
  this.chosenPeriod(this.timepartStandalone,form,form_type);
  }
 }
else if(form_type==2)
{ 
  this.range1=range;
  if(this.timpepartPeriodic!= undefined || this.timpepartPeriodic!='')
  {
  this.chosenPeriod(this.timpepartPeriodic,form,form_type);

  }
}
else{

  control['chosenDuration'].reset();
  this.range2=range;
  if(this.timpepartAuditOverdue!= undefined || this.timpepartAuditOverdue!='')
  {
  this.chosenPeriod(this.timpepartAuditOverdue,form,form_type);

  }
}
 // this.chosenPeriod(this.timepart,form)
  

}
resetFormPartial(){

}
chosenPeriod(dur:string,form:FormGroup,type){
 var strtTsp;
 console.log(dur);
//if(dur != undefined && this.range !=null || this.range1 !=null || this.range2 !=null)
if((dur != undefined) && (this.range !=null || this.range1 !=null || this.range2 !=null))
{
   
 
const control = form.controls;
if(type===1 ){
  this.timepartStandalone=dur;
this.requiredField_stand=true;
strtTsp=Math.floor(control['startDate'].value / 1000);
this.inspectorAcceptanceDateValidtion(dur,strtTsp,form);

}
else if(type===2){
  this.timpepartPeriodic=dur;
this.requiredField_periodic=true;
strtTsp=Math.floor(control['startDate'].value / 1000);
this.inspectorAcceptanceDateValidtion(dur,strtTsp,form);

}
else{
  this.timpepartAuditOverdue=dur;
 // this.requiredField_auditoverdue=true;
  var strtTsp=this.selectedRowStatusTable['overall_audit_start_date'];
  var endTsp=this.selectedRowStatusTable['overall_audit_end_date'];
 
 
    this.audit_overdue.controls['chosenDuration'].setValue(this.range2 + ' ' + this.timpepartAuditOverdue);
  //  this.requiredField_auditoverdue=true;
    this.timpepartAuditOverdue=undefined;

    if((strtTsp+(this.timediff.durationDropdownSelectionActual(dur,strtTsp,control['range'].value)))>=endTsp){
    swal.fire(
      'Error',
      'Incorrect Inspector Acceptance Deadline.Please Change !',
      'error'
  ).then(function(){

   control['range'].reset();
   control['chosenDuration'].reset();

  
    
  })
  return false;
    }
    else{
    //  console.log(this.requiredField_auditoverdue);
      this.auditOverdueInspectorAcceptanceTime=strtTsp+(this.timediff.durationDropdownSelectionActual(dur,strtTsp,control['range'].value));
    //  this.requiredField_auditoverdue=false;
    }
  
  

}



}
}

inspectorAcceptanceDateValidtion(dur,strtTsp,form:FormGroup){
  var todayDate=new Date();
  const control = form.controls;
  if((this.timediff.durationDropdownSelection(dur,strtTsp,control['range'].value)*1000)-(todayDate.getTime()) <= 0){
    this.timepartStandalone=undefined;
    this.timpepartPeriodic=undefined;
    swal.fire(
      'Error',
      'Incorrect Inspector Acceptance Deadline.Please Change !',
      'error'
  ).then(function(){

   control['range'].reset();
   control['chosenDuration'].reset();
  
    
  })
  return false;
}
else{
if(control['inspector_accp_time_starts_at']){
  this.inspectorAcceptanceTimeStandalone=this.timediff.durationDropdownSelection(dur,strtTsp,control['range'].value)*1000;
 control['inspector_accp_time_starts_at'].setValue(this.datePipe.transform(new Date(this.inspectorAcceptanceTimeStandalone), 'MM/dd/yyyy hh:mm a'));
 this.new_audit_standalone.controls['chosenDuration'].setValue(this.range + ' ' + this.timepartStandalone);

}
else{
  this.new_audit_periodic.controls['chosenDuration'].setValue(this.range1 + ' ' + this.timpepartPeriodic);

}

}
}

assignStandaloneAudit(){

  const control = this.new_audit_standalone.controls;
  if(this.formValidCheck(this.new_audit_standalone,1)!==false){

  const formData: FormData = new FormData();
  formData.append('retailers', JSON.stringify(control['retailer'].value));
  formData.append('start_date', String(Math.floor(control['startDate'].value / 1000)));
  formData.append('end_date', String(Math.floor(control['endDate'].value / 1000)));
  formData.append('audit_cycles_start_date', String(Math.floor(control['startDate'].value / 1000)));
  formData.append('audit_cycles_end_date', String(Math.floor(control['endDate'].value / 1000)));
  formData.append('insp_accp_time_starts_at', String(Math.floor(this.inspectorAcceptanceTimeStandalone / 1000)));
  formData.append('audit_type', String(1));
  formData.append('inspector', control['inspectors'].value);
  formData.append('manager', this.userID);
  formData.append('audit_status', String(1));
  // this.saveAudit(formData);
this.retailer_assign_check(formData);

  }



}
typeChanged(e){
 
 console.log(e.target.selectedIndex);
 var filteredArray=[];
if(e.target.selectedIndex !== 0){
  filteredArray = this.allData.filter(function(itm){
    return  itm.audit_type === e.target.selectedIndex ;
  });
  
  
}
else{
  filteredArray=this.allData;
}
this.dataSource.data=filteredArray;
  this.cdr.detectChanges();
  
}
statusChanged(e){
 console.log(e.target.selectedIndex);
 var filteredArray=[];
 console.log(this.selectedAuditList);
if(e.target.selectedIndex !== 0){
 this.selectedAuditList.forEach(element => {
 if(element.audit_status === e.target.selectedIndex){
filteredArray.push(element);
  } ;
  });
   
  
}
else{
  filteredArray=this.selectedAuditList;
}
console.log(filteredArray);
this.dataSource1.data=filteredArray;
this.totalSize=this.dataSource1.data.length;
  this.iterator(filteredArray);
}
// retailer assign checking
retailer_assign_check(formdata)
{
  
    this.auth.retailcheck(formdata).pipe(
    tap(
    result=>{
      console.log(result);
     this.retailer_data=result['data'];
     if(result['data']>0)
     {
   swal.fire({
      title: 'Are you sure?',
      // text: 'Already assign Audit List in Retailer Same Date!',
      text:result['data_r']+ ',has been already scheduled',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Assign it!',
      cancelButtonText: 'No, Change it'
    }).then((result) => {
      if (result.value) {
        this.saveAudit(formdata);
      
      }
    })  
   
   }
   else{this.saveAudit(formdata);}
    }
  )).subscribe()
}
async model_data(m_data)
{
console.log(m_data);
console.log(this.allDataModal);
  
  this.selectedAuditList=[];
  this.allDataModal[m_data['retailers']].forEach(element => {
    if (element['audit_type'] == m_data['audit_type'])
    {
      this.selectedAuditList.push(element)
    }
  
  });
  console.log(this.selectedAuditList);
   this.dataSource1= new MatTableDataSource<any>();
   this.dataSource1.data=this.selectedAuditList;
   this.totalSize = this.dataSource1.data.length;
   this.cdr.detectChanges();	
   this.iterator(this.selectedAuditList);
  
}
handlePage(e: PageEvent) {
  console.log(e);
  this.currentPage = e.pageIndex;
  this.pageSize = e.pageSize;
  this.iterator(this.selectedAuditList);
}
iterator(datatableData:any) {

  const end = (this.currentPage + 1) * this.pageSize;
  const start = this.currentPage * this.pageSize;
  const part = datatableData.slice(start, end);
  this.dataSource1.data = part;
  
  this.cdr.detectChanges();	

  
}
reset_form()
{
  this.new_audit_periodic.reset();
  this.new_audit_standalone.reset();
  this.selectedItems1=[];
  this.selectedItems=[];
  this.range=null;
  this.range1=null;
  //this.timepart=undefined;
  this.timepartStandalone=undefined;
  this.timpepartPeriodic=undefined;
 // this.timpepartAuditOverdue=undefined;
  this.new_audit_standalone.controls['range'].disable();
  this.new_audit_periodic.controls['range'].disable();
  this.requiredField=true;
  this.requiredField1=true;
  this.requiredField_periodic=true;
  this.requiredField_stand=true;
 // this.requiredField_auditoverdue=true;

}
auditOverdueAssign(form:FormGroup,type){

  this.formValidCheck(this.audit_overdue,3);
  //console.log(this.requiredField_auditoverdue);
  const control= this.audit_overdue.controls;
  console.log(control['inspectors'].value);
  console.log(control['chosenDuration'].value);
  console.log(control['range'].value);

 // console.log(this.auditOverdueInspectorAcceptanceTime);
 // console.log(control['chosenDuration'].value);

 
 //console.log(this.auditOverdueServerData);



  if((control['range'].value != undefined) && (control['chosenDuration'].value != '') && (control['inspectors'].value != '')){
    const control= this.audit_overdue.controls;
    this.auditOverdueServerData[0]['insp']=control['inspectors'].value;
    this.auditOverdueServerData[0]['insp_accp_time']=this.auditOverdueInspectorAcceptanceTime;  
    
    var method = "post";
var url = "save_audit_overdue";
this.auth.servicepost(this.auditOverdueServerData[0],method,url,'application/json').subscribe(data => { 
 console.log(data.data);
  this.getAllAuditList(data.data);  
  swal.fire(
   
        'Updated!',
        'Your Changes has been Updated.',
        'success'
  ).then(()=>{
  // this.auditOverdueServerData={};
 
})


  this.modalRef.close();
  this.auditOverdueServerData=[];
 /*control['assigned_insp'].reset();
  control['range'].reset();
  control['chosenDuration'].reset();*/
 // this.requiredField_auditoverdue=true;
  this.timpepartAuditOverdue=undefined;
  this.range2=null;
 // control['inspectors'].reset();

 this.audit_overdue.reset();
  
});


  }
 

}
resetAuditOverdueForm(){
    
}
//Edit overdue
editAuditOverdue(data,content)
{
const control= this.audit_overdue.controls;
this.selectedRowStatusTable=data;
this.modalRef=this.timediff.openCentred(content,'lg');


this.list=this.inspList.filter(s=>{return s['user_id']!=data['inspectors']});
let name=this.inspList.filter(s=>{return s['user_id']==data['inspectors']});

control['assigned_insp'].setValue(name[0].f_name+"."+name[0].l_name+" "+"("+data['inspectors']+")");
this.auditOverdueServerData.push({insp_accp_time:null,"s_no":data.s_no,"insp":""});


}
// Reschedule-Edit
EditReschedule(data,content)
{
 
var Reschedule=JSON.parse(data.reschedule_date);
var i =JSON.parse(data.reschedule_date).length-1;
//this.opensm(content);
this.modalRef=this.timediff.openCentred(content,'md');
let name=this.inspList.filter(s=>{return s['user_id']==Reschedule[i].inspector});
this.list=this.inspList.filter(s=>{return s['user_id']!=Reschedule[i].inspector});
this.reschedule_check=false;
this.Edit_reschedule = this.fb.group({
  Date: [this.datePipe.transform(new Date(Reschedule[0].date*1000), 'MM/dd/yyyy hh:mm a'),[Validators.required]],
  Inspector_name: [name[0].f_name+"."+name[0].l_name+"("+Reschedule[i].inspector+")" , [Validators.required]],
  Update_at:[Reschedule[i].updated_at,[Validators.required]],
  inspectors: ['',[Validators.required]],
  s_no:[data.s_no]
});
}
AcceptAssign_Reschedule(status:string)
{

  this.reschedule_check=false;
  switch(status)
  {
    
    case "Accpet":
      {
       // this.reschedule_view=false;
        var data = this.Edit_reschedule.value;
        var method = "post";
        var url = "save_Accpet_schedule";
        this.auth.servicepost(data,method,url,'application/json').subscribe(data => { 

         
          swal.fire(
           
                'Updated!',
                'Your record has been Updated.',
                'success'
          )
          this.modalRef.close()
          this.getAllAuditList(data.data[0]);
       //   this.model_data(data.data[0]);
      
        });
         //this.ngOnInit();
        break;
      }
    case "Assign":
      {
     
       
               if(!this.Edit_reschedule.invalid)
       {
        var data = this.Edit_reschedule.value;
        var method = "post";
        var url = "save_reschedule";
        this.auth.servicepost(data,method,url,'application/json').subscribe(data => { 

         
          swal.fire(
           
                'Updated!',
                'Your record has been Updated.',
                'success'
          ) 
          this.modalRef.close();
          this.getAllAuditList(data.data[0]);
         // this.model_data(data.data[0]);
        
        });
        // this.ngOnInit();
          }
          else{
            this.reschedule_check=true;
            
          }
      
          break;
           
      }

  }

}
get ins(){
  return this.Edit_reschedule.get('inspectors');
}
// ngpopover
Timeline_view(data_tooltip)

{
//  console.log(JSON.parse(data_tooltip.overall_details));
 var timeData =[];
 var structureCreation=[];
 timeData =JSON.parse(data_tooltip.overall_details);
  structureCreation[1]={time:this.datePipe.transform(new Date(data_tooltip['overall_audit_start_date']*1000), 'MM/dd/yyyy'),
  icon:'fa fa-genderless kt-font-primary',text: 'Audit Start Date'}
//  structureCreation[timeData.length+1]={time:this.datePipe.transform(new Date(data_tooltip['overall_audit_end_date']*1000), 'MM/dd/yyyy'),
//  icon:'fa fa-genderless kt-font-primary'}
 for(let value of timeData)
 {
   for(let key in value)
   {
     console.log(key)
     switch(key)
     {
       case 'accepted_at':
       {
        structureCreation.push({time:this.datePipe.transform(new Date(value[key]*1000), 'MM/dd/yyyy'),
        icon:'fa fa-genderless kt-font-success', text: 'Audit Accepted Date'})
         break;
       }
       case 'rejected_at':
       {
        structureCreation.push({time:this.datePipe.transform(new Date(value[key]*1000), 'MM/dd/yyyy'),
        icon:'fa fa-genderless kt-font-warning', text: 'Audit Rejected Date'})
         break;
       }
       case 'reschedule_at':
       {
        structureCreation.push({time:this.datePipe.transform(new Date(value[key]*1000), 'MM/dd/yyyy'),
        icon:'fa fa-genderless kt-font-dark', text: 'Audit Re-Scheduled Date'})
         break;
       }
       case 'scheduled_at':
       {
        // structureCreation.push({time:this.datePipe.transform(new Date(value[key]*1000), 'MM/dd/yyyy'),
        // icon:'fa fa-genderless kt-font-primary', text: 'Audit Scheduled Date:'+this.datePipe.transform(new Date(value[key]*1000), 'MM/dd/yyyy')})
      structureCreation[0]={time:this.datePipe.transform(new Date(value[key]*1000), 'MM/dd/yyyy'),
      icon:'fa fa-genderless kt-font-primary', text: 'Audit Scheduled Date'}
        break;
       }
       
     }
   }
 }
 structureCreation.push({time:this.datePipe.transform(new Date(data_tooltip.overall_audit_end_date*1000), 'MM/dd/yyyy'), icon: 'fa fa-genderless kt-font-success',text: 'Audit End Date',})
 console.log(structureCreation);
this.data=structureCreation;
/*this.data =[
  {
    time:this.datePipe.transform(new Date(value.audit_start_date*1000), 'MM/dd/yyyy'),
    icon:'fa fa-genderless kt-font-primary',
    text: 'Audit Start Date'
  },
  {
    time:this.datePipe.transform(new Date(value.audit_end_date*1000), 'MM/dd/yyyy'),
    icon: 'fa fa-genderless kt-font-success',
    text: 'Audit End Date',
  }
]*/

    // this.data = [
    //   {
    //     time: this.datePipe.transform(new Date(data_tooltip.overall_audit_start_date*1000), 'MM/dd/yyyy'),
    //     icon: 'fa fa-genderless kt-font-primary',
    //     text: 'Audit Start Date',
    //   },
      
    //   // {
    //   //   time: '15/10/20',
    //   //   icon: 'fa fa-genderless kt-font-dark',
    //   //   text: 'Reschedule Requested',
    //   // },
    //   // {
    //   //   time: '15/10/20',
    //   //   icon: 'fa fa-genderless kt-font-primary',
    //   //   text: 'Audit Re-Scheduled at 15/10/2020',
    //   // },
    //   {
    //     time: this.datePipe.transform(new Date(data_tooltip.overall_audit_end_date*1000), 'MM/dd/yyyy'),
    //     icon: 'fa fa-genderless kt-font-success',
    //     text: 'Audit End Date',
    //   },
    
    // ];
 
}
}
export interface Timeline2Data {
	time: string;
	text: string;
	icon?: string;
	attachment?: string;
}

export interface AuditOverdueAssign {
  insp_accp_time:number;
  insp: string;
  s_no: number;

}

