import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreInspectorMasterComponent } from './store-Inspector-master.component';

describe('StoreInspectorMasterComponent', () => {
  let component: StoreInspectorMasterComponent;
  let fixture: ComponentFixture<StoreInspectorMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreInspectorMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreInspectorMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
