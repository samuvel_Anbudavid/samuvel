// import { Component, OnInit } from '@angular/core';
import { AfterViewInit, AfterViewChecked, ViewChild,Output,EventEmitter ,Input} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';
// Angular
import { Component, OnInit, ElementRef, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef,ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatSnackBar,MatTableDataSource ,MatButtonToggleModule} from '@angular/material';
import { AuthService } from '../../../core/auth';
import { FormGroup, FormControl, Validators,FormBuilder } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { HttpserviceService } from '../../../httpservice.service';
//import { Http, ResponseContentType, Headers, RequestOptions } from '@angular/http';
// import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import * as XLSX from 'xlsx';
import { from } from 'rxjs';
//import { map } from 'rxjs/operators';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Overlay,OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import {LoaderComponent} from '../loader/loader.component';
import { count } from 'rxjs/operators';
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import {StoreInspectorModalComponent}from'../store-inspector-modal/store-inspector-modal.component';
import csc from 'country-state-city';
import {CommonSerService}from'../../../core/commonser';
type AOA = any[][];

@Component({
  selector: 'kt-store-inspector-master',
  templateUrl: './store-inspector-master.component.html',
  styleUrls: ['./store-inspector-master.component.scss']
})

export class StoreInspectorMasterComponent implements OnInit,AfterViewInit {

  mangerDetails: any=[];
  displayedColumns = ['select','s_no', 'user_id', 'name','assign_manager','actions'];
  dataSource=new MatTableDataSource<User>(ELEMENT_DATA);
 // dataSource=ELEMENT_DATA;
//private dataSource;
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  // Filter fields
	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
	  @Input() childProperty: string;
	@ViewChild('wizard', {static: true}) public el: ElementRef;
	@ViewChild('StoreInspectorModalComponent',{static:true})private child: StoreInspectorModalComponent;
  //lastQuery: QueryParamsModel;
  //Variable Declare
  selection = new SelectionModel<User>(true, []);
  usersResult: User[] = [];
  private be: any;
  public category_store:any=[];
   private allDataExcel= [];
  overlayRef: OverlayRef; 
   private fileName: string = 'Store Inspector.xlsx';
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  @Output() setItemEvent  = new EventEmitter();
  view_data:any;
  datas:any;
  state:any;
  country:any;
  district:any;
  taluk:any;
  pincode:any;
  city:any;
  address_1:any;
  address_2:any;
  constructor(private modalService: NgbModal, private service: AuthService,private fb: FormBuilder,private layoutUtilsService: LayoutUtilsService,private overlay: Overlay,private common:CommonSerService) {
  
   
  }
//Modal box functions
   openCentred(content) {
     // this.modalService.open(content, { centered: true ,size:'md'} );
   this.common.openCentred(content,'md');

  } 
  openCentred1(content) {
     // this.modalService.open(content, { centered: true ,size:'lg'} );
   this.common.openCentred(content,'lg');

  }
  openModal() {
    const modalRef = this.modalService.open(StoreInspectorModalComponent,
     
       { centered: true ,size:'lg'}
      );
	  
	  // data come form chilld compand  save data'
	  modalRef.result.then(
  (data: any) => {
   this.get_merchand();
   console.log(data);
  },
  (reason: any) => {
	    console.log(reason);
		if(reason != undefined)
		{
	this.save_merchand(reason);		
		}
		else
		{
			this.get_merchand();
		}			
   });
  }
  //paginator
  ngAfterViewInit() {
		

	this.dataSource = new MatTableDataSource();
  this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;		
	}
  
 ngOnInit() {
	console.log(this.childProperty);
this.overlayRef = this.overlay.create({	
    positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),	
    hasBackdrop: true	
  });	
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
this.get_merchand();
this.get_category();
this.overlayRef.detach();
      
}


isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
  }
/** FILTRATION */
filterConfiguration(): any {
  const filter: any = {};
  const searchText: string = this.searchInput.nativeElement.value;

  filter.lastName = searchText;

  filter.category = searchText;
  filter.store_name = searchText;
  //filter. = searchText
  return filter;
}

 applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  


//get modal Fecth value

 get_merchand()
{
  const ELEMENT_DATA:User[]=[];
  this.overlayRef.attach(this.LoaderComponentPortal);
    this.category_store=[];
  var data = "";
  var method = "post";
  var url = "get_merchand";
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => {
	  // for(let c of data.result){
		//   if(c.role_id == 3)
		//   {
	 //ELEMENT_DATA.push(c);
  this.allDataExcel=data['result'].filter((item)=>item.role_id==3);
   console.log(data.result);
         this.dataSource=new MatTableDataSource(this.allDataExcel);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
		

      // }
      this.overlayRef.detach();
// } 
  });    
}
			
//	delete
deletemerchand(del)
{
	
Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this  Record!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {
  if (result.value) {
   // this.overlayRef.attach(this.LoaderComponentPortal);
var data = del;
  var method = "post";
  var url = "delete_meerchand";
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1){Swal.fire(
      'Deleted!',
      'Your record has been deleted.',
      'success'
    )
this.selection.clear() ;
this.dataSource=new MatTableDataSource<User>(ELEMENT_DATA);
this.ngOnInit();
this.allDataExcel= [];

}
  });
}
})
}
getJson(x){
  let z = x.replace(/'/g, '"');
  return JSON.parse(z);
 
}
//bulk delete
fetchUsers() {
  const messages = [];
  
  this.selection.selected.forEach(elem => {
    messages.push({
      user_id: elem.category,
      email:elem.email,
      id:elem.id
    });
console.log(elem);      
  });
   
Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this  Record!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {

  if (result.value) {
 this.overlayRef.attach(this.LoaderComponentPortal);
var data = messages;
  var method = "post";
  var url = "bulkdel_meerchand";
  
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => { if(data.result==1)
    {
   	
    
    
this.dataSource=new MatTableDataSource<User>(ELEMENT_DATA);
 this.selection.selected.length=0;
 this.overlayRef.detach();
this.ngOnInit();
this.selection.clear() ;
this.allDataExcel= [];
}
  });
}
})
  
}

editUser(id,content)
{
	this.overlayRef.attach(this.LoaderComponentPortal);
	console.log(id);
    const modalRef = this.modalService.open(StoreInspectorModalComponent,
     
       { centered: true ,size:'lg'}
      );
	   modalRef.componentInstance.edit_id=id;
	  this.overlayRef.detach();
	   modalRef.result.then(
  (result: any) => {
   this.get_merchand();
   console.log(result);
  },
  (reason: any) => {
	    console.log(reason);
		if(reason != undefined)
		{
	this.edit_merchand(reason);		
		}
		else
		{
			 this.get_merchand();
		}
			
				
   });
	  modalRef.componentInstance.edit_id=id;
	  this.overlayRef.detach();
	  this.ngOnInit();
  }
  edit_merchand(edit_data)
  {
  var data =edit_data;
  var method = "post";
  var url = "edit_store_inspector";
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => {
		
	//let User_clear = new FirstComponent();
  Swal.fire(
 
      'Save!',
      'Your record has been Updated.',
      'success'
    )
	this.ngOnInit();
 //	User_clear.get_merchand();
        });
  }
  //Download  excel
export(): void {
//  const _title = 'Download';
  //const _description = 'Are you sure to download the user data?';
//  const _waitDesciption = 'Data is downloading...';
//  const _deleteMessage = `User has been deleted`;
  var downloadArray= [];
  var testArray= [];
  var headerArray= [];
  headerArray=['S.No','User ID','First Name','Last Name','E-mail','Mobile No','Alter Mobile No' ,'Address Line 1','Address Line 2','Town/Village','Taluk','District','State','Country','Pincode','Assigned Manager'];
  testArray.push(headerArray);
  
  for(var k=0;k<this.allDataExcel.length;k++) {
    //downloadArray=[k+1,this.allDataExcel[k]['category'],this.allDataExcel[k]['store_name']];
	var geo_data=JSON.parse(this.allDataExcel[k]['p_geo_data']);
	//var geo1_data=JSON.parse(this.allDataExcel[k]['geo1_data']);
   this.state=csc.getStateById(geo_data.state);
   this.country=csc.getCountryById(geo_data.country);
   this.district=csc.getDistrictsById(geo_data.district);
 /*  var state1=csc.getStateById(geo1_data.state);
   var country1=csc.getCountryById(geo1_data.country);
   var district1=csc.getDistrictsById(geo1_data.district);
   var taluk1=csc.getTalukById(geo1_data.taluk);*/
   this.taluk=csc.getTalukById(geo_data.taluk);
    var category1=JSON.parse(this.allDataExcel[k]['category']);
	var store_name=[];
	category1.forEach(function (value) {
 store_name.push(value.name);
});
console.log(store_name);
    downloadArray=[k+1,this.allDataExcel[k]['user_id'],this.allDataExcel[k]['f_name'],this.allDataExcel[k]['l_name'],this.allDataExcel[k]['email'],this.allDataExcel[k]['mobile_no'],this.allDataExcel[k]['alt_mobile_no'],geo_data.address1,geo_data.address2,geo_data.city,this.taluk.name,this.district.name,
	this.state.name,this.country.name,geo_data.pincode,store_name.toString()];
    
    testArray.push(downloadArray);
	console.log (testArray);
  }


Swal.fire({
  title: "Are you sure?",
  text: " You want to Download this page!",
  showConfirmButton: true,
  showCancelButton: true     
  })
  .then((willDelete) => {

  if(willDelete.value){
        const message ="Downloaded successfully!"
        this.generateSheet(testArray);
this.layoutUtilsService.showActionNotification(message);  
 this.overlayRef.detach();
    //  swal.fire("Success");
  }else{
    const message ="Download Rejected!"
    this.layoutUtilsService.showActionNotification(message); 
  //   swal.fire("Fail");
  } 

  console.log(willDelete)
});
}
generateSheet(sheetData){
  /* generate worksheet */
//this.overlayRef.attach(this.LoaderComponentPortal);
  const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(sheetData);
  // ws['!cols'] = fitToColumn(sheetData);

  // function fitToColumn(sheetData) {
  //     // get maximum character of each column
  //     return sheetData[0].map((a, i) => ({ wch: Math.max(...sheetData.map(a2 => a2[i].toString().length)) }));
  // }
  /* generate workbook and add the worksheet */
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  
  /* save to file */
  XLSX.writeFile(wb, this.fileName);
  
  
}


viewUser(data,content)
{
	console.log(content);
	  var geo1_data=JSON.parse(data.geo1_data);
	this.datas=data;
	this.openCentred(content);
   this.view_data=JSON.parse(data.category);
  
   //console(this.state,this.country,this.district);
   this.ngOnInit();
  }
  //User id data view
  viewUser1(data,content)
{
	console.log(content);
	this.datas=data;
    var geo_data=JSON.parse(data.p_geo_data);
   this.openCentred1(content);
   this.state=csc.getStateById(geo_data.state);
   this.country=csc.getCountryById(geo_data.country);
   this.district=csc.getDistrictsById(geo_data.district);
   this.taluk=csc.getTalukById(geo_data.taluk);
   this.view_data=JSON.parse(data.category);
   this.pincode=geo_data.pincode;
   this.city=geo_data.city;
     this.address_1=geo_data.address1;
    this.address_2=geo_data.address2;
   this.ngOnInit();
  }
  onFileChange(evt: any) {
  console.log(evt.target.files.length);
  console.log(evt.target.files[0]['type']);
  const target: DataTransfer = <DataTransfer>(evt.target);
  var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
  if (target.files.length !== 1) {
  this.layoutUtilsService.showActionNotification(`Multiple files not allowed.`, MessageType.Update, 5000, true, true);
  }
  if (validExts.indexOf(target.files[0]['type']) < 0)
   {
  this.layoutUtilsService.showActionNotification(`Only file with extensions .xlsx, .xls, .csv is allowed.`, MessageType.Update, 5000, true, true);

  }
  else{

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
    /* read workbook */
    const bstr: string = e.target.result;
    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
  
    /* grab first sheet */
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
  
    /* save data */
    this.be = (XLSX.utils.sheet_to_json(ws, { header: 1 }));
    this.be.splice(0, 1);
    console.log(this.be);
this.overlayRef.attach(this.LoaderComponentPortal);
var store_cat=[];
var p_geo_data=[];
var b_geo_data=[];
 var b_data=[];
for(let c of this.be){
if(c[1]!=null && c[11] != null  && c[12] != null  && c[10] != null )
{
 var District=csc.getDistrictByName(c[11]);
 var state=csc.getStatesByName(c[12]);
 var Taluk=csc.getTalukByName(c[10]);
 
   
 var store=c[15];
 
store_cat.push(store);
b_data.push({"user_id":c[1],"f_name":c[2],"l_name":c[3],"E-mail":c[4],"mobile_no":c[5],"alt_mobile_no":c[6]});
p_geo_data.push({"country":"101","state":state[0].id,"district":District[0].id,"taluk":Taluk[0].id,"city":c[9],"pincode":c[14],"address1":c[7],"address2":c[8]});

}
}
var data = {b_data,p_geo_data, store_cat};
  var method = "post";
  var url = "upload_save_storeinspector";
  this.service.servicepost(data, method, url, 'application/json')
  .subscribe(data => { if(data.status=="sucess"){
    Swal.fire(
     'Save!',
     'Your record has been saved.',
     'success'
   )
   

this.overlayRef.detach();
this.ngOnInit();
}
else
{
 if(data.status=="duplicate")
 {
    const dat_valid=[];
 for(let f of data.message ){
   console.log(f);
 
dat_valid.push(f);

 }
 
 Swal.fire(
  'Error!',
  'Already Exist Unique Fields(User ID,Mobile No,Email) in User ID :' +dat_valid,
  'error'
)
// this.layoutUtilsService.showActionNotification("Already Exist Category Name:" +dat_valid); 
 this.overlayRef.detach();
 this.ngOnInit();
  
 }
 else{
      this.layoutUtilsService.showActionNotification("Upload Document  is Empty" ); 
  this.overlayRef.detach();
 this.ngOnInit();

}
}
 });

};
reader.readAsBinaryString(target.files[0]);
}
evt.target.value = ''
}
 //save data
save_merchand(save_data)
{
	if(save_data.user_id != null || save_data.user_id != undefined)
	{
	//console.log(geo_data);
  var data =save_data;
  var method = "post";
  var url = "save_store_inspector";
  this.overlayRef.attach(this.LoaderComponentPortal);
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => {
 
	//let User_clear = new FirstComponent();
  Swal.fire(
 
      'Save!',
      'Your record has been saved.',
      'success'
    )
	this.overlayRef.detach();
	this.ngOnInit();

 });
	}
}
// category checking
get_category()
  {
    const ELEMENT_DATA:User[]=[];
    
    var data = "";
  var method = "post";
  var url = "get_merchand";
  this.service.servicepost(data, method, url, 'application/json')
    .subscribe(data => {
  //  for(let c of data.result){
	// 	  if(c.role_id == 2)
	// 	  {
   //ELEMENT_DATA.push(c);
   this.mangerDetails=data['result'].filter((item)=>item.role_id==2);
      // }
      // }
    });
  }
}

//Interface
export interface User {
	id:number;
  user_id: string;
    name: string;
    p_address:string;
    c_address:string;
    email:string;
	concat_no:string;
category:any[ ];
  }
  
const ELEMENT_DATA:User[]=[];
