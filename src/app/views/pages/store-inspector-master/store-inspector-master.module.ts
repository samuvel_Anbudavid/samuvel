import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgbDropdownConfig,
     FormsModule,
    ReactiveFormsModule
  ],
 exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbDropdownConfig
  ] 	,
  bootstrap: []
})
export class StoreInspectorMasterModule { }
