import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
//import { HttpserviceService } from '../../../httpservice.service'
//import { Http, ResponseContentType, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { ICountry, IState, ICity } from 'country-state-city';
import csc from 'country-state-city';
import { ThemeModule } from '../../theme/theme.module';

@Component({
  selector: 'kt-territory-master',
  templateUrl: './territory-master.component.html',
  styleUrls: ['./territory-master.component.scss']
})
export class TerritoryMasterComponent implements OnInit {
  country:any;
   country1:any;
  state:any;
  city:any;
  city1:any;
  sta:any;
  taluk:any;
  district:any;
  district1:any;
  region:any;
  pincode:any;
  pincode1:any;
  disable:boolean;
  sdisable:boolean;
  constructor() { } 
  
  ngOnInit() {
   
    this.disable=true;
    this.sdisable=true;
    this.pincode="";
	this.pincode1="";
    // console.log(csc.getAllCountries());

  this.country=csc.getAllCountries();
    this.country1=csc.getAllCountries();
  // console.log(this.country);
 //this.sates=csc.getAllStates();
 //console.log(csc.getAllStates());
// console.log(this.sates);
   
  }
  getstate(country_id)
  {
     this.sta=csc.getStatesOfCountry(country_id);
    if(country_id==101)
    {
     this.disable=false;
     this.sdisable=false;
  
    }
    else{
      this.disable=true;
      this.sdisable=true;
      this.state=csc.getStatesOfCountry(country_id);
    }
  }
  getregstate(region)
  {
    console.log(region);
    
     this.state=csc.getStatesOfRegion(region);
     console.log(this.state);
  }


getcitystaulk(taluks)
{
		console.log(taluks);
	this.city1=csc.getCitiesOfTaulk(taluks);
	console.log(this.city1);
}
  // getregion(country_id)
  // {
    
  //  this.region=csc.getRegionById(country_id);
   
  //  console.log(this.region.region);
  // }
  
  // getstate(country_id)
  // {
  //   this.state=csc.getStatesOfCountry(country_id);
  //   console.log(this.state);
  // }
  gettaluk(district2)
  {
	// this.taluk=csc.getTalukOfState(district2);
  this.taluk=csc.getTalukOfDistrict (district2);
  console.log(this.taluk);
  }
  
  getcity(district_id)
  {
    this.city=csc.getCitiesOfDistrict(district_id);
    console.log(this.city);
    console.log(district_id);
  }
  getdistrict(state_id)
  {
	  
	  
	  console.log(this.district1);
     if(state_id<=41)
     {
		 this.district1=csc.getDistrictById(state_id);
    this.district=csc.getDistrictById(state_id);
    console.log(this.district);
    console.log(state_id);
     }
     else
     {
		 
		 this.district1=csc.getCitiesOfState(state_id);
      this.city=csc.getCitiesOfState(state_id);
      this.sdisable=true;
     }
  }
  getpincode(city_id)
  {
    this.pincode=csc.getCityById(city_id);
	 this.pincode1=csc.getCityById(city_id);
    console.log(this.pincode);
    console.log(city_id);
  }
  
  
}
