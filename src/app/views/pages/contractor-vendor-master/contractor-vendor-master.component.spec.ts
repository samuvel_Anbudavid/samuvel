import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorVendorMasterComponent } from './contractor-vendor-master.component';

describe('ContractorVendorMasterComponent', () => {
  let component: ContractorVendorMasterComponent;
  let fixture: ComponentFixture<ContractorVendorMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorVendorMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorVendorMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
