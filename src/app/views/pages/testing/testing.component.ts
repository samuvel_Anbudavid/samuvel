import {
    AfterViewInit,
    Component,
    ElementRef,
    OnInit,
    ViewChild,
    Input,
    ChangeDetectorRef
} from '@angular/core';
import {
    NgbActiveModal
} from '@ng-bootstrap/ng-bootstrap';
import {
    FormGroup,
    FormBuilder,
    FormControl,
    Validators,
    NumberValueAccessor
} from '@angular/forms';
import {
    AuthService,
    AuthNoticeService
} from '../../../core/auth';
import {
    debounceTime,
    distinctUntilChanged,
    tap,
    skip,
    take,
    delay
} from 'rxjs/operators';
import {
    MatPaginator
} from '@angular/material/paginator';
import {
    MatSort
} from '@angular/material/sort';
import {
    MatTableDataSource
} from '@angular/material/table';
import * as _ from 'lodash';
import {
    NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import {
    SelectionModel
} from '@angular/cdk/collections';
import * as XLSX from 'xlsx';
import swal from 'sweetalert2';
import {
    Overlay,
    OverlayRef
} from '@angular/cdk/overlay';
import {
    ComponentPortal
} from '@angular/cdk/portal';
import {
    LoaderComponent
} from '../loader/loader.component';
import csc from 'country-state-city';
import{CommonSerService} from'../../../core/commonser';
@Component({
    selector: 'kt-testing',
    templateUrl: './testing.component.html',
    styleUrls: ['./testing.component.scss']
})
export class TestingComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator, {
        static: false
    }) paginator: MatPaginator;
    @ViewChild('sort1', {
        static: true
    }) sort: MatSort;
    @ViewChild('wizard', {
        static: true
    }) el: ElementRef;
    private retails: FormGroup;
    private quantityGroup: FormGroup;
    private storeCatList;
    private grade: number;
    private emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    private phonePattern: RegExp = /^[0-9-+]{10,12}$/;
    displayedColumns = ['name', 'material', 'dimensions', 'measurement', 'uom', 'quantity', 'actions'];
    displayedColumns1 = ['s_no', 'name', 'material', 'dimensions', 'measurement', 'uom', 'quantity', 'actions'];
    displayedColumnsPromo = ['product_category', 'name', 'quantity', 'actions'];
    displayedColumnsPromo1 = ['s_no', 'product_category', 'name', 'quantity', 'actions'];
    displayedColumnsProduct = [];
    displayedColumnsProduct1 = [];
    displayedColumnsCheckList = ['select', 's_no', 'question', 'ans_category', 'action'];
    displayedColumnsCheckList1 = ['s_no', 'question', 'ans_category', 'action'];
    overlayRef: OverlayRef;
    LoaderComponentPortal: ComponentPortal < LoaderComponent > ;
    
    public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;


    private dataSource = [{}];
    private dataSource1;
    private dataSourcePromo = [{}];
    private dataSource2;
    private dataSourceProducts = [{}];
    private dataSource3;
    private dataSource4;
    private dataSource5;

    //	private dataSourceCheckList=[{}];




    private beNameList;
    private pList;
    private productList;
    private allDataExcel = [];
    private allDataExcelMaterial = [];
    private allDataExcelProducts = [];
    private allDataExcelDim = [];
    private allDataExcelMeasurement = [];
    private allDataExcelUOM = [];
    private assignedBE = [];
    private assignedPromo = [];
    private assignedProduct = [];
    private assignedProduct1 = [];

    private assignedCheckList = [];


    private firstPageControls = {};
    private secondPageControls = {};
    private thirdPageControls = {};
    private fourthPageControls = {};
    private quantityControl = {};
    private modalRef;
    private modalRef1;
    private indexQuantity: number;
    private indexQuantityPromo: number;
    private indexQuantityProduct: number;
    private submitted = false;
    private msg: string;
    private stepCurrent: string;
    private productReatilsControls = [];
    private selection;
    private dupRetailId: boolean = false;
    private dupRetailEmail: boolean = false;
    private dupMobileNum: boolean = false;
	private notFocusedRetailID = false;
	private notFocusedEmail = false;
    private notFocusedMobile = false;
    private sNo:number=0;
    private action:string;





    private country: any;
    private state: any;
    private district: any;
    private country_geo: any;
    private state_geo: any;
    private district_geo: any;
    private taluk_geo: any;
    private city: any;
    private taluk: any;


    @Input() edit_data;
    @Input() all_data;
    totalSize1: number;
    totalSize3: number;
    totalSize2: number;
    totalSize4: number;

    constructor(private modalService: NgbModal, private fb: FormBuilder, private authNoticeService: AuthNoticeService,
        private auth: AuthService, private cdr: ChangeDetectorRef,private commonSer:CommonSerService,private overlay: Overlay, public activeModal: NgbActiveModal) {
        this.dataSource1 = new MatTableDataSource();
        this.dataSource2 = new MatTableDataSource();
        this.dataSource3 = new MatTableDataSource();
        this.dataSource4 = new MatTableDataSource();
        this.dataSource5 = new MatTableDataSource();

        this.selection = new SelectionModel();
        


    }

    ngOnInit() {
        this.overlayRef = this.overlay.create({
            positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
            hasBackdrop: true
        });
        this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
        // this.common.pageSize=5 ;
        // this.common.currentPage=0 ;
        // this.common.totalSize=0 ;
        this.initRegistrationForm();
        this.retails.get('state').disable();
        this.retails.get('district').disable();
        this.retails.get('taluk').disable();
        this.retails.get('city').disable();
        this.retails.get('pincode').disable();

        this.retails.get('be_material').disable();
        this.retails.get('be_dimension').disable();
        this.retails.get('be_measurement').disable();
        this.retails.get('be_uom').disable();
        this.retails.get('be_quantity').disable();
        this.getRetailCategories();
        this.getBrandingElementsRetails();
        this.getPromotionalRetails();
        this.getProductRetails();
        this.getCheckListRetails();
        //this.disable=true;
        this.country = csc.getAllCountries();
        console.log(this.all_data);
        if (this.edit_data !== 'data') {
            this.editRetail();
        }

    }
    editRetail() {
        this.action='edit';
        var geo = JSON.parse(this.edit_data['geo']);
        this.sNo=this.edit_data['s_no'];
        this.retails.patchValue({
            'retail_id': this.edit_data['retail_id'],
            'retailer': this.edit_data['retailer'],
            'retail_category': this.edit_data['retail_category'],
            'landline': this.edit_data['landline'],
            'mobile_no': this.edit_data['mobile_no'],
            'email': this.edit_data['email'],
            'address_line1': this.edit_data['address_line1'],
            'address_line2': this.edit_data['address_line2'],
            'business_partner_email': this.edit_data['business_partner_email'],
            'business_partner_name': this.edit_data['business_partner_name'],
            'tl_name': this.edit_data['tl_name'],
            'tl_email': this.edit_data['tl_email'],
            'country': geo['country'],
            'state': geo['state'],
            'district': geo['district'],
            'taluk': geo['taluk'],
            'city': geo['city'],
            'pincode': geo['pincode']

        });
        this.district = csc.getDistrictById(geo['state']);
        this.taluk = csc.getTalukOfDistrict(geo['district']);
        this.state = csc.getStatesOfCountry(geo['country']);
        this.retails.get('state').enable();
        this.assignedBE = JSON.parse(this.edit_data['branding_elements']);
        
        this.assignedPromo = JSON.parse(this.edit_data['promo']);

        this.assignedProduct = JSON.parse(this.edit_data['products']);

        this.assignedCheckList = JSON.parse(this.edit_data['check_list']);
        this.totalSize=this.assignedBE.length;
        this.totalSize1=this.assignedPromo.length;
        this.totalSize2=this.assignedProduct.length;
        this.totalSize3=this.assignedCheckList.length;
       
    }

    getRetailCategories() {

         	 const url='get_all_store_categories';
    this.auth.serviceget(url).pipe(
            tap(result => {
                this.storeCatList = result['data'];
                //	console.log(result);

            }),

        ).subscribe();
    }
    getBrandingElementsRetails() {
        const url='get_all_branding_elements_retails';
        this.auth.serviceget(url).pipe(
            tap(result => {

                this.beNameList = result['data'];
                this.allDataExcel = result['data'];

            }),

        ).subscribe();
    }
    getPromotionalRetails() {
        const url='get_all_promotional_retails';
        this.auth.serviceget(url).pipe(
            tap(result => {
                this.pList = result['data'];
            }),

        ).subscribe();
    }

    getProductRetails() {
        const url='get_all_product_retails';
        this.auth.serviceget(url).pipe(
            tap(result => {
                console.log(result);
                this.productList = result['data'];
                var myClonedArray = JSON.parse(JSON.stringify(result['colms']));

                this.displayedColumnsProduct = myClonedArray;
               this.displayedColumnsProduct.push('Type','Quantity', 'Action');
              //this.displayedColumnsProduct.push('Quantity', 'Action');
                var myClonedArray1 = JSON.parse(JSON.stringify(this.displayedColumnsProduct));
                this.displayedColumnsProduct1 = myClonedArray1;
                this.displayedColumnsProduct1.splice(0, 0, "S_No");


                this.displayedColumnsProduct.forEach((element, index) => {
                    this.productReatilsControls.push(element);
                    this.fourthPageControls[element] = FormControl;
                    this.retails.addControl(element, new FormControl(null, Validators.required));
                    if (index !== 0) {
                        this.retails.get(element).disable();
                    }

                });

            }),

        ).subscribe();
    }
    getCheckListRetails() {
        const url='get_all_checklist_retails';
        this.auth.serviceget(url).pipe(
            tap(result => {
                //	console.log(result);
                this.dataSource4.data = result['data'];
                this.selection = new SelectionModel(true, result['data'].filter(t => t.IsAssigned));
                this.totalSize4=this.dataSource4.data.length;
                this.cdr.detectChanges();
            }),

        ).subscribe();
    }

    initRegistrationForm() {

        this.retails = this.fb.group({
            retail_id: ['', Validators.compose([
                Validators.required
            ])],
            retailer: ['', Validators.compose([
                Validators.required
            ])],
            retail_category: [null, [Validators.required]],
            landline: ['', Validators.compose([
                Validators.required,
                Validators.pattern(this.phonePattern)
            ])],
            mobile_no: ['', Validators.compose([
                Validators.required,
                Validators.pattern(this.phonePattern)

            ])],
            email: ['', Validators.compose([
                Validators.required,
                //Validators.pattern(this.emailPattern)
                Validators.email
            ])],
            address_line1: ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(20)
            ])],
            address_line2: ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(20)
            ])],
            business_partner_name: ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(20)
            ])],
            business_partner_email: ['', Validators.compose([
                Validators.required,
                Validators.email
            ])],
            tl_name: ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(20)
            ])],
            tl_email: ['', Validators.compose([
                Validators.required,
                Validators.email
            ])],
            be_name: [null, [Validators.required]],
            be_material: [null, [Validators.required]],
            be_dimension: [null, [Validators.required]],
            be_measurement: [null, [Validators.required]],
            be_uom: [null, [Validators.required]],
            be_quantity: ['', Validators.compose([
                Validators.required
            ])],
            p_category: [null, [Validators.required]],
            p_quantity: ['', Validators.compose([
                Validators.required
            ])],
            p_description: ['', Validators.compose([
                Validators.required
            ])],
            Quantity: ['', Validators.compose([
                Validators.required
            ])],
            Type: [null, Validators.compose([
                Validators.required
            ])],
            city: [null, Validators.compose([
                Validators.required,

            ])],
            state: [null, [Validators.required]],

            country: [null, [Validators.required]],
            pincode: [null, Validators.compose([
                Validators.required,

            ])],
            taluk: [null, Validators.compose([
                Validators.required
            ])],
            district: [null, Validators.compose([
                Validators.required
            ])]


        });

        this.quantityGroup = this.fb.group({
            be_extraquantity: ['', Validators.compose([
                Validators.required
            ])]
        });

        this.firstPageControls = {
            "retail_id": FormControl,
            "retailer": FormControl,
            "retail_category": FormControl,
            "landline": FormControl,
            "mobile_no": FormControl,
            "email": FormControl,
            "address_line1": FormControl,
            "address_line2": FormControl,
            "business_partner_name": FormControl,
            "business_partner_email": FormControl,
            "tl_name": FormControl,
            "tl_email": FormControl

        }
        this.secondPageControls = {
            "be_name": FormControl,
            "be_material": FormControl,
            "be_dimension": FormControl,
            "be_measurement": FormControl,
            "be_uom": FormControl,
            "be_quantity": FormControl
        }

        this.quantityControl = {
            "be_extraquantity": FormControl

        }
        this.thirdPageControls = {
            "p_category": FormControl,
            "p_quantity": FormControl

        }

    }

    isControlHasError(controlName: string, validationType: string): boolean {
        const control = this.retails.controls[controlName];
        if (!control) {
            return false;
        }

        const result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    }
    pCategory(index) {
        var index = this.retails.controls['p_category'].value;
        this.retails.controls['p_description'].setValue(this.pList[index]['name_description']);
    }
    productChanged(index: number, formcontrolname) {
        console.log(index);
        const controls = this.retails.controls;
        var appendItems;
        switch (index) {
            case index: {

                if (index === 0) {
                   var j = 2;
                 //var j=3;
                    for (var i = 0; i < this.displayedColumnsProduct.length - j; i++) {
                        this.retails.get(this.productReatilsControls[j + i]).disable();
                        this.retails.get(this.productReatilsControls[j + i]).reset();

                    }
                    this.allDataExcelProducts = [];
                    this.productList.forEach(element => {
                        if ((element[formcontrolname]) == (controls[formcontrolname].value)) {
                            //console.log("working");
                            this.allDataExcelProducts.push(element);

                        }
                    });
                    this.productList[this.productReatilsControls[index + 1]] = this.allDataExcelProducts;
                    this.retails.get(this.productReatilsControls[index + 1]).reset();
                    this.retails.get(this.productReatilsControls[index + 1]).enable();
                    //break;
                }
           /*     else if(index === this.displayedColumnsProduct.length-3){
                   
                    console.log(this.allDataExcelProducts);
                   
                } */
                else {

                    for (var i = 1; i < this.displayedColumnsProduct.length - index; i++) {

                        this.retails.get(this.productReatilsControls[index + i]).disable();
                        this.retails.get(this.productReatilsControls[index + i]).reset();


                    }
                    this.allDataExcelProducts = [];
                    this.productList.forEach(element => {
                       
                        if ((element[formcontrolname]) == (controls[formcontrolname].value)) {
                            for (var i = 0; i <= index; i++) {

                                if (this.productList[formcontrolname][0][this.productReatilsControls[index - 1]] == element[this.productReatilsControls[index - 1]]) {
                                    this.allDataExcelProducts.push(element);

                                }
                            }
                        }

                    });
                 //   console.log(this.allDataExcelProducts);
                    this.productList[this.productReatilsControls[index + 1]] = _.uniqWith(this.allDataExcelProducts, _.isEqual);
                 //   console.log(this.productList);
                    this.retails.get(this.productReatilsControls[index + 1]).reset();
                    this.retails.get(this.productReatilsControls[index + 1]).enable();

                }
                break;
            }
        }

    }
  /*  typeChanged(value){
     
        this.allDataExcelProducts.forEach((element,index) => {
            this.allDataExcelProducts[index]['Type']=value.viewValue;
            
        });
        this.retails.get(this.productReatilsControls[this.productReatilsControls.length-2]).enable();

   
    }*/

  /*  clickedAddProduct(content) {
        const controls = this.retails.controls;
        this.stepCurrent = 'product';
        var result: boolean = true;
        for (var i = 0; i < this.displayedColumnsProduct.length - 1; i++) {
            if ((this.retails.controls[this.displayedColumnsProduct[i]].value == null) ||
                (this.retails.controls[this.displayedColumnsProduct[i]].value == '')) {
                Object.keys(this.fourthPageControls).forEach(controlName =>
                    controls[controlName].markAsTouched()

                );
                result = false;
            }
        }
        console.log(this.productList);
        console.log(this.assignedProduct);
        if(result===true){
            var clone = this.deepCopy(this.productList['Type'][0]);  
            clone['Type']=controls['Type'].value;
         this.assignedProduct.push(clone);

        }
        console.log(this.assignedProduct);
        this.dataSource3.data = this.assignedProduct;
        this.cdr.detectChanges();
    }*/
    deepCopy(obj){
        var copy;

        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;
    
        // Handle Date
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }
    
        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = this.deepCopy(obj[i]);
            }
            return copy;
        }
    
        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = this.deepCopy(obj[attr]);
            }
            return copy;
        }
    
        throw new Error("Unable to copy obj! Its type isn't supported.");
    }



    clickedAddProduct(content) {
        const controls = this.retails.controls;
        this.stepCurrent = 'product';
        var result: boolean = true;
        var clone = this.deepCopy(this.productList['Type'][0]); 
        clone['Type']=controls['Type'].value;

        for (var i = 0; i < this.displayedColumnsProduct.length - 1; i++) {
            if ((this.retails.controls[this.displayedColumnsProduct[i]].value == null) ||
                (this.retails.controls[this.displayedColumnsProduct[i]].value == '')) {
                Object.keys(this.fourthPageControls).forEach(controlName =>
                    controls[controlName].markAsTouched()

                );
                result = false;
            }
        }
        if (result === true) {
            if (this.assignedProduct.length > 0) {
                var valexist = 0;
                var fullCheck = 0;
                this.assignedProduct.forEach((element, index) => {

                    if ((element['s_no'] === clone['s_no']) && (element['Type'] === clone['Type'])) {
                        this.msg = 'Product already assigned.Do you want to add quantity to existing?';
                        this.indexQuantityProduct = index;
                        this.modalRef = this.openMedium(content);
                        valexist = 1;
                    }
                });
                if (valexist === 0) {
                    	console.log("val else");
                    clone['Quantity'] = this.retails.controls['Quantity'].value;
                    this.assignedProduct.push(clone);
                }

            } else {
                clone['Quantity'] = this.retails.controls['Quantity'].value;
                this.assignedProduct.push(clone);
            }
            this.dataSource3.data =this.commonSer.iterator(this.assignedProduct,this.currentPage,this.pageSize );;
            this.totalSize2=this.dataSource3.data.length;
            this.cdr.detectChanges();
        }




    }
  /*  clickedAddProduct(content) {
        const controls = this.retails.controls;
        this.stepCurrent = 'product';
        var result: boolean = true;
       
      //  const productList1  = JSON.parse(JSON.stringify(this.productList));
      const productList1 = this.productList;

    //    productList1['Type'][0]['Type']=controls['Type'].value;

        console.log(productList1);

        for (var i = 0; i < this.displayedColumnsProduct.length - 1; i++) {
            if ((this.retails.controls[this.displayedColumnsProduct[i]].value == null) ||
                (this.retails.controls[this.displayedColumnsProduct[i]].value == '')) {
                Object.keys(this.fourthPageControls).forEach(controlName =>
                    controls[controlName].markAsTouched()

                );
                result = false;
            }
        }
        if (result === true) {
            if (this.assignedProduct.length > 0) {
                var valexist = 0;
                var fullCheck = 0;
                this.assignedProduct.forEach((element, index) => {
                        console.log(element['Type']);
                        console.log(controls['Type'].value);
                 
                    if ((element['s_no'] === productList1['Type'][0]['s_no'])) {
                        if((element['Type'] === controls['Type'].value)){
                            this.msg = 'Product already assigned.Do you want to add quantity to existing?';
                            this.indexQuantityProduct = index;
                            this.modalRef = this.openMedium(content);
                            valexist = 1;
                        }
                        else{
                            productList1['Type'][0]['Quantity'] = this.retails.controls['Quantity'].value;
                            productList1['Type'][0]['Type']=controls['Type'].value;
                            this.assignedProduct.push(productList1['Type'][0]);
                        }
                       
                    }
                });
                if (valexist === 0) {
                    //	console.log("val else");
                    productList1['Type'][0]['Quantity'] = this.retails.controls['Quantity'].value;
                    productList1['Type'][0]['Type']=controls['Type'].value;
                    this.assignedProduct.push(productList1['Type'][0]);
                }

            } else {
                console.log(productList1);
                productList1['Type'][0]['Quantity'] = this.retails.controls['Quantity'].value;
                productList1['Type'][0]['Type']=controls['Type'].value;

                this.assignedProduct.push(productList1['Type'][0]);
            }

        }

        this.dataSource3.data = this.assignedProduct;
        this.cdr.detectChanges();
console.log(this.assignedProduct);
return false;
    }*/
    clickedAddPromo(content) {
        this.stepCurrent = 'promo';
        this.quantityGroup.reset();
        this.indexQuantityPromo;


        const controls = this.retails.controls;
        var index = this.retails.controls['p_category'].value;

        if ((this.retails.controls['p_category'].value == null) ||
            (this.retails.controls['p_quantity'].value == "")) {
            Object.keys(this.thirdPageControls).forEach(controlName =>
                controls[controlName].markAsTouched()
            );
        } else {

            if (this.assignedPromo.length > 0) {
                var valexist = 0;
                this.assignedPromo.forEach((element, index) => {

                    if ((element['category'] == this.pList[index]['product_category']) && (element['name'] == this.retails.controls['p_description'].value)) {
                        this.msg = 'Promo material already assigned.Do you want to add quantity to existing promo material?';
                        this.indexQuantityPromo = index;
                        this.modalRef = this.openMedium(content);
                        valexist = 1;
                    }
                });
                if (valexist === 0) {
                    //	console.log("val else");
                    this.assignedPromo.push({
                        'category': this.pList[index]['product_category'],
                        'name': this.pList[index]['name_description'],
                        'id': this.pList[index]['product_id'],
                        'quantity': this.retails.controls['p_quantity'].value
                    });
                }
            } else {
                this.assignedPromo.push({
                    'category': this.pList[index]['product_category'],
                    'name': this.pList[index]['name_description'],
                    'id': this.pList[index]['product_id'],
                    'quantity': this.retails.controls['p_quantity'].value
                });
            }
        }
        this.dataSource2.data = this.commonSer.iterator(this.assignedPromo,this.currentPage,this.pageSize );;
        this.totalSize1= this.dataSource2.data.length;
        this.cdr.detectChanges();
    }

    clickedAdd(content) {
        this.stepCurrent = 'branding element';
        this.quantityGroup.reset();
        this.indexQuantity;

        const controls = this.retails.controls;
        if ((this.retails.controls['be_name'].value == null) || (this.retails.controls['be_material'].value == null) ||
            (this.retails.controls['be_dimension'].value == null) || (this.retails.controls['be_measurement'].value == null) ||
            (this.retails.controls['be_uom'].value == null) || (this.retails.controls['be_quantity'].value == null)) {
            Object.keys(this.secondPageControls).forEach(controlName =>
                controls[controlName].markAsTouched()
            );
        } else {
            if (this.assignedBE.length > 0) {
                var valexist = 0;
                this.assignedBE.forEach((element, index) => {
                    if ((element['name'] == this.retails.controls['be_name'].value) &&
                        (element['material'] == this.retails.controls['be_material'].value) &&
                        (element['dimension'] == this.retails.controls['be_dimension'].value) &&
                        (element['measurement'][0] == this.retails.controls['be_measurement'].value[0]) &&
                        (element['measurement'][1] == this.retails.controls['be_measurement'].value[1]) &&
                        (element['measurement'][2] == this.retails.controls['be_measurement'].value[2]) &&
                        (element['uom'] == this.retails.controls['be_uom'].value)) {
                        this.msg = 'Branding element with given dimensions already assigned.Do you want to add quantity to existing branding element?';
                        this.indexQuantity = index;
                        this.modalRef = this.openMedium(content);
                        valexist = 1;

                    }

                });
                if (valexist === 0) {
                    this.assignedBE.push({
                        'name': this.retails.controls['be_name'].value,
                        'material': this.retails.controls['be_material'].value,
                        'dimension': this.retails.controls['be_dimension'].value,
                        'measurement': this.retails.controls['be_measurement'].value,
                        'uom': this.retails.controls['be_uom'].value,
                        'quantity': this.retails.controls['be_quantity'].value
                    });
                }
            } else {
                this.assignedBE.push({
                    'name': this.retails.controls['be_name'].value,
                    'material': this.retails.controls['be_material'].value,
                    'dimension': this.retails.controls['be_dimension'].value,
                    'measurement': this.retails.controls['be_measurement'].value,
                    'uom': this.retails.controls['be_uom'].value,
                    'quantity': this.retails.controls['be_quantity'].value
                });
            }
        }
        console.log(this.assignedBE);
        this.dataSource1.data = this.commonSer.iterator(this.assignedBE,this.currentPage,this.pageSize );
       this.totalSize=this.dataSource1.data.length
        this.cdr.detectChanges();

    }


    openMedium(content) {
        return this.commonSer.openCentred(content,'md');
    }

    clickedDone() {
        //console.log(this.quantityGroup.controls['be_extraquantity'].value);
        //	console.log(this.retails.controls['Quantity'].value);
        const controls = this.quantityGroup.controls;
        if (this.quantityGroup.controls['be_extraquantity'].value == null) {
            //		console.log("checking");
            Object.keys(this.quantityControl).forEach(controlName =>
                controls[controlName].markAsTouched()
            );

            return;
        } else {
            this.modalRef.close();
            switch (this.stepCurrent) {
                case 'branding element': {
                    this.assignedBE[this.indexQuantity].quantity = this.assignedBE[this.indexQuantity].quantity + this.quantityGroup.controls['be_extraquantity'].value;
                    this.dataSource1.data = this.assignedBE;
                    break;
                }
                case 'promo': {
                    this.assignedPromo[this.indexQuantityPromo].quantity = this.assignedPromo[this.indexQuantityPromo].quantity + this.quantityGroup.controls['be_extraquantity'].value;
                    this.dataSource2.data = this.assignedPromo;
                    break;
                }
                case 'product': {
                    //	this.assignedProduct[this.indexQuantityProduct].Quantity=this.retails.controls['Quantity'].value+this.quantityGroup.controls['be_extraquantity'].value;
                    this.assignedProduct[this.indexQuantityProduct].Quantity = this.assignedProduct[this.indexQuantityProduct].Quantity + this.quantityGroup.controls['be_extraquantity'].value;

                    this.dataSource3.data = this.assignedProduct;

                    break;
                }
            }
            //		console.log(this.assignedProduct);

            this.cdr.detectChanges();
        }
    }

    addQuantity(content) {
        this.modalRef.close();
        this.modalRef = this.openMedium(content);
    }

    deleteBE(index) {
        this.assignedBE.splice(index, 1);
        this.dataSource1.data =this.commonSer.iterator(this.assignedBE,this.currentPage,this.pageSize); 
        this.totalSize= this.dataSource1.data.length
        this.cdr.detectChanges();

    }
    deletePromo(index) {
        this.assignedPromo.splice(index, 1);
        this.dataSource2.data = this.commonSer.iterator(this.assignedPromo,this.currentPage,this.pageSize); 
        this.totalSize1= this.dataSource2.data.length
        this.cdr.detectChanges();
    }
    deletePro(index) {
        this.assignedProduct.splice(index, 1);
        this.dataSource3.data = this.commonSer.iterator(this.assignedProduct,this.currentPage,this.pageSize); 
        this.totalSize2= this.dataSource3.data.length
        this.cdr.detectChanges();
    }
    deleteQuestion(index) {
        this.assignedCheckList.splice(index, 1);
        this.dataSource5.data = this.commonSer.iterator(this.assignedCheckList,this.currentPage,this.pageSize);
        this.totalSize3= this.dataSource5.data.length
        this.cdr.detectChanges();
    }


    nameChanged(current, next, db: any) {
        const controls = this.retails.controls;
        switch (current) {
            case 'be_name': {
                this.allDataExcelMaterial = [];
                this.allDataExcel.forEach(element => {
                    if ((element['name']) == (controls['be_name'].value)) {
                        this.allDataExcelMaterial.push(element);
                    }
                });
                this.retails.get('be_material').reset();
                this.retails.get('be_dimension').reset();
                this.retails.get('be_measurement').reset();
                this.retails.get('be_uom').reset();
                this.retails.get('be_quantity').reset();

                this.retails.get('be_dimension').disable();
                this.retails.get('be_measurement').disable();
                this.retails.get('be_uom').disable();
                this.retails.get('be_quantity').disable();

                this.retails.get('be_material').enable();
                break;
            }

            case 'be_material': {
                this.allDataExcelDim = [];
                this.allDataExcel.forEach(element => {
                    if ((element['name']) == (controls['be_name'].value) && (element['material']) == (controls['be_material'].value)) {
                        this.allDataExcelDim.push(element);
                    }
                });

                this.retails.get('be_measurement').reset();
                this.retails.get('be_uom').reset();
                this.retails.get('be_quantity').reset();


                this.retails.get('be_measurement').disable();
                this.retails.get('be_uom').disable();
                this.retails.get('be_quantity').disable();


                this.retails.get('be_dimension').reset();
                this.retails.get('be_dimension').enable();
                break;

            }

            case 'be_dimension': {
                this.allDataExcelMeasurement = [];
                this.allDataExcel.forEach(element => {
                    if ((element['name']) == (controls['be_name'].value) && (element['material']) == (controls['be_material'].value) && (element['dimensions']) == (String(controls['be_dimension'].value))) {
                        this.allDataExcelMeasurement.push(element);
                    }
                });
                this.retails.get('be_uom').reset();
                this.retails.get('be_quantity').reset();

                this.retails.get('be_uom').disable();
                this.retails.get('be_quantity').disable();


                this.retails.get('be_measurement').reset();
                this.retails.get('be_measurement').enable();
                break;

            }

            case 'be_measurement': {
                this.allDataExcelUOM = [];
                this.allDataExcel.forEach(element => {
                    if ((element['name']) == (controls['be_name'].value) && (element['material']) == (controls['be_material'].value) &&
                        (element['dimensions']) == (controls['be_dimension'].value) &&
                        (element['length']) == (String(controls['be_measurement'].value[0])) &&
                        (element['height']) == (String(controls['be_measurement'].value[1])) &&
                        (element['width']) == (String(controls['be_measurement'].value[2]))) {
                        this.allDataExcelUOM.push(element);
                    }
                });
                this.retails.get('be_quantity').reset();
                this.retails.get('be_quantity').disable();

                this.retails.get('be_uom').reset();
                this.retails.get('be_uom').enable();
                break;

            }
            case 'be_uom': {
                this.retails.get('be_quantity').reset();
                this.retails.get('be_quantity').enable();
            }

        }
    }

    checkId() {
        var valid_data = [];
console.log(this.sNo);
if(this.sNo===0){
        for (let record of this.all_data) {
           
            if (record.retail_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase() == this.retails.controls['retail_id'].value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()) {
                valid_data.push('id');
            }
            if (record.email.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase() == this.retails.controls['email'].value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()) {
                valid_data.push('email');
            }
            if (record.mobile_no.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase() == this.retails.controls['mobile_no'].value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase()) {
                valid_data.push('mobile');
            }



        }
    }
    else{
        for (let record of this.all_data) {
           
            if ((record.s_no !== this.sNo) && (record.retail_id.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase() == this.retails.controls['retail_id'].value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())) {
                valid_data.push('id');
            }
            if ((record.s_no !== this.sNo) && (record.email.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase() == this.retails.controls['email'].value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())) {
                valid_data.push('email');
            }
            if ((record.s_no !== this.sNo) && (record.mobile_no.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase() == this.retails.controls['mobile_no'].value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s]/g, "").toLowerCase())) {
                valid_data.push('mobile');
            }



        }
    }
        return valid_data;
    }



    ngAfterViewInit(): void {

        	/*const wizard = new KTWizard(this.el.nativeElement, {
        		startStep: 1
            });
            

        
        	wizard.on('beforeNext', (wizardObj) => {
        		
        	});

        	// Change event
        	wizard.on('change', () => {
        		setTimeout(() => {
        			KTUtil.scrollTop();
        		}, 500);
        	});*/

        this.dataSource1 = new MatTableDataSource();
        this.dataSource2 = new MatTableDataSource();
        this.dataSource3 = new MatTableDataSource();

        //   this.dataSource.paginator = this.paginator;
        //  this.dataSource.sort = this.sort;		
    }
    close(){
        const wizard = new KTWizard(this.el.nativeElement, {
            startStep: 1
        });
     //   wizard.close();
      //  wizard.dismiss();
      wizard.stop();
        
    }

    onSubmit_next() { 
       
        this.dupRetailId = false;
        this.dupRetailEmail = false;
        this.dupMobileNum = false;
        this.submitted = true;
        const wizard = new KTWizard(this.el.nativeElement, {
            startStep: 1
        });
        const controls = this.retails.controls;


        this.grade = wizard.currentStep;
        switch (this.grade) {
            case 1: {

                if (controls['retail_id'].value == null || controls['retail_id'].value == "" ||
                    controls['retailer'].value == null || controls['retailer'].value == "" ||
                    controls['retail_category'].value == null || controls['retail_category'].value == "" ||
                    controls['landline'].value == null || controls['landline'].value == "" ||
                    controls['mobile_no'].value == null || controls['mobile_no'].value == "" ||
                    controls['email'].value == null || controls['email'].value == "" ||
                    controls['address_line1'].value == null || controls['address_line1'].value == "" ||
                    controls['address_line2'].value == null || controls['address_line2'].value == "" ||
                    controls['business_partner_name'].value == null || controls['business_partner_name'].value == "" ||
                    controls['business_partner_email'].value == null || controls['business_partner_email'].value == "" ||
                    controls['tl_name'].value == null || controls['tl_name'].value == "" ||
                    controls['tl_email'].value == null || controls['tl_email'].value == "" || controls['country'].value == null || controls['country'].value == "" ||
                    controls['state'].value == null || controls['state'].value == "" || controls['district'].value == null || controls['district'].value == "" ||
                    controls['taluk'].value == null || controls['taluk'].value == "" || controls['city'].value == null || controls['city'].value == "" ||
                    controls['pincode'].value == null || controls['pincode'].value == "") {

                    Object.keys(this.firstPageControls).forEach(controlName =>
                        controls[controlName].markAsTouched()
                    );
                    wizard.on('beforeNext', function(wizardObj) {
                        wizardObj.stop();

                    });
                    return;

                } else {

                    var duplicate = this.checkId();
                    console.log(duplicate);
                    console.log(this.sNo);
                    if (duplicate.length > 0) {
                        duplicate.forEach((element, index) => {
                            if (element === 'id') {
                                this.dupRetailId = true;

                            } else if (element === 'email') {
                                this.dupRetailEmail = true;

                            } else {
                                this.dupMobileNum = true;

                            }
                        });
                        wizard.on('beforeNext', function(wizardObj) {
                            wizardObj.stop();

                        });
					}
					else{

                    this.dataSource1.data = this.assignedBE;

                    this.cdr.detectChanges();
                    wizard.goTo(2);
                   
					}

                }
                break;
            }
            case 2: {

                if (this.assignedBE.length == 0) {
                    Object.keys(this.secondPageControls).forEach(controlName =>
                        controls[controlName].markAsTouched()
                    );
                    wizard.on('beforeNext', function(wizardObj) {
                        wizardObj.stop();

                    });
                    return;
                } else {
                    this.dataSource2.data = this.assignedPromo;
                    this.cdr.detectChanges();
                    wizard.goTo(3);
                }
                break;
            }
            case 3: {
                //	console.log(this.assignedPromo);
                if (this.assignedPromo.length == 0) {
                    Object.keys(this.thirdPageControls).forEach(controlName =>
                        controls[controlName].markAsTouched()
                    );
                    wizard.on('beforeNext', function(wizardObj) {
                        wizardObj.stop();

                    });
                    return;
                } else {
                    this.dataSource3.data = this.assignedProduct;
                    this.cdr.detectChanges();
                    wizard.goTo(4);
                }
                break;
            }
            case 4: {
                //	console.log(this.assignedProduct);
                if (this.assignedProduct.length == 0) {
                    Object.keys(this.fourthPageControls).forEach(controlName =>
                        controls[controlName].markAsTouched()
                    );
                    wizard.on('beforeNext', function(wizardObj) {
                        wizardObj.stop();

                    });
                    return;
                } else {
                    this.dataSource5.data = this.assignedCheckList;
                    this.cdr.detectChanges();
                    wizard.goTo(5);
                }
                break;
            }
            case 5: {
                //	console.log(this.assignedCheckList);
                if (this.assignedCheckList.length == 0) {
                    //	this.authNoticeService.setNotice('testing', 'danger');

                } else {
                    //wizard.goTo(5);
                }
                break;
            }
            default: {

                break;

            }
        }
    }
    masterToggle() {
        if (this.selection.selected.length === this.dataSource4.data.length) {
            this.selection.clear();
        } else {
            this.dataSource4.data.forEach(row => this.selection.select(row));
        }
    }
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource4.data.length;
        return numSelected === numRows;
    }
    addQuestions(selected) {

        if (this.assignedCheckList.length > 0) {
            for (var i = selected.length - 1; i >= 0; i--) {
                for (var j = 0; j < this.assignedCheckList.length; j++) {
                    if (selected[i] && (selected[i].id === this.assignedCheckList[j].id)) {
                        selected.splice(i, 1);
                    }
                }
            }
            selected.forEach(element => {
                this.assignedCheckList.push(element);
            });
        } else {
            selected.forEach(element => {
                this.assignedCheckList.push(element);
            });

        }
        this.selection.clear();
        this.dataSource5.data = this.assignedCheckList;
        this.totalSize3=this.dataSource5.data.length;
        this.cdr.detectChanges();
    }
    fetchUsers() {
        const messages = [];
        this.addQuestions(this.selection.selected);


    }
    addQuestion(index) {
        //console.log(index);
        this.addQuestions([this.dataSource4.data[index]]);

    }

    onSubmit() {
        //	console.log(this.assignedCheckList.length);
        const controls = this.retails.controls;

        if (this.assignedCheckList.length == 0) {
            console.log("if");
            //this.authNoticeService.setNotice('testing', 'danger');
        } else {
        
            var geo = {};
          
            geo['country'] = this.retails.controls['country'].value;
            geo['state'] = this.retails.controls['state'].value;
            geo['district'] = this.retails.controls['district'].value;
            geo['taluk'] = this.retails.controls['taluk'].value;
            geo['city'] = this.retails.controls['city'].value;
            geo['pincode'] = this.retails.controls['pincode'].value;



            const formData: FormData = new FormData();
            formData.append('retail_id', controls['retail_id'].value);
            formData.append('retailer', controls['retailer'].value);
            formData.append('retail_category', controls['retail_category'].value);
            formData.append('landline', controls['landline'].value);
            formData.append('mobile_no', controls['mobile_no'].value);
            formData.append('email', controls['email'].value);
            formData.append('address_line1', controls['address_line1'].value);
            formData.append('address_line2', controls['address_line2'].value);
            formData.append('business_partner_name', controls['business_partner_name'].value);
            formData.append('business_partner_email', controls['business_partner_email'].value);
            formData.append('tl_name', controls['tl_name'].value);
            formData.append('tl_email', controls['tl_email'].value);
            formData.append('branding_elements', JSON.stringify(this.assignedBE));
            formData.append('products', JSON.stringify(this.assignedProduct));
            formData.append('promo', JSON.stringify(this.assignedPromo));
            formData.append('check_list', JSON.stringify(this.assignedCheckList));
            formData.append('geo', JSON.stringify(geo));
            formData.append('type', this.action);
            formData.append('s_no', String(this.sNo));

            
            this.auth.saveRetailDetails(formData).pipe(
                tap(result => {
                    if (result['code'] === 200) {
                        this.activeModal.dismiss(result['code']);
                        this.commonSer.Modal_close();
                        var title=result['title'];
                        var msg=result['message'];
                        swal.fire(
                            title,
                            msg,
                            'success'
                        )
                    }

                }),

            ).subscribe();




        }


    }



    //personalbar
    getstate(country_id) {
        this.retails.get('state').reset();
        this.retails.get('state').enable();

        this.retails.get('district').reset();
        this.retails.get('district').disable();

        this.retails.get('taluk').reset();
        this.retails.get('taluk').disable();

        this.retails.get('city').reset();
        this.retails.get('city').disable();

        this.retails.get('pincode').reset();
        this.retails.get('pincode').disable();



        setTimeout(() => {

            this.state = csc.getStatesOfCountry(country_id);
        });
    }
    getdistrict(state_id) {
        //	console.log(state_id['id']);
        this.retails.get('district').enable();

        this.retails.get('taluk').reset();
        this.retails.get('taluk').disable();

        this.retails.get('city').reset();
        this.retails.get('city').disable();

        this.retails.get('pincode').reset();
        this.retails.get('pincode').disable();

        setTimeout(() => {


            if (state_id <= 41) {

                this.district = csc.getDistrictById(state_id);

            } else {


                this.city = csc.getCitiesOfState(state_id);

            }
        });
    }
    gettaluk(district) {
        //	console.log(district['id']);
        this.retails.get('taluk').enable();

        this.retails.get('city').reset();
        this.retails.get('city').disable();

        this.retails.get('pincode').reset();
        this.retails.get('pincode').disable();




        setTimeout(() => {
            this.taluk = csc.getTalukOfDistrict(district);
        });
    }
    getcity() {
        this.retails.get('city').enable();
    }
    typedTownVillage() {
        this.retails.get('pincode').enable();

    }

    Type: common[] = [
        {value: 'Display', viewValue: 'Display', index: 0},
        {value: 'Sellable', viewValue: 'Sellable', index: 1}
        
        
       
      
      ];
      page_chage(events,data)
{
  
  
  var range=this.commonSer.handlePage(events,data);
  this.dataSource1=this.commonSer.iterator(this.assignedBE,range[0]['start'],range[0]['end']);
  this.dataSource2=this.commonSer.iterator(this.assignedPromo,range[0]['start'],range[0]['end'])
  this.dataSource3=this.commonSer.iterator(this.assignedProduct,range[0]['start'],range[0]['end'])
  this.dataSource4=this.commonSer.iterator(this.dataSource4.data,range[0]['start'],range[0]['end'])
  this.dataSource5=this.commonSer.iterator(this.assignedCheckList,range[0]['start'],range[0]['end'])
  this.cdr.detectChanges();
  
}
}
export interface common {
    value: string;
    viewValue: string;
    index: number
  }

